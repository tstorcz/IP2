# Vendor modifications

## Fixes in pyzernikemoment.py
print method call has to be adjusted to python 3 format

## Fixes in StereoVision  
### `blockmatchers.py`  
Stereovision presets constants are replaced with numbers
from https://docs.opencv.org/java/2.4.4/constant-values.html#org.opencv.calib3d.StereoBM.NARROW_PRESET  

`calibration.py`:  
new method added (after `add_corners`) `add_exact_corners` to speed up calibration by finding checkerboard corners only ones

```python
    def add_exact_corners(self, corners):
    """
    Record chessboard corners found in an image pair.
    """
    side = "left"
    self.object_points.append(self.corner_coordinates)
    for imagePoints in corners:
        self.image_points[side].append(imagePoints.reshape_image_batch_tensor(-1, 2))
        side = "right"
        self.image_count += 1
```

### `stereo_cameras.py`  
- `captures` property has to be initialized with Nothing in `CalibratedPair.__init__` if no devices are set  
```python
	if devices:
	    super(CalibratedPair, self).__init__(devices)
	else:
	    self.captures = None
```
- `captures` property has to be checked for Nothing before release in `StereoPair.__exit__`  
(desctructor of base of CalibratedPair)  
```python
	if not self.captures is None:
	    for capture in self.captures:
		capture.release()
```
- `show_frames` method copied to `show_captured_frames`
	- to show previously captured (and enhanced) frames
	- key press handling to control online processing
    		return value: `return cv2.waitKey(wait) & 0xFF`
```python
	def show_captured_frames(self, frames, wait=0):
	    """
	    Show previously captured frames.

	    `wait` is the wait interval in milliseconds before the window closes.
	    """
	    for window, frame in zip(self.windows, frames):
		cv2.imshow(window, frame)

	    return cv2.waitKey(wait) & 0xFF
```

### For creation of `cv2` block matchers
- factory methods have to be used
```python
	stereo = cv2.StereoBM_create(numDisparities=16, blockSize=15)
``` 
