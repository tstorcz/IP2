import setuptools


setuptools.setup(
    name='ImageProcessing',
    version='0.1',
    description='Collection of image processing tools',
    url='#',
    author='Tamas Storcz',
    install_requires=['tensorflow'],
    author_email='storcz.tamas@mik.pte.hu',
    packages=setuptools.find_packages(),
    zip_safe=False
)
