from Graph.Edge import Edge


class Vertex(object):

    def __init__(self):
        self.Edges = []
        
        
    def addEdge(self, newEdgePoints):
        self.Edges.append(Edge(newEdgePoints))
        
    def attachEdge(self, newEdge):
        if isinstance(newEdge, Edge):
            self.Edges.append(newEdge)
        else:
            raise ValueError('Invalid newEdge parameter type')
        
    def findEdge(self, targetCoords):
        
        for e in self.Edges:
            if e.isToCoords(targetCoords):
                return e
        
        return None
        
    def edgeCount(self):
        return len(self.Edges)
    
    
    def __repr__(self):
        s = ""
        for edge in self.Edges:
            if(len(s)> 0):
                s = s + "-"
            s = s + str(edge.count())
        return str(len(self.Edges)) + "[" + s + "]"