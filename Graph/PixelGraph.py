import numpy
from skimage import measure

from Image.Filter import Filter
from Image.Kernel import Kernel
from Image.PixelIndex import PixelIndex
from Graph.Vertex import Vertex
from Graph.Vectorizer import Vectorizer


class PixelGraph(object):

    def __init__(self, binary_skeleton):
        self.Vertices = {}

        self.checked_vertices = set()
        self.depth_first_start = None

        self.__Neighbours = numpy.uint8(
            Filter.convolution(
                binary_skeleton,
                Kernel.create_kernel('articulationIndex'),
                1
            )
        )
        self.__Neighbours[binary_skeleton == 0] = 0
        
        self.__NeighbourCount = numpy.uint8(
            Filter.convolution(
                binary_skeleton,
                Kernel.create_kernel('neighbour8'),
                1
            )
        )
        self.__NeighbourCount[binary_skeleton == 0] = 0
        
        self.__find_vertices(1)
        self.__find_vertices(3)
        self.__find_vertices(4)
        
        self.__follow_edges()
        
        self.__merge_vertices()
        
    def first_vertex(self):
        key_list = list(self.Vertices.keys())
        key_list.sort(key=lambda tup: tup[0])
        return key_list[-1]
        
    def vertex_count(self):
        return len(self.Vertices)
        
    def __find_vertices(self, neighbor_count):
        (sr, sc) = numpy.where(self.__NeighbourCount == neighbor_count)
    
        for i in range(0, len(sr)):
            vertex_coords = (sr[i], sc[i])
            new_vertex = Vertex()
            self.Vertices[vertex_coords] = new_vertex
            for neighbor in self.__get_neighbors(vertex_coords):
                new_vertex.addEdge([vertex_coords, neighbor])
        
    def __get_neighbors(self, point):
        neighbors = []
        direction = 1
        point_direction = self.__Neighbours[point[0], point[1]]
        point_coords = numpy.array(point)
        while direction <= point_direction:
            if (direction & point_direction) > 0:
                neighbor_coords = point_coords + PixelIndex.GetIndexModifiers(direction)
                neighbors.append((neighbor_coords[0], neighbor_coords[1]))

            direction *= 2
        
        return neighbors
        
    def __follow_edges(self):
        for _, vertex in self.Vertices.items():
            for i in range(0, vertex.edgeCount()):
                if not vertex.Edges[i].IsReversed:
                    full_edge = self.__follow_single_edge(vertex.Edges[i].Points)
                    vertex.Edges[i].set(full_edge)
                    self.__save_reversed_edge(full_edge)
    
    # continues the edge, so in front of the last, there could be ambiguous pixels identified by more complex algorithms
    def __follow_single_edge(self, edge):
        while self.__NeighbourCount[edge[-1][0], edge[-1][1]] == 2:
            direction = self.__Neighbours[
                            edge[-1][0],
                            edge[-1][1]
                        ] \
                        & \
                        (255 ^ PixelIndex.GetArticulationIndex((edge[-2][0]-edge[-1][0], edge[-2][1]-edge[-1][1])))
            edge.append(tuple(edge[-1] + PixelIndex.GetIndexModifiers(direction)))
    
        return edge
    
    def __save_reversed_edge(self, edge):
        reversed_edge = edge[::-1]
        for i in range(0, self.Vertices[edge[-1]].edgeCount()):
            if self.Vertices[edge[-1]].Edges[i].Points[0:2] == reversed_edge[0:2]:
                self.Vertices[edge[-1]].Edges[i].setReversed(reversed_edge)
    
    # Has to be called after vectorization to put a new segment to the beginning of edge
    def __merge_vertices(self):
        feature_points = numpy.ones(numpy.shape(self.__NeighbourCount))
        feature_points[self.__NeighbourCount < 3] = 0
        feature_neighbor_count = numpy.uint8(
            Filter.convolution(
                feature_points,
                Kernel.create_kernel('neighbour8'),
                1
            )
        )
        feature_points[feature_neighbor_count < 2] = 0
        labels, num = measure.label(feature_points, neighbors=8, return_num=True)
        
        for i in range(1, num+1):
            x, y = numpy.where(labels == i)
            point_coords = list(zip(x, y))
            self.__merge_points(point_coords)

    def __merge_points(self, point_coords):
        new_coord = self.__compute_new_coords(point_coords)
        new_vertex = Vertex()
        
        for c in point_coords:
            for e in self.Vertices[c].Edges:
                # Outgoing edges start in newCoord if not connections between area points
                # (otherwise ignored, reversed will be ignored from other side)
                if e[-1] not in point_coords:
                    if e[0] != new_coord:
                        e.Points.insert(0, new_coord)
                        self.__update_reversed_edge(e)

                    new_vertex.attachEdge(e)
            del self.Vertices[c]
                
        self.Vertices[new_coord] = new_vertex
        
        return 0

    @staticmethod
    def __compute_new_coords(point_coords):
        new_x = 0.0
        new_y = 0.0
        for c in point_coords:
            new_x += c[0]
            new_y += c[1]
            
        return int(new_x / len(point_coords)), int(new_y / len(point_coords))
    
    def __update_reversed_edge(self, edge):
        connected_vertex = self.Vertices[edge[-1]]
        # first point is the new vertex coordinate
        reversed_edge = connected_vertex.findEdge(edge[1])
        reversed_edge.Points.append(edge[0])
    
    def depth_first_descriptor(self, start_vertex_coords):
        self.checked_vertices = set()
        self.depth_first_start = start_vertex_coords
        vertex = self.Vertices[start_vertex_coords]
        descriptor = ["e{0:.2f}".format(vertex.Edges[0].length())]
        descriptor.extend(self.__create_vertex_descriptor(vertex.Edges[0][-1], vertex.Edges[0]))
        return descriptor
    
    # Do not check edge.IsReversed, because edges were followed randomly
    def __create_vertex_descriptor(self, vertex_id, incoming_edge):
        self.checked_vertices.add(vertex_id)
        vertex = self.Vertices[vertex_id]
        prev_edge = incoming_edge
        descriptor = []

        # Create edge list, ordered by relative angle to incoming edge
        # If incomingEdge is None, the we are starting from a leaf vertex,
        # there is only 1 outgoing edge, no need for sort
        ordered_edge_list = []
        for i in range(0, vertex.edgeCount()):
            ordered_edge_list.append(
                (Vectorizer.getAngle(incoming_edge.Points, vertex.Edges[i].Points), vertex.Edges[i])
            )
        ordered_edge_list.sort(key=lambda tup: tup[0])
        
        for _, edge in ordered_edge_list:
            if edge.Points[-1] not in self.checked_vertices and edge.Points[-1] != self.depth_first_start:
                descriptor.append("t{0:.2f}".format(Vectorizer.getAngle(prev_edge.Points, edge.Points)))
                prev_edge = edge
                descriptor.append("e{0:.2f}".format(prev_edge.length()))
                descriptor.extend(self.__create_vertex_descriptor(prev_edge.Points[-1], prev_edge))
        descriptor.append('/')
        return descriptor
