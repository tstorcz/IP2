import math
from Graph.Vectorizer import Vectorizer


class Edge(object):

    def __init__(self, points = [], isReversed = False):
        self.Points = points
        self.IsReversed = isReversed


    def set(self, points):
        self.Points = Vectorizer.vectorize(points)


    def setReversed(self, points):
        self.Points = Vectorizer.vectorize(points)
        self.IsReversed = True

    def isToCoords(self, targetCoords):
        return self.Points[-1] == targetCoords


    def count(self):
        return len(self.Points)


    def directLength(self, digits = 2):
        return round(math.sqrt((self.Points[-1][0] - self.Points[0][0])**2 + (self.Points[-1][1] - self.Points[0][1])**2), digits)


    def length(self, digits = 2):
        length = 0
        for i in range(0, len(self.Points)-1):
            length += round(math.sqrt((self.Points[i+1][0] - self.Points[i][0])**2 + (self.Points[i+1][1] - self.Points[i][1])**2), digits)
        return length


    def __getitem__(self, index):
        return self.Points[index]
