import numpy
import math


class Vectorizer(object):
    '''
    Vector representation as a list of points (not point pairs) is preferred
    Same to follow, but easier to revert and create vector representation
    '''
    
    '''
    pixels: array of pixels - tuple of integers (coord1, coord2)
    accuracy: max distance of pixels from substituting vector
    '''
    @staticmethod
    def vectorize(pixels, accuracy = 1):
        if(len(pixels) < 3):
            return pixels
        
        vectors = [pixels[0]]
        startIndex = 0
        endIndex = 1
        
        while(endIndex < len(pixels)):
            isStraight = True
            i=startIndex
            while(i<endIndex and isStraight):
                d21 = numpy.subtract(pixels[endIndex], pixels[startIndex])
                d13 = numpy.subtract(pixels[startIndex], pixels[i])
                d = numpy.linalg.norm(numpy.cross(d21, d13))/numpy.linalg.norm(d21)
                isStraight = (d <= accuracy)
                i+=1
            
            if(isStraight):
                endIndex += 1
            else:
                vectors.append(pixels[endIndex - 1])
                startIndex = endIndex - 1
        
        vectors.append(pixels[-1])
        
        return vectors
    
    
    @staticmethod
    def getAngle(edge1, edge2):
        i = -2
        v1 = (0, 0)
        while(i + len(edge1) >= 0 and Vectorizer.length(v1) < 10):
            v1 = numpy.subtract(edge1[-1], edge1[i])
            i -= 1
            
        i = 1
        v2 = (0, 0)
        while(i < len(edge2) and Vectorizer.length(v2) < 10):
            v2 = numpy.subtract(edge2[i], edge2[0])
            i += 1
            
        angle = Vectorizer.getAbsoluteAngle(v2) - Vectorizer.getAbsoluteAngle(v1) + 180
        
        while(angle > 360):
            angle -= 360
            
        while(angle < 0):
            angle += 360
        
        return angle if angle > 0 else 360 + angle
    
    
    @staticmethod
    def getAbsoluteAngle(vector):
        angle = math.degrees(math.atan2(vector[1], vector[0]))
        angle = 180 - angle
        return angle
     
        
    @staticmethod
    def length(v):
        return math.sqrt(Vectorizer.dotproduct(v, v))
    
    
    @staticmethod
    def dotproduct(v1, v2):
        return sum((a*b) for a, b in zip(v1, v2))
    