import smtplib
import ssl

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class EmailSender(object):
    def __init__(self, smtp_server, smtp_port):
        self.server = smtp_server
        self.port = smtp_port
        self.login = ""
        self.password = ""

    def set_credentials(self, login, password):
        self.login = login
        self.password = password

    def send(self, sender_email, recipient_email, subject, contents, attachments=None, is_html=False):
        context = ssl.create_default_context()

        server = None
        try:
            server = smtplib.SMTP(self.server, self.port)
            server.starttls(context=context)
            server.login(self.login, self.password)

            message = EmailSender.__create_message(
                sender_email, recipient_email,
                subject, contents,
                is_html=is_html
            )

            if attachments is not None:
                for file in attachments:
                    EmailSender.__add_message_attachment(message, file)

            server.sendmail(sender_email, recipient_email, message.as_string())

        except Exception as e:
            print(e)
        finally:
            if server is not None:
                server.quit()

    @staticmethod
    def __create_message(sender_email, recipient_email, subject, content, is_html=False):
        message = MIMEMultipart("alternative")
        message["Subject"] = subject
        message["From"] = sender_email
        message["To"] = recipient_email

        message.attach(MIMEText(content, "plain"))

        if is_html:
            html_content = "<html><body><p>{0}</p></body></html>".format(content)
            part = MIMEText(html_content, "html")
            message.attach(part)

        return message

    @staticmethod
    def __add_message_attachment(message, file_name):
        with open(file_name, "rb") as attachment_file:
            attachment = MIMEBase("application", "octet-stream")
            attachment.set_payload(attachment_file.read())

        encoders.encode_base64(attachment)

        attachment.add_header(
            "Content-Disposition",
            f"attachment; filename= {file_name}",
        )

        message.attach(attachment)

        return message
