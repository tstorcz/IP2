import dropbox
import os


class DropBoxConnection(object):

    def __init__(self, access_token):
        self.dbx = dropbox.Dropbox(access_token)

    def upload(self, source_file):
        _, destination_file_name = os.path.split(source_file)
        f = open(source_file, 'rb')
        self.dbx.files_upload(f.read(), "/{0}".format(destination_file_name))
