import selectors
import socket
import threading

from ConnectionDescriptor import ConnectionDescriptor


class TCPServer:
    def __init__(self, host="127.0.0.1", port=65432, buffer_size=1024,
                 data_receive_callback=None,
                 echo_received=True
                 ):
        self.__host = host
        self.__port = port
        self.buffer_size = buffer_size
        self.__data_receive_callback = data_receive_callback
        self.__echo_received = echo_received
        self.__selector = selectors.DefaultSelector()
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__socket.bind((self.__host, self.__port))
        self.__socket.setblocking(False)
        self.__connections = []
        self.__listening = False
        self.__listener_thread = None

        self.__next_connection_id = 1
        self.__thread_lock = threading.Lock()

    def start(self):
        self.__listening = True
        self.__listener_thread = threading.Thread(target=self.__listen, daemon=True)
        self.__listener_thread.start()

    def stop(self):
        self.__listening = False

        self.__listener_thread.join()
        self.__close_socket()
        self.__selector.close()

    def send_all(self, data):
        self.__echo(data)

    def send(self, data, targets: list):
        for conn_desc in self.__connections:
            if conn_desc.identifier in targets:
                conn_desc.connection.send(data)

    def close_connection(self, targets: list):
        to_close = []
        for conn_desc in self.__connections:
            if conn_desc.identifier in targets:
                conn_desc.connection.close()
                to_close.append(conn_desc)

        for conn_desc in to_close:
            self.__connections.remove(conn_desc)

    def get_connection_address(self, connection_id):
        for conn_desc in self.__connections:
            if conn_desc.identifier == connection_id:
                return conn_desc.address

        return None

    def __listen(self):
        self.__socket.listen()
        self.__selector.register(self.__socket, selectors.EVENT_READ, self.__accept)

        while self.__listening:
            events = self.__selector.select()
            for key, flags in events:
                callback = key.data
                callback(key.fileobj, flags)

    def __accept(self, source_socket, event_flags):
        new_conn, addr = source_socket.accept()
        with self.__thread_lock:
            self.__connections.append(ConnectionDescriptor(self.__next_connection_id, new_conn, addr))
            self.__next_connection_id += 1
        new_conn.setblocking(False)
        self.__selector.register(new_conn, selectors.EVENT_READ, self.__data_received)

    def __data_received(self, source_connection, event_flags):
        data, flags = source_connection.recv(self.buffer_size)
        if data:
            if self.__echo_received:
                self.__echo(data, source_connection)

            if self.__data_receive_callback is not None:
                for conn_desc in self.__connections:
                    if conn_desc.connection == source_connection:
                        self.__data_receive_callback(conn_desc.address, data)

        else:
            self.__close_connection(source_connection)

    def __echo(self, data, excluded_connection=None):
        for conn_desc in self.__connections:
            if conn_desc.connection != excluded_connection:
                conn_desc.connection.send(data)

    def __close_connection(self, connection):
        self.__selector.unregister(connection)
        connection.close()
        connection_descriptor_to_remove = None
        for conn_desc in self.__connections:
            if conn_desc.connection == connection:
                connection_descriptor_to_remove = conn_desc

        if connection_descriptor_to_remove is not None:
            self.__connections.remove(connection_descriptor_to_remove)

    def __close_socket(self):
        for conn_desc in self.__connections:
            self.__close_connection(conn_desc.connection)
        self.__selector.unregister(self.__socket)
        self.__socket.close()
        self.__socket = None
