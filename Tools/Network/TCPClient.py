import socket
import selectors


class TCPClient:
    def __init__(self, host="127.0.0.1", port=65432, buffer_size=1024, data_receive_callback=None):
        self.__host = host
        self.__port = port
        self.buffer_size = buffer_size
        self.__data_receive_callback = data_receive_callback
        self.__selector = selectors.DefaultSelector()
        self.__socket = None

    def open(self):
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__socket.setblocking(False)
        self.__socket.connect_ex((self.__host, self.__port))

        self.__selector.register(self.__socket, selectors.EVENT_READ, self.__data_received)

    def __data_received(self, source_connection, event_flags):
        data, flags = source_connection.recv(self.buffer_size)
        if data:
            if self.__data_receive_callback is not None:
                self.__data_receive_callback(data)
        else:
            self.close()

    def close(self):
        self.__socket.close()
