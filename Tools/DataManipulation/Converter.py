import numpy


class Converter(object):

    @staticmethod
    def number_to_binary(number, length=8):
        return numpy.binary_repr(number, width=length)

    @staticmethod
    def string_to_nested_list(list_string, item_converter=None):
        output = list_string.strip()
        if output.startswith('[') and output.endswith(']'):
            output = output.split(',')
            output = [Converter.string_to_nested_list(x, item_converter) for x in output]
        else:
            if item_converter is not None:
                output = item_converter(output)

        return output
