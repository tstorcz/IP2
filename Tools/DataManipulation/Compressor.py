import shutil


class Compressor(object):

    @staticmethod
    def compress_folder(source_folder, archive_name):
        shutil.make_archive(archive_name, 'zip', source_folder)
        return archive_name
