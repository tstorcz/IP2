import time


class StopWatch(object):

    def __init__(self, label=''):
        self.splits = []
        self.label = label
        self.is_running = False

    def reset(self):
        self.splits = [time.time()]
        self.is_running = False

    def start(self):
        self.reset()
        self.is_running = True

    def split_time(self, split_index) -> float:
        if split_index >= len(self.splits):
            time_to = time.time()
        else:
            time_to = self.splits[split_index + 1]
        return time_to - self.splits[split_index]

    def last_split_time(self, current_time=None) -> float:
        if current_time is None:
            if self.is_running:
                return time.time() - self.splits[-1]
            else:
                return self.splits[-1] - self.splits[-2]

    def total_time(self, current_time=None) -> float:
        if self.is_running:
            if current_time is None:
                time_to = time.time()
            else:
                time_to = current_time

            return time_to - self.splits[0]
        else:
            return self.splits[-1] - self.splits[0]

    def split(self):
        if self.is_running:
            self.splits.append(time.time())

    def stop(self) -> float:
        self.split()
        self.is_running = False
        if len(self.splits) >= 2:
            return self.total_time()
        else:
            return 0
