import numpy


class FileLoader:

    @staticmethod
    def load_list_of_dicts(file_name, keys, value_converters=None):
        items = []
        with open(file_name) as file:
            line = file.readline()
            while line:
                line = file.readline()
                values = line.split(";")
                if len(keys) == len(values):
                    items.append({})
                    for i in range(0, len(values)):
                        if len(keys[i]) > 0:
                            v = values[i].strip()
                            if value_converters is None:
                                items[-1][keys[i]] = v
                            else:
                                items[-1][keys[i]] = v if value_converters[i] is None else value_converters[i](v)

        return items

    @staticmethod
    def load_list_of_lists(file_name, filters=None,
                           value_converters=None, item_transformer=None,
                           as_array=False):
        items = []
        with open(file_name) as file:
            line = file.readline()
            while line:
                line = file.readline()
                values = line.split(";")
                if len(filters) == len(values):
                    new_item = []
                    for i in range(0, len(values)):
                        if filters is None or filters[i]:
                            v = values[i].strip()
                            if value_converters is None:
                                converted = v
                            else:
                                converted = v if value_converters[i] is None else value_converters[i](v)
                            new_item.append(converted)
                    if item_transformer is not None:
                        new_item = item_transformer(new_item)
                    items.append(new_item)

        return items if not as_array else numpy.asarray(items)

    @staticmethod
    def load_text_file(file_name,
                       data_preprocessor=None,
                       column_header_count=0,
                       return_header=False):
        headers = []
        lines = []

        with open(file_name) as file:
            line = True
            for h in range(0, column_header_count):
                line = file.readline()
                headers.append(line)

            while line:
                line = file.readline()
                if data_preprocessor is None:
                    lines.append(line)
                else:
                    new_line = data_preprocessor(line)
                    if new_line is not None:
                        lines.append(new_line)

        return (lines, headers) if return_header else lines
