import os.path

from os import walk
from os.path import join, splitext
from pathlib import Path


class FileSystem(object):

    @staticmethod
    def split_file_name(file_name_with_path):
        dir_name = os.path.dirname(file_name_with_path)
        file_name = os.path.basename(file_name_with_path)
        file_name_without_extension, file_extension = splitext(file_name)
        return dir_name, file_name_without_extension, file_extension

    @staticmethod
    def create_folder(folder_path, sub_path=None):

        if folder_path is not None and sub_path is not None:
            folder_path = os.path.join(folder_path, sub_path)

        if folder_path is not None and not os.path.exists(folder_path):
            os.makedirs(folder_path)

        return folder_path

    @staticmethod
    def get_file_names_of_folder(path_to_search, is_recursive=False, extension_list=None):
        files_found = []

        # dirNames parameter not in use
        for (dir_path, _, file_names) in walk(path_to_search):
            for file_name in file_names:
                file_path_with_name = join(dir_path, file_name)
                file_name_without_extension, ext = splitext(file_name)

                if extension_list is None or ext.lower() in extension_list:
                    files_found.append([file_path_with_name, file_name_without_extension, ext])

            if not is_recursive:
                break

        return files_found

    @staticmethod
    def get_folders_of_folder(path_to_search, is_recursive=False):
        folders_found = []

        for (dir_path, folders, _) in walk(path_to_search):
            for folder in folders:
                full_path_found = join(dir_path, folder)
                folders_found.append(full_path_found)

            if not is_recursive:
                break

        return folders_found

    @staticmethod
    def process_files_of_folder(input_folder, extension_list,
                                processor_method_reference, processor_method_arg_list=None,
                                is_recursive=False,
                                progress_indicator_method_reference=None):
        images = FileSystem.get_file_names_of_folder(input_folder, is_recursive, extension_list)
        for i in range(0, len(images)):
            if progress_indicator_method_reference is not None:
                progress_indicator_method_reference(i, len(images))

            if processor_method_arg_list is None:
                processor_method_reference(images[i])
            else:
                processor_method_reference(images[i], processor_method_arg_list)

    @staticmethod
    def add_subfolder(file_name_with_path, subfolder):
        path = Path(file_name_with_path)
        return join(os.path.dirname(file_name_with_path), subfolder, path.name)

    @staticmethod
    def create_subfolder(file_name_with_path, subfolder):
        return join(os.path.dirname(file_name_with_path), subfolder)

    @staticmethod
    def create_file_name(file_name_with_path, suffix, new_path=None):
        dir_name = os.path.dirname(file_name_with_path)
        file_name = os.path.basename(file_name_with_path)
        file_name_without_extension, file_extension = splitext(file_name)

        new_file_name = file_name_without_extension + suffix + file_extension

        if len(dir_name) > 0 or new_path is not None:
            new_file_name = os.path.join(
                dir_name if new_path is None else new_path,
                new_file_name
            )

        return new_file_name

    @staticmethod
    def delete_file(file_full_name):
        os.remove(file_full_name)

    @staticmethod
    def move_file(source_file, destination_file):
        Path(source_file).rename(destination_file)
