from enum import Enum


class LogChannelStates(Enum):
    INITIALIZED = 1
    OPENED = 2
    CLOSED = 3


class LogChannel(object):
    def __init__(self, channel_id: str, write_header=True):
        self._channel_id = channel_id
        self._state = LogChannelStates.INITIALIZED
        self.write_header = write_header

    def get_id(self):
        return self._channel_id

    def get_state(self):
        return self._state

    def is_opened(self):
        return self._state.value == LogChannelStates.OPENED.value

    def open(self):
        self._state = LogChannelStates.OPENED
        self._open()

    def close(self):
        self._state = LogChannelStates.CLOSED
        self._close()

    def write(self, header: str, message: str):
        if self._state == LogChannelStates.OPENED:
            if self.write_header:
                self._log_message(
                    "{0} {1}".format(header, message)
                )
            else:
                self._log_message(message)

    def _open(self):
        print(self._channel_id, "open")
        raise NotImplementedError()

    def _close(self):
        print(self._channel_id, "close")
        raise NotImplementedError()

    def _log_message(self, message: str):
        print(self._channel_id, message)
        raise NotImplementedError()
