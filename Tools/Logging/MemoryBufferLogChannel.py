from Tools.Logging.LogChannel import LogChannel


class MemoryBufferLogChannel(LogChannel):

    def __init__(self, channel_id: str, write_header=True):
        super().__init__(channel_id, write_header)
        self.__buffer = []

        self.open()

    def _open(self):
        pass

    def _close(self):
        pass

    def _log_message(self, message: str):
        self.__buffer.append(message)

    def get_content(self):
        return self.__buffer

    def clear(self):
        self.__buffer = []
