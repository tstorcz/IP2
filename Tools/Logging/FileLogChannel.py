from Tools.Logging.LogChannel import LogChannel


class FileLogChannel(LogChannel):

    def __init__(self, channel_id: str, file_name: str, buffer_lines: int = 0, write_header=True):
        super().__init__(channel_id, write_header)

        self._buffer = []
        self._buffer_size = 0
        self.file_name = file_name
        self.buffer_lines = buffer_lines

        self.open()

    def _open(self):
        pass

    def _close(self):
        self._flush()

    def _log_message(self, message: str):
        self._buffer.append(message)
        self._buffer_size += len(message)

        if len(self._buffer) >= self.buffer_lines:
            self._flush()

    def _flush(self):
        f = open(self.file_name, "a")
        for line in self._buffer:
            line_string = "{0}\n".format(line)

            f.write(line_string)

        f.close()

        self._buffer = []
        self._buffer_size = 0
