import os
from datetime import datetime

from Tools.Logging.FileLogChannel import FileLogChannel


class FileRotatorLogChannel(FileLogChannel):

    def __init__(self, channel_id: str, file_name_template: str, max_size: int, daily_file: bool = False,
                 buffer_lines: int = 0, write_header=True):
        self.__sequence = 0
        self.daily_file = daily_file
        self.__current_date = ""
        self.file_name_template = file_name_template
        self.max_size = max_size

        self._get_new_file()
        super().__init__(channel_id, self.file_name, buffer_lines, write_header)

    def _log_message(self, message: str):
        file_stats = os.stat(self.file_name)
        if (0 < self.max_size < file_stats.st_size + self._buffer_size + len(message)) or \
                (self.daily_file and self.__current_date != datetime.now().strftime("%Y%m%d")):
            self._flush()
            self._get_new_file()

        self._buffer.append(message)
        self._buffer_size += len(message)

        if len(self._buffer) >= self.buffer_lines:
            self._flush()

    def _get_new_file(self):
        self.__sequence += 1
        self.__set_file_name()

    def __set_file_name(self):
        self.file_name = self.file_name_template
        current_date = datetime.now()

        self.__current_date = current_date.strftime("%Y%m%d")
        self.file_name.replace("<date>", current_date.strftime("%Y%m%d"))
        self.file_name.replace("<time>", current_date.strftime("%H%M%S"))
        self.file_name.replace("<sequence>", str(self.__sequence))
