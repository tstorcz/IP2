class ValueCollector:
    @staticmethod
    def flatten(t):
        if isinstance(t, (list, tuple)):
            result = []
            for i in t:
                result.extend(ValueCollector.flatten(i))

            return result
        else:
            return [t]

    @staticmethod
    def list_it(t):
        return list(map(ValueCollector.list_it, t)) if isinstance(t, (list, tuple)) else t

    def __init__(self, value_collector_id: str = None, decimals=2):

        self.__value_collector_id = value_collector_id
        self.__decimals = decimals
        self.__headers = []
        self.__values = []

    def get_id(self):
        return self.__value_collector_id

    def add_headers(self, new_headers: []):
        self.__headers.extend(new_headers)

    def collect_value(self, single_value):
        self.__values.append(single_value)

    def collect_values(self, value_collection):
        self.__values.extend(ValueCollector.flatten(ValueCollector.list_it(value_collection)))

    def get_headers(self):
        return self.__headers

    def flush(self, message_text: str, delimiter=", "):
        value_text = ""
        for i in self.__values:
            if len(value_text) > 0:
                value_text += delimiter

            if isinstance(i, float):
                value_text += ("{" + ":.{0}f".format(self.__decimals) + "}").format(i)
            else:
                value_text += str(i)

        self.__values = []

        return message_text + value_text
