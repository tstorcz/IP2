from Tools.Logging.LogChannel import LogChannel


class ConsoleLogChannel(LogChannel):

    def __init__(self, channel_id: str, write_header=True):
        super().__init__(channel_id, write_header)

        self.open()

    def _open(self):
        pass

    def _close(self):
        pass

    def _log_message(self, message: str):
        print(message)
