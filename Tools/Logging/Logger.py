import datetime

from enum import Enum
from Tools.Logging.LogChannel import LogChannel


class LogLevels(Enum):
    NO = 0
    CRITICAL = 1
    ERROR = 2
    WARNING = 3
    INFO = 4
    VERBOSE = 5


class Logger(object):
    def __init__(self, logger_id: str = None, log_level=LogLevels.ERROR):
        self.__logger_id = logger_id
        self.__log_level = log_level
        self.__channels = []

    def get_id(self):
        return self.__logger_id

    def set_level(self, new_level: LogLevels):
        self.__log_level = new_level

    def add_channel(self, new_channel: LogChannel):
        self.__channels.append(new_channel)

    def remove_channel(self, channel_id: str):
        for i in range(len(self.__channels)-1, -1, -1):
            if self.__channels[i].get_id() == channel_id:
                del self.__channels[i]

    def log(self, message_text: str, message_level: LogLevels):
        if self.__log_level.value > LogLevels.NO.value and self.__log_level.value >= message_level.value:
            current_date = datetime.datetime.now()

            message_header = "{0} [{1}.{2}]".format(
                        current_date.strftime("%Y.%m.%d %H:%M:%S"),
                        self.__logger_id,
                        message_level.name)

            for ch in self.__channels:
                if ch.is_opened():
                    ch.write(message_header, message_text)

    def critical(self, message):
        self.log(message, LogLevels.CRITICAL)

    def error(self, message):
        self.log(message, LogLevels.ERROR)

    def warning(self, message):
        self.log(message, LogLevels.WARNING)

    def info(self, message):
        self.log(message, LogLevels.INFO)

    def verbose(self, message):
        self.log(message, LogLevels.VERBOSE)
