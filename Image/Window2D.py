import numpy as np


class Window2D(object):
    # ---------------------------------------------------
    # TODO: extend execution with multiple image handling
    # ---------------------------------------------------
    @staticmethod
    def convolution(x, y):
        padded_kernel = np.zeros(x.shape)
        top_left = np.floor(np.subtract(np.asarray(x.shape), np.asarray(y.shape)) / 2.0).astype(np.int)
        bottom_right = np.add(top_left, np.asarray(y.shape)).astype(np.int)
        padded_kernel[top_left[0]:bottom_right[0], top_left[1]:bottom_right[1]] = y

        fr = np.fft.fft2(x)
        fr2 = np.fft.fft2(np.flipud(np.fliplr(padded_kernel)))
        m, n = fr.shape
        cc = np.real(np.fft.ifft2(fr * fr2))
        cc = np.roll(cc, int(-m / 2 + 1), axis=0)
        cc = np.roll(cc, int(-n / 2 + 1), axis=1)
        return cc

    @staticmethod
    def standard_deviation(image, window_size):
        # based on kernel structure, 2D cross correlation can be used, convolution is not required

        uniform_kernel = np.ones([window_size, window_size]) / (window_size ** 2)

        filtered = Window2D.convolution(image, uniform_kernel)
        square_filtered = Window2D.convolution(
            image**2,
            uniform_kernel
        )

        return np.subtract(square_filtered, filtered**2)

    @staticmethod
    def normalized_cross_correlation(image, kernel, window_size=None):
        if window_size is None:
            window_size = kernel[0].shape[0]

        padded_kernel = np.zeros(image.shape)
        top_left = np.floor(np.subtract(np.asarray(image.shape), np.asarray(kernel.shape)) / 2.0).astype(np.int)
        bottom_right = np.add(top_left, np.asarray(kernel.shape)).astype(np.int)
        padded_kernel[top_left[0]:bottom_right[0], top_left[1]:bottom_right[1]] = kernel

        di = Window2D.standard_deviation(image, window_size) * window_size
        dt = Window2D.standard_deviation(padded_kernel, window_size) * window_size
        n = Window2D.convolution(image, padded_kernel)
        return np.divide(n, np.multiply(di, dt))
