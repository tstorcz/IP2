import random
import numpy
from Image.Filter import Filter


class ImagePartDataSource(object):

    def __init__(self,
                 kernels,
                 image,
                 bin_count,
                 batch_size):

        self.__kernels = kernels
        self.__image = image
        self.__bin_count = bin_count
        self.__batch_size = batch_size
        self.__process_image()

    def __process_image(self):
        self.__filtered_images = []
        self.__histograms = []
        self.histogram_sum = None
        for kernel in self.__kernels:
            filtered_image = numpy.absolute(
                    Filter.convolution(self.__image, kernel),
                )

            self.__filtered_images.append(filtered_image)

            quantized_image = Filter.quantize(filtered_image, self.__bin_count)

            # histogram contains number of elements !NOT NORMED!
            new_histogram = numpy.histogram(
                quantized_image,
                bins=numpy.arange(self.__bin_count),
                range=(0, self.__bin_count-1),
                density=False
            )
            self.__histograms.append(new_histogram)

            if self.histogram_sum is None:
                self.histogram_sum = new_histogram
            else:
                self.histogram_sum = numpy.add(self.histogram_sum, new_histogram)

    # TODO: finish creation from each kernels of each bins
    def get_batch(self, bin_index):
        y = []
        x = []
        rnd = random.Random()

        for bin_index in range(0, self.__bin_count):
            # TODO: calculate how many items are taken from bins
            for kernel_idx in range(0, len(self.__kernels)):
                # TODO: calculate how many items are taken from kernels of bins
                new_y = rnd.randrange(0, kernel_idx)
                new_x = rnd.randrange(0, kernel_idx)
                y.append(new_y)
                x.append(new_x)

        return self.__kernels[random.randrange(len(self.__kernels))]
