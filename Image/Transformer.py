import numpy as np

from scipy import ndimage

from Image.ImageData import ImageData


class Transformer(object):

    @staticmethod
    def rotate(image, angle, reshape=True):
        return ndimage.rotate(image, angle, reshape=reshape)

    @staticmethod
    def flip(image, axis=1):
        return np.flip(image, axis=axis)

    @staticmethod
    def augmentation(
        source_image,
        cropped_square_size,
        augmentation_count=1,
        min_random_shift=0, max_random_shift=0,
        enable_mirroring=False
    ):
        augmentation = []

        augmentation.extend(
            Transformer.get_rotated_images(
                source_image,
                cropped_square_size,
                (augmentation_count - 1) // 2 + (augmentation_count - 1) % 2    # number of new images with mirroring
                if enable_mirroring
                else augmentation_count,                                        # number of new images without mirroring
                min_random_shift, max_random_shift
            )
        )

        if enable_mirroring:
            augmentation.extend(
                Transformer.get_rotated_images(
                    Transformer.flip(source_image),
                    cropped_square_size,
                    (augmentation_count - 1) // 2,
                    min_random_shift, max_random_shift
                )
            )

        return augmentation

    @staticmethod
    def get_rotated_images(
            source_image,
            cropped_square_size,
            augmentation_count=1,
            min_random_shift=0, max_random_shift=0,
            min_angle_difference=10
    ):
        angle_interval = int((360 - (augmentation_count * min_angle_difference)) / augmentation_count)
        angle_interval_shift = int(angle_interval / 2)
        angle_step = int(360 / augmentation_count)

        augmented_images = [
            ImageData.crop_image(
                source_image,
                [cropped_square_size, cropped_square_size],
                center=[int(source_image.shape[0] / 2), int(source_image.shape[1] / 2)]
            )
        ]

        for aug_idx in range(1, augmentation_count):
            random_shift = np.array(np.random.rand(2) * (max_random_shift - min_random_shift))
            random_shift += min_random_shift
            random_shift = np.multiply(random_shift, ((np.random.randint(0, 2, 2) * 2) - 1))

            rotated_base_image = Transformer.rotate(
                source_image.copy(),
                np.random.randint(1, angle_interval) - angle_interval_shift + aug_idx * angle_step
            )

            rotated_center = [int(rotated_base_image.shape[0] / 2), int(rotated_base_image.shape[1] / 2)]
            new_center = rotated_center + random_shift

            augmented_images.append(
                ImageData.crop_image(rotated_base_image, [cropped_square_size, cropped_square_size], center=new_center)
            )

        return augmented_images
