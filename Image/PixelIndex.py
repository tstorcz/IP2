import numpy


class PixelIndex(object):

    @staticmethod
    def GetIndexModifiers(articulationIndex):
        dct = {
            0:   [ 0,  0],
            1:   [-1, -1],
            2:   [-1,  0],
            4:   [-1,  1],
            8:   [ 0,  1],
            16:  [ 1,  1],
            32:  [ 1,  0],
            64:  [ 1, -1],
            128: [ 0, -1]
        }
                
        return numpy.array(dct[articulationIndex], dtype=numpy.int16)
    
    
    @staticmethod
    def GetArticulationIndex(relativeCoords):
        dct = {
            ( 0,  0):   0,
            (-1, -1):   1,
            (-1,  0):   2,
            (-1,  1):   4,
            ( 0,  1):   8,
            ( 1,  1):  16,
            ( 1,  0):  32,
            ( 1, -1):  64,
            ( 0, -1): 128
        }
                
        return numpy.uint8(dct[relativeCoords])
    
    
    @staticmethod
    def GetReversedArticulationIndex(articulationIndex):
        dct = {
            0:   0,
            1:   16,
            2:   32,
            4:   64,
            8:   128,
            16:  1,
            32:  2,
            64:  4,
            128: 8
        }
                
        return numpy.uint8(dct[articulationIndex])
    
    @staticmethod
    def GetRowColIndices(matrix):
        r, c = numpy.shape(matrix)
        x, y = numpy.meshgrid(numpy.linspace(0,c-1,c), numpy.linspace(0,r-1,r))
        
        return x, y