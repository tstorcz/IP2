import math
import numpy
from matplotlib import pyplot as plt


class Histogram(object):

    @staticmethod
    def create_masked(source, mask, bins=numpy.arange(0, 256, 1)):
        if mask is not None:
            filtered_source = source[mask]
        else:
            filtered_source = source

        hist_full, _ = numpy.histogram(filtered_source, bins, density=False)

        return filtered_source, numpy.true_divide(hist_full, len(filtered_source.ravel()))

    @staticmethod
    def roll(histogram_probability, class_start, class_end):
        bin_count = len(histogram_probability)

        h1, h2 = Histogram.split(histogram_probability, class_start, class_end)

        ev1 = Histogram.__expected_index(h1)
        ev2 = Histogram.__expected_index(h2)
        dist_of_evs = abs(ev1-ev2)
        inv_dist_of_evs = bin_count - dist_of_evs

        if ev1 <= class_start <= ev2 or ev2 <= class_start <= ev1:
            if dist_of_evs <= inv_dist_of_evs:
                shift = class_end
            else:
                shift = class_start
        else:
            if dist_of_evs <= inv_dist_of_evs:
                shift = class_start
            else:
                shift = class_end

        histogram_probability = numpy.roll(histogram_probability, -shift)
        class_start = class_start - shift
        if class_start < 0:
            class_start += bin_count

        class_end = class_end - shift
        if class_end < 0:
            class_end += bin_count

        if class_end < class_start:
            t = class_end
            class_end = class_start
            class_start = t

        return histogram_probability, class_start, class_end

    @staticmethod
    def split(histogram, class_start, class_end):
        mask = Histogram.create_mask(histogram, class_start, class_end)
        class_0_hist = numpy.multiply(histogram.copy(), mask)
        class_1_hist = numpy.multiply(histogram.copy(), (1 - mask))

        return class_0_hist, class_1_hist

    @staticmethod
    def create_mask(histogram, class_start, class_end):
        mask = \
            [
                (0 if i < class_start or i > class_end else 1)
                for i in range(0, len(histogram))
            ]
        return numpy.asarray(mask)

    # Tests overlap of histograms having normal distribution
    @staticmethod
    def test_overlap(histogram: numpy.ndarray, class_start, class_end, circular_histogram=False):
        histogram_probability = histogram.astype(numpy.float)
        histogram_probability = histogram_probability / float(histogram.sum())

        if circular_histogram:
            histogram_probability, class_start, class_end = \
                Histogram.roll(histogram_probability, class_start, class_end)

        h1, h2 = Histogram.split(histogram_probability, class_start, class_end)

        ev1, var1 = Histogram.__normal_distribution_parameters(h1)
        ev2, var2 = Histogram.__normal_distribution_parameters(h2)

        sigma1 = math.sqrt(var1)
        sigma2 = math.sqrt(var2)

        reliability = [0, 0.68, 0.95, 0.99, 0.9999, 0.999999]

        for sigma_rule in range(1, len(reliability)):
            sec1 = [ev1 - sigma_rule * sigma1, ev1 + sigma_rule * sigma1]
            sec2 = [ev2 - sigma_rule * sigma2, ev2 + sigma_rule * sigma2]
            if Histogram.__section_intersection(sec1, sec2):
                return reliability[sigma_rule - 1]

        return reliability[-1]

    @staticmethod
    def __section_intersection(section1, section2):
        if section1[0] <= section2[0] <= section1[1] or section1[0] <= section2[1] <= section1[1] \
                or section2[0] <= section1[0] <= section2[1] or section2[0] <= section1[1] <= section2[1]:
            return True

        return False

    @staticmethod
    def __expected_index(histogram_probability):
        expected_value = 0
        for i in range(0, len(histogram_probability)):
            expected_value += i * histogram_probability[i]

        return expected_value

    @staticmethod
    def __normal_distribution_parameters(histogram_probability):
        expected_value = Histogram.__expected_index(histogram_probability)

        variance = 0
        for i in range(0, len(histogram_probability)):
            variance += histogram_probability[i] * (i - expected_value)**2

        return expected_value, variance

    @staticmethod
    def show_segmentation(histogram, class_start, class_end):
        bin_count = len(histogram)
        plt.figure()
        plt.bar(range(0, bin_count), histogram)
        plt.plot(
            range(0, bin_count),
            [(0 if i < class_start or i > class_end else histogram.max(initial=0)) for i in range(0, bin_count)],
            c="r"
        )
        plt.show()

    @staticmethod
    def get_valid_index(length, raw_index):
        while raw_index >= length:
            raw_index = raw_index - length

        while raw_index < 0:
            raw_index = raw_index + length

        return raw_index
