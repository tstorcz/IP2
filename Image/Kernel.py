import numpy

from Image.Transformer import Transformer


class Kernel(object):

    @staticmethod
    def create_kernel(kernel_type):
        if kernel_type == 'box_blur':
            return numpy.ones((3, 3)) * (1.0 / 9.0)

        if kernel_type == 'ones':
            return numpy.ones((3, 3))

        if kernel_type == 'gaussian':
            return Kernel.create_gaussian_kernel(3)

        if kernel_type == 'derivative_x':
            return numpy.asarray([[1.0, 0.0, -1.0], [2.0, 0.0, -2.0], [1.0, 1.0, -1.0]], numpy.float32)

        if kernel_type == 'derivative_y':
            return numpy.asarray([[1.0, 2.0, 1.0], [0.0, 0.0, 0.0], [-1.0, -2.0, -1.0]], numpy.float32)

        if kernel_type == 'border':
            return numpy.asarray([[1.0, 1.0, 1.0], [1.0, 10.0, 1.0], [1.0, 1.0, 1.0]], numpy.float32)

        if kernel_type == 'edge4':
            return numpy.asarray([[0.0, -1.0, 0.0], [-1.0, 4.0, -1.0], [0.0, -1.0, 0.0]], numpy.float32)

        if kernel_type == 'edge8':
            return numpy.asarray([[-1.0, -1.0, -1.0], [-1.0, 8.0, -1.0], [-1.0, -1.0, -1.0]], numpy.float32)

        if kernel_type == 'sharpen':
            return numpy.asarray([[0.0, -1.0, 0.0], [-1.0, 5.0, -1.0], [0.0, -1.0, 0.0]], numpy.float32)

        if kernel_type == 'emboss':
            return numpy.asarray([[-2.0, -1.0, 0.0], [-1.0, 1.0, 1.0], [0.0, 1.0, 2.0]], numpy.float32)

        if kernel_type == 'laplacian_inv4':
            return numpy.asarray([[0.0, -1.0, 0.0], [-1.0, 4.0, -1.0], [0.0, -1.0, 0.0]], numpy.float32)

        if kernel_type == 'laplacian_inv8':
            return numpy.asarray([[-1.0, -1.0, -1.0], [-1.0, 8.0, -1.0], [-1.0, -1.0, -1.0]], numpy.float32)

        if kernel_type == 'neighbour4' or kernel_type == 'cross':
            return numpy.asarray([[0.0, 1.0, 0.0], [1.0, 0.0, 1.0], [0.0, 1.0, 0.0]], numpy.float32)

        if kernel_type == 'neighbour8':
            return numpy.asarray([[1.0, 1.0, 1.0], [1.0, 0.0, 1.0], [1.0, 1.0, 1.0]], numpy.float32)

        if kernel_type == 'neighbour8dist':
            return numpy.asarray([[1.41, 1.0, 1.41], [1.0, 0.0, 1.0], [1.41, 1.0, 1.41]], numpy.float32)

        if kernel_type == 'articulationIndex':
            return numpy.asarray([[16.0, 32.0, 64.0], [8.0, 0.0, 128.0], [4.0, 2.0, 1.0]], numpy.float32)

        if kernel_type == 'reversedArticulationIndex':
            return numpy.asarray([[1.0, 2.0, 4.0], [128.0, 0.0, 8.0], [64.0, 32.0, 16.0]], numpy.float32)

        if kernel_type == 'pathItem':
            return numpy.asarray([[11.0, 12.0, 13.0], [21.0, 22.0, 23.0], [31.0, 32.0, 33.0]], numpy.float32)

        raise ValueError('Invalid kernelType parameter')

    @staticmethod
    def create_gaussian_kernel(size, fwhm=2, center=None):
        """ Make a square gaussian kernel.
        size is the length of a side of the square
        fwhm is full-width-half-maximum, which can be thought of as an effective radius.
        """

        x = numpy.arange(0, size, 1, float)
        y = x[:, numpy.newaxis]

        if center is None:
            x0 = y0 = size // 2
        else:
            x0 = center[0]
            y0 = center[1]

        kernel = numpy.exp(-4 * numpy.log(2) * ((x - x0) ** 2 + (y - y0) ** 2) / fwhm ** 2)

        return kernel / numpy.sum(kernel)

    @staticmethod
    def get_rotation_angles(from_angle, to_angle, step_count):
        angles = []
        angle_step = (to_angle - from_angle) / step_count
        while from_angle < to_angle:
            angles.append(from_angle)
            from_angle += angle_step

        return angles

    @staticmethod
    def rotate_kernel(kernel, rotation_angles, reshape=False, normalize=False, size=None):
        if size is None:
            size = kernel.shape

        top_left = [
            max(int(kernel.shape[0] / 2 - size[0] / 2), 0),
            max(int(kernel.shape[1] / 2 - size[1] / 2), 0)
        ]

        rotated_kernels = []

        # add rotated kernels
        for angle in rotation_angles:

            if angle == 0:
                rotated_kernel = kernel
            else:
                rotated_kernel = Transformer.rotate(kernel, angle, reshape=reshape)

            if normalize:
                rotated_kernel = numpy.true_divide(rotated_kernel, numpy.sum(numpy.abs(rotated_kernel)))

            rotated_kernels.append(
                rotated_kernel[
                    top_left[0]:top_left[0] + size[0],
                    top_left[1]:top_left[1] + size[1]
                ]
            )

        return rotated_kernels

    @staticmethod
    def zero_mean_kernels(kernels):
        zero_mean_kernels = []
        for kernel in kernels:
            zero_mean_kernels.append(kernel - numpy.true_divide(numpy.sum(kernel), kernel.size))
        return zero_mean_kernels

    @staticmethod
    def get_kernel_from_image(image, kernel_size_y, kernel_size_x, center_y, center_x):

        y = center_y - kernel_size_y // 2
        x = center_x - kernel_size_x // 2

        return image[y:y + kernel_size_y, x:x + kernel_size_x]

    @staticmethod
    def flip_kernels(kernel_list):
        flip = [slice(None, None, -1), slice(None, None, -1)]
        return [x[flip] for x in kernel_list]
