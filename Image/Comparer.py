
import numpy


class Comparer(object):

    @staticmethod
    def compare_absolute(gray_image_1, gray_image_2, max=0):
        diff = numpy.abs(numpy.subtract(gray_image_1, gray_image_2))

        if max > 0:
            diff = (diff / max) * 100

        return diff

    @staticmethod
    def compare_relative(gray_image_orig, gray_image_new):
        diff = numpy.abs(numpy.subtract(gray_image_orig, gray_image_new))

        temp = numpy.copy(gray_image_orig)
        temp[temp == 0] = numpy.min(temp[temp > 0])

        return numpy.divide(diff, temp) * 100

    @staticmethod
    def get_specific_predictions(binary_image_orig, binary_image_pred, prediction_validity):
        values = {
            "true_positive": [1, 1],
            "false_positive": [0, 1],
            "true_negative": [0, 0],
            "false_negative": [1, 0]
        }
        selected_predictions = numpy.zeros(binary_image_orig.shape)
        selected_predictions[(binary_image_orig == values[prediction_validity][0]) & (binary_image_pred == values[prediction_validity][1])] = 1

        return selected_predictions
