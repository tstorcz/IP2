from Image.ImageLoader import ImageLoader


class ImageData(object):

    def __init__(self, file_name=None, mode="<N/A>", data=None, crop_size=None):
        self.data = None
        self.file_mode = mode
        self.file_name = file_name if file_name is not None else ''

        if file_name is not None and data is None:
            self.data = ImageLoader.load_image_pixels(file_name, mode)

        if data is not None:
            self.data = data

        self.__filtered_data = {}
        self.__crop_size = self.data.shape if crop_size is None else crop_size

    def shape(self):
        return self.data.shape

    def add_filtered_data(self, filtered_data, key='filtered'):
        self.__filtered_data[key] = filtered_data

    def get_filtered_data(self, key=None):
        if key is not None:
            if key in self.__filtered_data:
                return self.__filtered_data[key]

        return self.data

    def remove_filtered_data(self, key):
        self.__filtered_data.pop(key)

    def crop(self, source_key=None, frame=None, center=None, size=None):

        if size is None:
            size = self.__crop_size

        if frame is None:
            if center is None or size is None:
                raise Exception("Missing parameter")

            y = center[0] - size[0] // 2
            x = center[1] - size[1] // 2
        else:
            y = frame[0][0]
            x = frame[0][1]
            size = (frame[0][0] - frame[1][0], frame[0][1] - frame[1][1])

        if source_key is None:
            source = self.data
        else:
            source = self.__filtered_data[source_key]

        return ImageData.crop_image(source, size, top_left=[y, x])

    @staticmethod
    def crop_image(image, size, top_left=None, center=None):
        if top_left is None:
            if center is None:
                top_left = [0, 0]
            else:
                top_left = [int(center[0] - size[0] // 2), int(center[1] - size[1] // 2)]

        for idx in range(0, 2):
            diff = image.shape[idx] - top_left[idx] - size[idx]

            if diff < 0:
                top_left[idx] += diff

            if top_left[idx] < 0:
                top_left[idx] = 0

        if len(image.shape) == 3:
            return image[top_left[0]:(top_left[0] + size[0]), top_left[1]:(top_left[1] + size[1]), :]
        else:
            return image[top_left[0]:(top_left[0] + size[0]), top_left[1]:(top_left[1] + size[1])]
