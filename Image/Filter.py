import math
import numpy

from scipy import fftpack
from scipy import interpolate
from scipy.ndimage import filters


class Filter(object):

    @staticmethod
    def convolution(image, kernel, iteration=1, mode='nearest', cval=0):
        filtered = image.astype(numpy.float)
        for _ in range(0, iteration):
            filtered = filters.convolve(filtered, kernel, mode=mode, cval=cval)

        return filtered

    @staticmethod
    def low_pass(image, threshold, cut_target_value=None):
        if cut_target_value is None:
            cut_target_value = threshold

        image[image >= threshold] = cut_target_value

    @staticmethod
    def high_pass(image, threshold, cut_target_value=None):
        if cut_target_value is None:
            cut_target_value = threshold

        image[image <= threshold] = cut_target_value

    @staticmethod
    def threshold(gray_image, threshold, low_value, high_value):
        binary_image = numpy.ones(gray_image.shape) * low_value
        binary_image[gray_image >= threshold] = high_value
        return binary_image

    @staticmethod
    def quantize(array, bin_count=100, max_value=1, min_value=0):
        return numpy.digitize(
            array,
            numpy.arange(min_value, max_value, float(max_value - min_value) / float(bin_count))
        )

    @staticmethod
    def normalize(array, max_value=255.0):
        if (array.max() - array.min()) != 0 and not numpy.isinf(array.min()):
            coeff = max_value / (array.max() - array.min())
            return ((array - array.min()) * coeff).astype(numpy.uint8)

        return array

    @staticmethod
    def project(array, from_range=None, to_range=(0, 1)):
        if from_range is None or from_range[1] is None:
            from_max = float(array.max())
        else:
            from_max = float(from_range[1])

        if from_range is None or from_range[0] is None:
            from_min = float(array.min())
        else:
            from_min = float(from_range[0])

        if (from_max - from_min) != 0 :
            coeff = (to_range[1] - to_range[0]) / (from_max - from_min)
            return ((array.astype(numpy.float) - from_min) * coeff) + to_range[0]

        return array - from_min

    @staticmethod
    def create_value_surface(source, region_counts,
                             value_function=numpy.max,
                             value_low_pass_threshold=float('inf'),
                             value_high_pass_threshold=0):
        x = 0
        x_step = math.ceil(source.shape[1] / region_counts[1])
        x_half_step = math.ceil(source.shape[1] / (2 * region_counts[1]))
        y_step = math.ceil(source.shape[0] / region_counts[0])
        y_half_step = math.ceil(source.shape[0] / (2 * region_counts[0]))
        x_coords = []
        y_coords = []
        values = []
        value = 0

        while x < source.shape[1]:
            y = 0
            while y < source.shape[0]:
                value = value_function(source[y:y + y_step, x:x + x_step])

                # top left
                if x == 0 and y == 0 \
                        and value_high_pass_threshold <= value <= value_low_pass_threshold:
                    x_coords.extend([0])
                    y_coords.extend([0])
                    values.extend([value])

                # left
                if x == 0 \
                        and value_high_pass_threshold <= value <= value_low_pass_threshold:
                    x_coords.extend([0])
                    y_coords.extend([y + y_half_step])
                    values.extend([value])

                # right
                if x + x_step >= source.shape[1] \
                        and value_high_pass_threshold <= value <= value_low_pass_threshold:
                    x_coords.extend([source.shape[1]])
                    y_coords.extend([y + y_half_step])
                    values.extend([value])

                # top right
                if x + x_step >= source.shape[1] and y == 0 \
                        and value_high_pass_threshold <= value <= value_low_pass_threshold:
                    x_coords.extend([source.shape[1]])
                    y_coords.extend([0])
                    values.extend([value])

                # top
                if y == 0 \
                        and value_high_pass_threshold <= value <= value_low_pass_threshold:
                    x_coords.extend([x + x_half_step])
                    y_coords.extend([0])
                    values.extend([value])

                # internal points
                if value_high_pass_threshold <= value <= value_low_pass_threshold:
                    x_coords.extend([x + x_half_step])
                    y_coords.extend([y + y_half_step])
                    values.extend([value])

                y = y + y_step

            # bottom
            if value_high_pass_threshold <= value <= value_low_pass_threshold:
                x_coords.extend([x + x_half_step])
                y_coords.extend([source.shape[0]])
                values.extend([value])

            # bottom left
            if x == 0 and value_high_pass_threshold <= value <= value_low_pass_threshold:
                x_coords.extend([0])
                y_coords.extend([source.shape[0]])
                values.extend([value])

            x = x + x_step

        # bottom right
        if value_high_pass_threshold <= value <= value_low_pass_threshold:
            x_coords.extend([source.shape[1]])
            y_coords.extend([source.shape[0]])
            values.extend([value])

        # x_coords = numpy.array(x_coords)
        # y_coords = numpy.array(y_coords)
        # values = numpy.array(values).astype(numpy.uint8)
        # plt.gray()
        # plt.imshow(source)
        # plt.scatter(
        #     x_coords,
        #     y_coords,
        #     c=values, s=40, edgecolor='black')
        # plt.show()

        coords = numpy.column_stack((numpy.array(y_coords), numpy.array(x_coords)))
        grid_y, grid_x = numpy.mgrid[0:source.shape[0], 0:source.shape[1]]
        return interpolate.griddata(coords, numpy.array(values), (grid_y, grid_x), method='linear')

    @staticmethod
    def grid_rescale_image(image_gray, blur_kernel_size, surface_grid_region_counts):
        if blur_kernel_size > 1:
            filtered = numpy.copy(image_gray)
            filtered = Filter.convolution(
                filtered,
                numpy.ones((blur_kernel_size, blur_kernel_size)) * (1.0 / math.pow(blur_kernel_size, 2))
            )
        else:
            filtered = image_gray

        threshold_surface = numpy.uint8(Filter.create_value_surface(filtered, surface_grid_region_counts))
        image_gray = numpy.true_divide(image_gray, threshold_surface) * 255.0
        image_gray[image_gray > 255] = 255
        image_gray[image_gray < 0] = 0

        return image_gray

    @staticmethod
    def intensity_bands(image, threshold_vector):
        # TODO: handle threshold vector, starting from value 1/high/pass/true
        return image

    @staticmethod
    def cut_constant_frequency(array):
        f = fftpack.fft2(array)
        f[0, 0] = 0
        return fftpack.ifft2(f).real
