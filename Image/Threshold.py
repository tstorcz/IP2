import math
import numpy


class Threshold(object):

    # RETURNS:
    #   start_threshold, end_threshold, inter_class_variance
    # start_threshold is always 0
    # start_threshold <= class < end_threshold
    @staticmethod
    def otsu(histogram):
        probability = histogram / numpy.sum(histogram)

        return (0,) + Threshold.__otsu_of_density(probability)

    # The method returns: start_threshold, end_threshold, inter_class_variance
    # start_threshold <= class < end_threshold
    @staticmethod
    def otsu_cyclic(histogram, step=1):
        probability = histogram / numpy.sum(histogram)
        length = len(probability)

        class_0_start = 0
        class_0_end = 1
        interclass_variance_max = -1
        center_dist = -1
        start = 0
        while start < length:
            rolled_probability = numpy.roll(probability, start)
            threshold, interclass_variance = \
                Threshold.__otsu_of_density(rolled_probability)

            max_1 = numpy.nonzero(rolled_probability[:threshold] == max(rolled_probability[:threshold]))[0][
                        0] + start
            mean_1 = numpy.average(numpy.arange(0, threshold), weights=rolled_probability[:threshold])
            d_1 = math.fabs(max_1 - mean_1)

            max_2 = numpy.nonzero(rolled_probability[threshold:] == max(rolled_probability[threshold:]))[0][
                        0] + start + threshold
            mean_2 = numpy.average(numpy.arange(threshold, length), weights=rolled_probability[threshold:])
            d_2 = math.fabs(max_2 - mean_2)

            c_d = math.sqrt(d_1**2+d_2**2)

            if center_dist == -1 or center_dist > c_d:
                center_dist = c_d

                class_0_start = Threshold.__get_valid_index(length, -start)
                class_0_end = Threshold.__get_valid_index(length, threshold - start)
                interclass_variance_max = interclass_variance

            start = start + step

        if class_0_start > class_0_end:
            t = class_0_start
            class_0_start = class_0_end
            class_0_end = t

        return class_0_start, class_0_end, interclass_variance_max

    @staticmethod
    def between_first_and_max(histogram, first=None):
        if first is None:
            first = numpy.where(histogram > 0)[0][0]
        return \
            first, \
            round((numpy.where(histogram == max(histogram))[0][0] - first) / 2), \
            0

    # The method returns: end_threshold, inter_class_variance_max
    # class < end_threshold
    @staticmethod
    def __otsu_of_density(probability):
        length = len(probability)

        sum_0 = 0
        sum_1 = probability.sum()
        prod_0 = 0
        prod_full = numpy.arange(0, length).astype(numpy.float).dot(probability)

        threshold = 0
        interclass_variance_max = -1

        for t in range(1, length):
            sum_0 = sum_0 + probability[t - 1]
            sum_1 = sum_1 - probability[t - 1]
            prod_0 = prod_0 + probability[t - 1] * (t - 1)
            prod_1 = prod_full - prod_0

            if sum_0 > 0 and sum_1 > 0:
                var_inter = sum_0 * sum_1 * ((prod_0 / sum_0) - prod_1 / sum_1) ** 2

                if var_inter > interclass_variance_max or interclass_variance_max == -1:
                    interclass_variance_max = var_inter
                    threshold = t

        return threshold, interclass_variance_max

    @staticmethod
    def __get_valid_index(length, raw_index):
        while raw_index >= length:
            raw_index = raw_index - length

        while raw_index < 0:
            raw_index = raw_index + length

        return raw_index