# part of PILLOW.PIL module
import ExifTags
import numpy

from PIL import Image


class ImageLoader(object):
    IMAGE_EXTENSION_LIST = [".jpg", ".png"]

    # Modes: http://pillow.readthedocs.io/en/latest/handbook/concepts.html#concept-modes
    @staticmethod
    def load_image_pixels(image_file_name, mode='RGB'):
        image = Image.open(image_file_name)
        image = image.convert(mode)
        return ImageLoader.get_image_pixels(image)

    @staticmethod
    def get_image_rotation_angle(image_file_name):
        image = Image.open(image_file_name)
        portrait = image.size[0] < image.size[1]
        rotation = 0
        try:
            if image._getexif() is not None:
                orientation = -1
                for orientation in ExifTags.TAGS.keys():
                    if ExifTags.TAGS[orientation] == 'Orientation':
                        break

                exif = dict(image._getexif().items())

                if orientation >= 0 and orientation in exif:
                    if exif[orientation] == 3:
                        rotation = 180
                    elif exif[orientation] == 6:
                        rotation = 270
                        portrait = not portrait
                    elif exif[orientation] == 8:
                        rotation = 90
                        portrait = not portrait
        finally:
            rotation_of_oriented_image = 0 if portrait else 90
            raw_rotation = (rotation + rotation_of_oriented_image) % 360

            return raw_rotation, rotation_of_oriented_image

    @staticmethod
    def load_oriented_image_pixels(image_file_name, mode='RGB'):
        image = Image.open(image_file_name)
        try:
            if image._getexif() is not None:
                orientation = -1
                for orientation in ExifTags.TAGS.keys():
                    if ExifTags.TAGS[orientation] == 'Orientation':
                        break

                exif = dict(image._getexif().items())

                if orientation >= 0 and orientation in exif:
                    if exif[orientation] == 3:
                        image = image.rotate(180, expand=True)
                    elif exif[orientation] == 6:
                        image = image.rotate(270, expand=True)
                    elif exif[orientation] == 8:
                        image = image.rotate(90, expand=True)
        finally:
            image = image.convert(mode)

        return ImageLoader.get_image_pixels(image)

    @staticmethod
    def get_image_pixels(image):
        width, height = image.size
        lst = list(image.getdata())

        if type(lst[0]) is tuple:
            pixels = numpy.array(lst, dtype=numpy.uint8).reshape((height, width, len(lst[0])))
        else:
            pixels = numpy.array(lst, dtype=numpy.uint8).reshape((height, width))
        return pixels
