import numpy
import os

from matplotlib import pyplot as plt
from PIL import Image
from Tools.Files.FileSystem import FileSystem


class ImageOut(object):
    IMAGE_EXTENSION_LIST = [".jpg", ".png"]

    # Modes: https://pillow.readthedocs.io/en/3.1.x/handbook/concepts.html#concept-modes
    @staticmethod
    def create_image_from_data(array, mode='None'):
        return Image.fromarray(array.astype(numpy.uint8), mode)

    @staticmethod
    def save_image(array, file_name, subfolder=None, mode='None'):
        if mode == "None":
            raise Exception('Please set ImageOut.save_image mode parameter according to documentation!')

        if subfolder is not None:
            file_name = FileSystem.add_subfolder(file_name, subfolder)
        im = ImageOut.create_image_from_data(array.astype(numpy.uint8), mode)
        im.save(file_name)

    @staticmethod
    def show_images(images, cmap='gray', colorbar=False):
        if isinstance(images[0], list):
            rows = len(images)
            cols = len(images[0])
        else:
            rows = len(images)
            cols = 1

        fig = plt.figure()
        index = 1
        for row in images:
            if isinstance(row, list):
                for image in row:
                    fig.add_subplot(rows, cols, index)
                    ImageOut.__show_single_image(
                        index,
                        image,
                        cmap,
                        colorbar
                    )
                    index = index + 1
            else:
                fig.add_subplot(rows, cols, index)
                ImageOut.__show_single_image(
                    index,
                    row,
                    cmap,
                    colorbar
                )
                index = index + 1

        plt.show()

    @staticmethod
    def show_histogram(data, bin_count=100, file_name=None):
        plt.hist(data.ravel(), bins=bin_count)
        if file_name is not None:
            plt.savefig(file_name)
            plt.clf()
        else:
            plt.show()

    @staticmethod
    def show_image_with_data(image, sections=None, colors=None, points=None, show_section_center=False):
        fig, ax = plt.subplots()
        ax.imshow(image, cmap='gray')

        if sections is not None:
            if not isinstance(sections[0], list):
                sections = [sections]

            if colors is None:
                colors = ['firebrick']

            for idx in range(0, len(sections)):
                for line_index in range(0, len(sections[idx])):
                    line = sections[idx][line_index]

                    ax.plot(
                        line.coordinates(1), line.coordinates(0),
                        linewidth=2, color=colors[idx % len(colors)]
                    )

                    if show_section_center:
                        plt.text(line.center[1], line.center[0], line_index,
                                 fontsize=6,
                                 horizontalalignment='center', verticalalignment='center',
                                 bbox=dict(facecolor='red', alpha=0.5))

        if points is not None:
            x_list = [i[1] for i in points]
            y_list = [i[0] for i in points]
            ax.plot(x_list, y_list, marker='o', color='r', ls='')

        plt.show()

    @staticmethod
    def create_thumbnail(src_image_file_name_with_path,
                         dst_path=None, dst_file_name=None,
                         magnitude=None, x_size=None, y_size=None):

        path, name, ext = FileSystem.split_file_name(src_image_file_name_with_path)
        if dst_path is None:
            dst_path = path

        if dst_file_name is None:
            dst_file_name = name

        dst_file_name = dst_file_name.replace("<name>", name)
        dst_file_name = dst_file_name.replace("<ext>", ext.replace('.', ''))

        out_file_name = os.path.join(dst_path, dst_file_name)

        if magnitude is not None and (x_size is not None or y_size is not None):
            raise ValueError("Both Magnitude and Size set.")

        if magnitude is None and x_size is None and y_size is None:
            raise ValueError("Magnitude or Size has to be set.")

        if src_image_file_name_with_path != out_file_name:
            try:
                im = Image.open(src_image_file_name_with_path)

                if magnitude is not None:
                    new_size = tuple([round(z * magnitude) for z in im.size])
                else:
                    new_size = tuple(
                        [
                            x_size if x_size is not None else im.size[0] * im.size[1] / y_size,
                            y_size if y_size is not None else im.size[1] * im.size[0] / x_size
                        ]
                    )

                im.thumbnail(new_size, Image.ANTIALIAS)
                im.save(out_file_name, "JPEG")
            except IOError as ex:
                print("cannot create thumbnail for '{src_image_file_name_with_path}'\n{ex}")
                return None

        return out_file_name

    @staticmethod
    def get_parts(src_image_file_name_with_path, boxes):
        parts = []
        im = Image.open(src_image_file_name_with_path)
        for box in boxes:
            parts.append(im.crop(box))

        return parts

    @staticmethod
    def get_part(src_image_file_name_with_path, rectangle):
        im = Image.open(src_image_file_name_with_path)
        return im.crop((rectangle[0], rectangle[1], rectangle[0] + rectangle[2], rectangle[1] + rectangle[3]))

    @staticmethod
    def __show_image_row(index_base, image_row, cmap, colorbar):
        index_offset = index_base
        for image in image_row:
            ImageOut.__show_single_image(
                index_offset,
                image,
                cmap,
                colorbar
            )
            index_offset = index_offset + 1

    @staticmethod
    def __show_single_image(index, image, cmap, colorbar):
        plt.imshow(image, cmap=cmap)
        if colorbar:
            plt.colorbar()
