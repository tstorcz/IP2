import math
import numpy

from matplotlib import pyplot as plt

from Image.Histogram import Histogram
from MachineLearning.KMeans import KMeans


class Segment(object):

    # min_pass_count is the number of required passes of a pixel values (multichannel threshold)
    @staticmethod
    def create_threshold_mask(source, thresholds, min_pass_count=1):
        if len(source.shape) == 2:
            mask = numpy.zeros(source.shape, numpy.uint8)
            mask[source > thresholds[0]] = 1

        else:
            mask = numpy.zeros(source.shape[:2], numpy.uint8)

            for i in range(len(thresholds)):
                mask[source[:, :, i] > thresholds[i]] = mask[source[:, :, i] > thresholds[i]] + 1

        return mask >= min_pass_count

    @staticmethod
    def create_distance_mask(source, weights, threshold):
        source32 = source.astype(numpy.int32)
        if len(source.shape) == 2:
            mask = numpy.zeros(source.shape, numpy.uint8)
            mask[(source32 * weights[0]) > threshold] = 1

        else:
            mask = numpy.zeros(source.shape[:2], numpy.uint8)

            sq = numpy.square(source32)
            out = numpy.zeros(mask.shape, numpy.int32)
            for i in range(source.shape[2]):
                out = numpy.add(out, sq[:, :, i] * weights[i])

            dist = numpy.sqrt(out)

            mask[dist >= threshold] = 1

        return mask > 0

    @staticmethod
    def histogram_threshold_with_frame(source, threshold_method, frame=None, mask=None):
        if frame is None:
            frame = [10, 10]

        frame_max_index = Segment.__get_frame_max_index(source, frame, mask)

        binary, class_0_start, class_0_end, _ = Segment.histogram_threshold(source, threshold_method, mask)

        # invert selection if frame is in class_0
        if (class_0_start < class_0_end and class_0_start <= frame_max_index <= class_0_end) \
                or \
                (class_0_start > class_0_end and (class_0_start <= frame_max_index or frame_max_index <= class_0_end)):
            binary = 1 - binary

            if mask is not None:
                binary[not mask] = 0

            temp = class_0_start
            class_0_start = class_0_end
            class_0_end = temp

        return binary, class_0_start, class_0_end

    @staticmethod
    def histogram_threshold(source, threshold_method, mask=None):
        filtered_source, hist_probabilities = Histogram.create_masked(source, mask)

        class_1_start, class_1_end, _ = threshold_method(hist_probabilities)

        binary = numpy.zeros(source.shape[:2], numpy.uint8)

        if class_1_start > class_1_end:
            # class 1 is out of class_1_start...class_1_end interval
            binary[(source < class_1_start) | (class_1_end < source)] = 1
        else:
            # class 1 is between class_1_start and class_1_end
            binary[(class_1_start <= source) & (source <= class_1_end)] = 1

        if mask is not None:
            binary[numpy.logical_not(mask)] = 0

        return binary, class_1_start, class_1_end, hist_probabilities

    @staticmethod
    def get_histogram_segmentation_weights_by_distance(
            source, classified,
            class_start, class_end, bin_count=256,
            is_cyclic=False
    ):
        weight = numpy.minimum(
            numpy.abs(numpy.subtract(source, class_start)),
            numpy.abs(numpy.subtract(source, class_end))
        ).astype(numpy.float)

        if is_cyclic:
            weight = numpy.minimum(
                weight,
                numpy.abs(numpy.subtract(source, class_start + bin_count))
            )

            weight = numpy.minimum(
                weight,
                numpy.abs(numpy.subtract(source, class_end + bin_count))
            )

            weight = numpy.minimum(
                weight,
                numpy.abs(numpy.subtract(source, class_start - bin_count))
            )

            weight = numpy.minimum(
                weight,
                numpy.abs(numpy.subtract(source, class_end - bin_count))
            )

        weight[classified == 1] = (weight / ((class_end - class_start) / 2.0))[classified == 1]
        weight[classified == 0] = (weight / ((bin_count - class_end + class_start) / 2.0))[classified == 0]

        return weight

    @staticmethod
    def get_histogram_segmentation_weights_by_normalized_distance(source, classified):

        weight = numpy.zeros(source.shape).astype(numpy.float)

        source0 = source[classified == 0]
        avg_0 = source0.mean()
        var_0 = source0.var()
        weight[classified == 0] = (source0 - avg_0) / var_0

        source1 = source[classified == 1]
        avg_1 = source1.mean()
        var_1 = source1.var()
        weight[classified == 1] = (source1 - avg_1) / var_1

        return numpy.abs(weight)

    @staticmethod
    def __normalize_class(weight, source, classified, class_condition):
        source = source[classified == class_condition]

        avg = source.mean()
        var = math.sqrt(source.var())
        weight[classified == class_condition] = (source - avg) / var

    @staticmethod
    def create_frame_mask(shape, frame_size):
        if not isinstance(frame_size, list):
            frame_size = [frame_size, frame_size]
        frame_mask = numpy.ones(shape, numpy.uint8)
        frame_mask[frame_size[0]:shape[0] - frame_size[0], frame_size[1]:shape[1] - frame_size[1]] = 0

        return frame_mask

    @staticmethod
    def __get_frame_max_index(source, frame=None, mask=None):
        if frame is None:
            frame = [10, 10]

        frame_mask = Segment.create_frame_mask(source.shape[:2], frame) > 0

        if mask is not None:
            masked_image = source[frame_mask & mask]
        else:
            masked_image = source[frame_mask]

        hist_frame, _ = numpy.histogram(masked_image, bins=numpy.arange(0, 256, 1), density=False)

        # smooth histogram shape and find max position
        hist_frame = numpy.convolve(hist_frame, [1, 1, 1])
        frame_max_index = numpy.argmax(hist_frame)

        return frame_max_index

    @staticmethod
    def rgb_threshold(rgb_image, box_width=10.0):
        (x_size_orig, y_size_orig, z_size_orig) = rgb_image.shape
        box_item_count_threshold = box_width * box_width * box_width

        boxed_rgb_image = (rgb_image / box_width).astype(int)
        hist_size = int(math.ceil(255 / box_width)) + 1
        rgb_hist = numpy.zeros((hist_size, hist_size, hist_size))
        (x_size, y_size, z_size) = boxed_rgb_image.shape

        for i in range(0, x_size):
            for j in range(0, y_size):
                rgb_hist[boxed_rgb_image[i, j, 0], boxed_rgb_image[i, j, 1], boxed_rgb_image[i, j, 2]] += 1

        (x_indices, y_indices, z_indices) = numpy.where(rgb_hist > box_item_count_threshold)
        color = rgb_hist[rgb_hist > box_item_count_threshold]

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(x_indices, y_indices, z_indices, c=color, marker='o', cmap=plt.hot())
        plt.show()

        a_index = 0
        b_index = 0
        max_distance = 0
        for i in range(0, len(x_indices)):
            for j in range(0, len(x_indices)):
                v = (
                    x_indices[j] - x_indices[i],
                    y_indices[j] - y_indices[i],
                    z_indices[j] - z_indices[i]
                )
                distance = math.sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2])
                if max_distance < distance:
                    max_distance = distance
                    a_index = i
                    b_index = j

        binary = numpy.zeros(rgb_image.shape[:2], numpy.uint8)
        binary_weight = numpy.zeros(rgb_image.shape[:2], numpy.double)

        kmeans = KMeans(2, 100)

        kmeans.centroids = [
            [x_indices[a_index] * box_width, y_indices[a_index] * box_width, z_indices[a_index] * box_width],
            [x_indices[b_index] * box_width, y_indices[b_index] * box_width, z_indices[b_index] * box_width]
        ]
        kmeans.fit(numpy.reshape(rgb_image.flatten(), [209200, 3]))

        distance = math.sqrt(
            math.pow(kmeans.centroids[0][0] - kmeans.centroids[1][0], 2)
            + math.pow(kmeans.centroids[0][1] - kmeans.centroids[1][1], 2)
            + math.pow(kmeans.centroids[0][2] - kmeans.centroids[1][2], 2)
        ) / 2.0

        for i in range(0, x_size_orig):
            for j in range(0, y_size_orig):
                v_a = (
                    kmeans.centroids[0][0] - rgb_image[i, j, 0],
                    kmeans.centroids[0][1] - rgb_image[i, j, 1],
                    kmeans.centroids[0][2] - rgb_image[i, j, 2]
                )
                v_b = (
                    kmeans.centroids[1][0] - rgb_image[i, j, 0],
                    kmeans.centroids[1][1] - rgb_image[i, j, 1],
                    kmeans.centroids[1][2] - rgb_image[i, j, 2]
                )
                distance_a = math.sqrt(v_a[0] * v_a[0] + v_a[1] * v_a[1] + v_a[2] * v_a[2])
                distance_b = math.sqrt(v_b[0] * v_b[0] + v_b[1] * v_b[1] + v_b[2] * v_b[2])

                if distance_a > distance_b:
                    binary[i, j] = 1

                binary_weight[i, j] = math.fabs(distance_a - distance_b) / distance

        return binary, binary_weight

    @staticmethod
    def get_valid_index(length, raw_index):
        while raw_index >= length:
            raw_index = raw_index - length

        while raw_index < 0:
            raw_index = raw_index + length

        return raw_index

    @staticmethod
    def cyclic_expected_value(probability, start, length):
        ev = 0
        while length > 0:
            ev = ev + (probability[start] * start)
            length = length - 1
            start = Segment.get_valid_index(len(probability), start + 1)
        return ev

    @staticmethod
    def cyclic_sigma(probability, start, length):
        expected_value = Segment.cyclic_expected_value(probability, start, length)
        sigma = 0
        while length > 0:
            sigma = sigma + (probability[start] * (start - expected_value) ** 2)
            length = length - 1
            start = Segment.get_valid_index(len(probability), start + 1)
        return sigma

    @staticmethod
    def to_binary(image, threshold, high_value=1, invert=False):
        temp = numpy.copy(image)
        temp[temp < threshold] = 0
        temp[temp >= threshold] = high_value

        if invert:
            temp = 255 - temp

        return temp
