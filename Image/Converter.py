import cv2
import numpy
import numpy as np
import tensorflow as tf

from Image.Filter import Filter
from Image.Kernel import Kernel
from Image.Segment import Segment


class Converter(object):
    # Image types: https://docs.opencv.org/3.2.0/d7/d1b/group__imgproc__misc.html#ga4e0972be5de079fed4e3a10e24ef5ef0
    @staticmethod
    def convert_image_data_from_rgb(image, target_type, output_type=numpy.float):
        conversion_mode = 0

        if target_type == 'HSV':
            conversion_mode = cv2.COLOR_RGB2HSV_FULL

        if target_type == 'Lab':
            conversion_mode = cv2.COLOR_RGB2Lab

        if target_type == 'YCrCb':
            conversion_mode = cv2.COLOR_RGB2YCrCb

        if target_type == 'BGR':
            conversion_mode = cv2.COLOR_RGB2BGR

        if target_type == 'Luv':
            conversion_mode = cv2.COLOR_RGB2Luv

        if target_type == 'XYZ':
            conversion_mode = cv2.COLOR_RGB2XYZ

        if target_type == 'gray':
            conversion_mode = cv2.COLOR_BGR2GRAY

        if conversion_mode != 0:
            return cv2.cvtColor(image, conversion_mode)
        else:
            if target_type == 'lightness':
                temp = image.max(2).astype(output_type)
                temp = numpy.add(temp, image.min(2))
                return numpy.uint8(temp / 2.0)

            if target_type == 'intensity':
                return numpy.uint8(image.sum(axis=2, dtype=output_type) / 3.0)

        return image

    @staticmethod
    def convert_image_data_to_rgb(image, source_type):
        conversion_mode = 0

        if source_type == 'HSV':
            conversion_mode = cv2.COLOR_HSV2RGB_FULL

        if source_type == 'Lab':
            conversion_mode = cv2.COLOR_Lab2RGB

        if source_type == 'YCrCb':
            conversion_mode = cv2.COLOR_YCrCb2RGB

        if source_type == 'BGR':
            conversion_mode = cv2.COLOR_BGR2RGB

        if source_type == 'Luv':
            conversion_mode = cv2.COLOR_Luv2RGB

        if source_type == 'XYZ':
            conversion_mode = cv2.COLOR_XYZ2RGB

        if source_type == 'grayscale' or source_type == 'gray':
            conversion_mode = cv2.COLOR_GRAY2RGB

        if conversion_mode != 0:
            return cv2.cvtColor(image, conversion_mode)
        else:
            return image

    @staticmethod
    def get_image_gradient_intensities(image):
        grad_gray_x = Filter.convolution(image, Kernel.create_kernel('derivative_x'))
        grad_gray_y = Filter.convolution(image, Kernel.create_kernel('derivative_y'))
        return numpy.sqrt(numpy.add(numpy.power(grad_gray_x, 2), numpy.power(grad_gray_y, 2)))

    @staticmethod
    def get_image_gradient_directions(image):
        grad_gray_x = Filter.convolution(image, Kernel.create_kernel('derivative_x'))
        grad_gray_y = Filter.convolution(image, Kernel.create_kernel('derivative_y'))
        return numpy.arctan2(grad_gray_y, grad_gray_x)

    @staticmethod
    def get_image_gradient_data(image):
        grad_gray_x = Filter.convolution(image, Kernel.create_kernel('derivative_x'))
        grad_gray_y = Filter.convolution(image, Kernel.create_kernel('derivative_y'))
        return \
            numpy.sqrt(numpy.add(numpy.power(grad_gray_x, 2), numpy.power(grad_gray_y, 2))), \
            numpy.arctan2(grad_gray_y, grad_gray_x)

    @staticmethod
    def get_binary_image_from_rgb(image, number_of_quants=255):
        image_gray = Converter.convert_image_data_from_rgb(
            image,
            'intensity'
        )
        return Segment.to_binary(image_gray / number_of_quants, 0.5, 1)

    @staticmethod
    def get_binary_image_from_gray(image_gray, number_of_quants=255):
        return Segment.to_binary(image_gray / number_of_quants, 0.5, 1)

    @staticmethod
    def get_color_categories(hsv_image,
                             number_of_color_categories, number_of_intensity_categories=2,
                             min_saturation=10,
                             max_value=255):
        d_color = max_value / number_of_color_categories
        s = d_color / 2
        categories = np.zeros(hsv_image.shape, dtype=np.int8)
        categories[hsv_image[0] < s] = 0
        categories[hsv_image[0] >= max_value - s] = 0

        for i in range(0, number_of_color_categories - 1):
            categories[(hsv_image[0] >= s + i * d_color) & (hsv_image[0] < s + (i + 1) * d_color)] = i

        if min_saturation > 0:
            d_intensity = max_value / number_of_intensity_categories

            for i in range(0, number_of_intensity_categories - 1):
                categories[
                    (hsv_image[1] < min_saturation)
                    & (hsv_image[2] >= i * d_intensity)
                    & (hsv_image[2] < (i + 1) * d_intensity)
                ] = number_of_color_categories + i

            categories[
                (hsv_image[1] < min_saturation)
                & (hsv_image[2] >= max_value - d_intensity)
                ] = number_of_color_categories + number_of_intensity_categories - 1

    @staticmethod
    def one_hot_encoder(index_array, class_count):
        return tf.one_hot(index_array, class_count)
