import numpy
from pyzernikemoment import Zernikemoment


class Moments(object):

    def __init__(self, image):
        self.Image = numpy.zeros(numpy.shape(image), dtype=numpy.uint8)
        if image.dtype == numpy.bool:
            self.Image[image == True] = 255
        else:
            self.Image[image > 0] = 255
    
    def moments(self):
        moments = {}
        r, c = self.Image.shape
        xv = numpy.linspace(0, c-1, c)
        yv = numpy.linspace(0, r-1, r)
        x, y = numpy.meshgrid(xv, yv)
        
        xi = x*self.Image
        yi = y*self.Image
        x2 = x**2
        y2 = y**2
        
        # raw or spatial moments
        moments['m00'] = self.Image.sum()
        moments['m01'] = xi.sum()
        moments['m10'] = yi.sum()
        moments['m11'] = (y*xi).sum()
        moments['m02'] = (x2*self.Image).sum()
        moments['m20'] = (y2*self.Image).sum()
        moments['m12'] = (y2*xi).sum()
        moments['m21'] = (x2*yi).sum()
        moments['m03'] = (x**3*self.Image).sum()
        moments['m30'] = (y**3*self.Image).sum()
        
        moments['mean_x'] = moments['m10']/moments['m00']
        moments['mean_y'] = moments['m01']/moments['m00']
        
        # central moments
        moments['mu00'] = moments['m00'] 
        # moments['mu01']= ((y-moments['mean_y'])*image).sum()                                 # should be 0
        # moments['mu10']= ((x-moments['mean_x'])*image).sum()                                 # should be 0
        dx = x-moments['mean_x']
        dy = y-moments['mean_y']
        dxi = dx * self.Image
        dx2 = dx**2
        dy2 = dy**2
        moments['mu11'] = (dy * dxi).sum()
        moments['mu02'] = (dy2 * self.Image).sum()
        moments['mu20'] = (dx2 * self.Image).sum()
        moments['mu12'] = (dy2 * dxi).sum()
        moments['mu21'] = (dx2 * dy * self.Image).sum()
        moments['mu03'] = (dy**3 * self.Image).sum()
        moments['mu30'] = (dx**3 * self.Image).sum()
        
        # helpers
        mu00exp20 = moments['mu00']**2.0 #(2/2+1)
        mu00exp25 = moments['mu00']**2.5 #(3/2+1)
        
        # central standardized or normalized or scale invariant moments
        moments['nu11'] = moments['mu11'] / mu00exp20
        moments['nu12'] = moments['mu12'] / mu00exp25
        moments['nu21'] = moments['mu21'] / mu00exp25
        moments['nu20'] = moments['mu20'] / mu00exp20
        moments['nu02'] = moments['mu02'] / mu00exp20
        moments['nu03'] = moments['mu03'] / mu00exp25
        moments['nu30'] = moments['mu30'] / mu00exp25
        return moments
        
    def hu(self):
        moments = self.moments()
        
        hu1 = moments['nu20'] + moments['nu02']
        hu2 = (moments['nu20'] - moments['nu02'])**2 + 4*(moments['nu11']**2)
        hu3 = (moments['nu30'] - 3*moments['nu12'])**2 + (3*moments['nu21'] - moments['nu03'])**2
        hu4 = (moments['nu30'] + moments['nu12'])**2 + (moments['nu21'] + moments['nu03'])**2
        hu5 = (moments['nu30'] - 3*moments['nu12']) * (moments['nu30'] + moments['nu12']) * ((moments['nu30'] + moments['nu12'])**2 - 3*(moments['nu21'] + moments['nu03'])**2) \
                       + (3*moments['nu21'] - moments['nu03']) * (moments['nu21'] + moments['nu03']) * (3*(moments['nu30'] + moments['nu12'])**2 - (moments['nu21'] + moments['nu03'])**2)
        hu6 = (moments['nu20'] - moments['nu02']) * ((moments['nu30'] + moments['nu12'])**2 - (moments['nu21'] + moments['nu03'])**2) \
                       + 4*moments['nu11']*(moments['nu30'] + moments['nu12'])*(moments['nu21'] + moments['nu03'])
        hu7 = (3*moments['nu21'] - moments['nu03']) * (moments['nu30'] + moments['nu12']) * ((moments['nu30'] + moments['nu12'])**2 - 3*(moments['nu21'] + moments['nu03'])**2) \
                       - (moments['nu30'] - 3*moments['nu12']) * (moments['nu21'] + moments['nu03']) * (3*(moments['nu30'] + moments['nu12'])**2 - (moments['nu21'] + moments['nu03'])**2)
        hu8 = moments['nu11'] * ((moments['nu30'] + moments['nu12'])**2 - (moments['nu03'] + moments['nu21'])**2) \
                       - (moments['nu20'] - moments['nu02'])*(moments['nu30'] + moments['nu12'])*(moments['nu03'] + moments['nu21'])
        
        return [hu1, hu2, hu3, hu4, hu5, hu6, hu7, hu8]
    
    def zernike(self, order):
        moments = []
        for i in range(order, -1, -2):
            zmre, zmim = Zernikemoment(self.Image, order, i)
            moments.append(zmre)
            moments.append(zmim)
        return moments
