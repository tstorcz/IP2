import numpy

from Image.Filter import Filter
from Image.Kernel import Kernel


class Thinner(object):
    is_articulation_list = numpy.array([
        0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0,
        0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0,
        1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0,
        0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0,
        0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ], dtype=numpy.uint8)

    @staticmethod
    def large_scale_thinning(binary_image):
        return Thinner.__thinning(binary_image, 1)

    @staticmethod
    def medium_scale_thinning(binary_image):
        return Thinner.__thinning(binary_image, 2)

    @staticmethod
    def small_scale_thinning(binary_image):
        return Thinner.__thinning(binary_image, 3)

    @staticmethod
    def multi_scale_thinning(binary_image):
        ls = Thinner.__thinning(binary_image, 1)
        ms = Thinner.__thinning(ls, 2)
        return Thinner.__thinning(ms, 3)

    @staticmethod
    def __thinning(binary_image, scale):
        skeleton = binary_image.copy()
        one_more_step = True

        while one_more_step:
            neighbour_count = Filter.convolution(skeleton, Kernel.create_kernel('neighbour4'), 1)

            neighbour_count[skeleton == 0] = 0

            interior_mask = numpy.zeros(numpy.shape(skeleton))
            interior_mask[neighbour_count == (5 - scale)] = 1

            one_more_step = Thinner._remove_boundary_pixels(skeleton, interior_mask)

        return skeleton

    @staticmethod
    def _remove_boundary_pixels(image, interior_mask, min_removed_count=20):
        """
        Not interior, but has at least 1 neighbor, not articulation
        """
        neighbour_count = Filter.convolution(interior_mask, Kernel.create_kernel('neighbour8'), 1)
        neighbour_count[image == 0] = 0

        mask = numpy.zeros(numpy.shape(image), dtype=numpy.uint8)
        mask[numpy.logical_and(neighbour_count > 0, interior_mask == 0)] = 1

        articulation = Filter.convolution(image, Kernel.create_kernel('reversedArticulationIndex'), 1)

        remove_indices = (mask == 1) & (Thinner.is_articulation_list[articulation.astype(numpy.uint8)] == 0)
        image[remove_indices] = 0

        return remove_indices[remove_indices].size >= min_removed_count

    @staticmethod
    def get_submatrix(image, top, left, rows, cols):
        (imageR, imageC) = numpy.shape(image)
        sr = 0 if top >= 0 else 1
        sc = 0 if left >= 0 else 1
        sub = image[max(top, 0):min(imageR, top + rows), max(left, 0):min(imageC, left + cols)]
        (subR, subC) = numpy.shape(sub)
        frame = numpy.zeros((rows, cols))
        frame[sr:sr + subR, sc:sc + subC] = sub
        return frame

    @staticmethod
    def _is_articulation(image_part):
        index = numpy.int32(numpy.multiply(image_part, Kernel.create_kernel('articulationIndex')).sum())

        return Thinner.is_articulation_list[index] == 1
