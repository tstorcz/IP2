import cv2
import numpy
from PIL import Image

from Image.Filter import Filter
from Image.Kernel import Kernel


class EdgeDetection(object):

    @staticmethod
    def canny(image, threshold_min=100, threshold_max=200):
        return cv2.Canny(image, threshold_min, threshold_max)

    @staticmethod
    def downscale(image, new_width):
        width_percent = (new_width / float(image.size[0]))
        height = int((float(image.size[1])*float(width_percent)))
        img = image.resize((new_width, height), Image.ANTIALIAS)
        return img

    @staticmethod
    def find_binary_contour(image):
        inner = Filter.convolution(image, Kernel.create_kernel("ones"), 1)
        contour = numpy.copy(image)
        contour[inner == 9] = 0

        return contour
