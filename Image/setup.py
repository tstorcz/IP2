import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="image_preprocessing",
    version="0.0.1",
    author="Tamas Storcz",
    author_email="storcz.tamas@mik.pte.hu",
    description="Classes to support image preprocessing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/tstorcz/IP2",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='=3.7',
)