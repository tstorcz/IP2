import numpy


class Stereogram(object):

    @staticmethod
    def make_pattern(shape=(16, 16), levels=64):
        # Creates a pattern from gray values.
        return numpy.random.randint(0, levels - 1, shape) / levels

    @staticmethod
    def __normalize(depthmap):
        # Normalizes values of depthmap to [0, 1] range.
        if depthmap.max() > depthmap.min():
            return (depthmap - depthmap.min()) / (depthmap.max() - depthmap.min())
        else:
            return depthmap

    @staticmethod
    def create_random_dot_stereogram(depthmap, pattern, shift_coeff=0.1, invert=False):
        # Creates an stereogram from depthmap and pattern.
        # Positive depthmap values indicate that object is closer to the use than background
        depthmap = Stereogram.__normalize(depthmap)
        if invert:
            depthmap = 1 - depthmap

        stereogram = numpy.zeros_like(depthmap, dtype=pattern.dtype)

        for r in numpy.arange(stereogram.shape[0]):
            for c in numpy.arange(stereogram.shape[1]):
                if c < pattern.shape[1]:
                    # First (leftmost) pattern, no change
                    stereogram[r, c] = pattern[r % pattern.shape[0], c]
                else:
                    # Shift related to first pattern, based on depthmap
                    shift = int(depthmap[r, c] * shift_coeff * pattern.shape[1])
                    stereogram[r, c] = stereogram[r, c - pattern.shape[1] + shift]
        return stereogram
