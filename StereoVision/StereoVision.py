import cv2
import os.path
import numpy

from stereovision.stereo_cameras import StereoPair, ChessboardFinder, CalibratedPair
from stereovision.calibration import StereoCalibrator, StereoCalibration
from stereovision.blockmatchers import StereoBM, StereoSGBM

from BMTuner import BMTuner
from Image.Filter import Filter
from Image.Kernel import Kernel
from Image.Segment import Segment
from PointCloud import PointCloud
from Math.Vector import Vector


class StereoVision(object):
    CAMERAS = [0, 1]

    CALIBRATION_IMAGE_COUNT = 30
    TUNE_IMAGE_COUNT = 5
    BOARD_ROWS = 7
    BOARD_COLS = 7
    BOARD_SQUARE_SIZE_CM = 2.2

    USE_STEREO_BM = True           # otherwise use semi-global block matcher
    USE_BM_TUNING = True
    USE_BM_SETTINGS = True

    FOLDER_PROJECT_ROOT = "D:\\ImageProcessing\\Stereo\\"
    FOLDER_IMAGES_CALIBRATION = FOLDER_PROJECT_ROOT + "01_Samples\\01_Calibration"
    FOLDER_IMAGES_TUNING = FOLDER_PROJECT_ROOT + "01_Samples\\02_Tuning"
    FOLDER_CALIBRATION_DATA = FOLDER_PROJECT_ROOT + "02_Calibration"
    FOLDER_BM_DATA = FOLDER_PROJECT_ROOT + "03_BM_Settings"
    FOLDER_POINT_CLOUDS = FOLDER_PROJECT_ROOT + "04_point_clouds"
    
    BM_SETTINGS_FILE = FOLDER_BM_DATA + "\\" + "tuned_bm_settings.dta"

    KEY_CODE_CALIBRATION = ord('c')
    KEY_CODE_TUNING = ord('t')
    KEY_CODE_QUIT = ord('q')

    def __init__(self, device_pair, image_width, image_height, capture_smoothing_cycles=1):
        self.device_pair = device_pair
        self.image_width = image_width
        self.image_height = image_height
        self.capture_smoothing_cycles = capture_smoothing_cycles

    def capture_generic_image_pair(self, exit_key_code):
        wait_key = 0
        frames = None
        with StereoPair(self.device_pair) as pair:
            while wait_key != exit_key_code:
                frames, wait_key = self.__capture_image_pair(pair)

        return frames

    def online_camera_calibration(self):
        print("Starting camera pair hardware calibration")
        print("Target a checkerboard image with both cameras")
        print("Setup camera pair (focus and orientation) to minimize single image calibration error")
        print("If done, press 'c' to calibrate")

        self.__capture_calibration_images(self.KEY_CODE_CALIBRATION)

        print("Starting calibration of stereo block matcher")
        print("{0} sample images will be captured, saved and used for calibration".format(self.CALIBRATION_IMAGE_COUNT))

        calibration_images, calibration_corners = \
            self.__capture_calibration_images(0, True)

        StereoVision.__save_image_pairs(calibration_images, self.FOLDER_IMAGES_CALIBRATION)

        calibration = self.__camera_calibration_by_corners(calibration_corners)

        if self.USE_BM_TUNING:
            print("Press 't' to capture an image for tuning the block matcher")

            tuning_image_pair = \
                self.capture_generic_image_pair(self.KEY_CODE_TUNING)

            StereoVision.__save_image_pairs([tuning_image_pair], self.FOLDER_IMAGES_TUNING)

            self.__tune_block_matcher(calibration, tuning_image_pair)

    def offline_camera_calibration(self):
        print("Reading input files...")
        calibration_images = []
        for i in range(self.CALIBRATION_IMAGE_COUNT):
            image_pair = []
            for side in ("left", "right"):
                number_string = str(i + 1).zfill(len(str(self.CALIBRATION_IMAGE_COUNT)))
                file_name = "{}_{}.jpg".format(side, number_string)
                file_name_with_path = os.path.join(self.FOLDER_IMAGES_CALIBRATION, file_name)
                image_pair.append(cv2.imread(file_name_with_path))
            calibration_images.append(image_pair)

        calibration = self.__camera_calibration_by_images(calibration_images)

        if self.USE_BM_TUNING:
            tuner_image_pair = []
            for side in ("left", "right"):
                file_name = "{}_1.jpg".format(side)
                file_name_with_path = os.path.join(self.FOLDER_IMAGES_TUNING, file_name)
                tuner_image_pair.append(cv2.imread(file_name_with_path))

            self.__tune_block_matcher(calibration, tuner_image_pair)

    def __capture_image_pair(self, stereo_pair, capture_edges=False, show_images=True):
        frames = stereo_pair.get_frames()
        wait_key = 0

        for i in range(0, len(frames)):
            frames[i] = cv2.cvtColor(frames[i], cv2.COLOR_BGR2GRAY)

            if self.capture_smoothing_cycles > 0:
                frames[i] = Filter.convolution(
                    frames[i],
                    Kernel.create_kernel('gaussian'),
                    self.capture_smoothing_cycles
                )

            frames[i] = Filter.normalize(frames[i], 255)

            if capture_edges:
                frames[i][cv2.Canny(frames[i], 60, 250) == 0] = 0

        if show_images:
            wait_key = stereo_pair.show_captured_frames(frames, 1)

        return frames, wait_key

    def __capture_calibration_images(self, exit_key_code, store_images=False):
        wait_key = 0
        calibration_images = []
        calibration_corners = []
        with ChessboardFinder(self.device_pair) as pair:
            while (exit_key_code != 0 and wait_key != exit_key_code) or \
                    (exit_key_code == 0 and len(calibration_images) < StereoVision.CALIBRATION_IMAGE_COUNT):

                calibrator = StereoCalibrator(self.BOARD_ROWS, self.BOARD_COLS, self.BOARD_SQUARE_SIZE_CM,
                                              (self.image_width, self.image_height))

                found_chessboard = [False, False]
                corners = [numpy.empty([1]), numpy.empty([1])]
                images = []
                while not all(found_chessboard) \
                        and ((corners[0] is None or len(corners[0]) != 49)
                             or (corners[1] is None or len(corners[1]) != 49)) \
                        and wait_key != StereoVision.KEY_CODE_CALIBRATION:
                    images, wait_key = self.__capture_image_pair(pair)

                    for i, frame in enumerate(images):
                        if len(images[i].shape) == 3:
                            images[i] = cv2.cvtColor(images[i], cv2.COLOR_BGR2GRAY)

                        (found_chessboard[i], corners[i]) = cv2.findChessboardCorners(
                            frame,
                            (self.BOARD_COLS, self.BOARD_ROWS),
                            cv2.CALIB_CB_ADAPTIVE_THRESH
                        )

                        found_chessboard[i] = found_chessboard[i] and corners[i].shape[0] != 49

                interest_points, angles = StereoVision.__get_checkerboard_angles(corners[0], images[0])
                corner_string = ""
                for i in range(0, len(interest_points)):
                    corner_string = corner_string + \
                                    "  X: {0:.0f}  Y: {1:.0f}  A: {2:.0f}".format(
                                        interest_points[i][0],
                                        interest_points[i][1],
                                        angles[i]
                                    )

                if wait_key != StereoVision.KEY_CODE_CALIBRATION:
                    for i in range(0, len(images)):
                        cv2.cornerSubPix(images[i], corners[i], (5, 5), (-1, -1),
                                         (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 30, 0.01))

                    calibrator.add_exact_corners(corners)

                if wait_key != StereoVision.KEY_CODE_CALIBRATION:
                    calibration_images.append(images)
                    calibration_corners.append(corners)
                    while (store_images and len(calibration_images) > StereoVision.CALIBRATION_IMAGE_COUNT) \
                            or (not store_images and len(calibration_images) > 1):
                        calibration_images.pop(0)
                        calibration_corners.pop(0)

                    calibration = calibrator.calibrate_cameras()
                    avg_error = calibrator.check_calibration(calibration)
                    print(
                        corner_string,
                        "Average error: {0:.2f} pixels. Image count: {1}".format(avg_error, len(calibration_images))
                    )

        return calibration_images, calibration_corners

    def __camera_calibration_by_corners(self, corner_pair_array):
        print("Starting calibration")

        calibrator = StereoCalibrator(self.BOARD_ROWS, self.BOARD_COLS, self.BOARD_SQUARE_SIZE_CM,
                                      (self.image_width, self.image_height))

        for corner_pair in corner_pair_array:
            calibrator.add_exact_corners(corner_pair)

        calibration = calibrator.calibrate_cameras()
        avg_error = calibrator.check_calibration(calibration)
        print("The average error between chessboard points and their epipolar "
              "lines is {0:.2f} pixels.".format(avg_error))
        calibration.export(self.FOLDER_CALIBRATION_DATA)

        print("Calibration finished")

        return calibration

    def __camera_calibration_by_images(self, image_pairs):
        height, width = image_pairs[0][0].shape[:2]
        calibrator = StereoCalibrator(self.BOARD_ROWS, self.BOARD_COLS, self.BOARD_SQUARE_SIZE_CM, (width, height))

        print("Calibrating cameras. This can take a while.")

        while image_pairs:
            img_left, im_right = image_pairs[0]
            calibrator.add_corners((img_left, im_right))
            image_pairs = image_pairs[1:]

        calibration = calibrator.calibrate_cameras()
        avg_error = calibrator.check_calibration(calibration)
        print("The average error between chessboard points and their epipolar "
              "lines is {0:.2f} pixels.".format(avg_error))
        calibration.export(self.FOLDER_CALIBRATION_DATA)

        return calibration

    def __tune_block_matcher(self, calibration, tuner_image_pair):
        print("Starting Block Matcher tuning")

        if self.USE_STEREO_BM:
            block_matcher = StereoBM()
        else:
            block_matcher = StereoSGBM()

        # rectified_pair = calibration.rectify(tuner_image_pair)
        BMTuner(block_matcher, calibration, tuner_image_pair)

        block_matcher.save_settings(StereoVision.BM_SETTINGS_FILE)
        print("Block Matcher tuning finished")

        return block_matcher

    def scan_point_cloud(self):
        print("Point cloud scanning started")
        print("Press 'q' to quit")

        if self.USE_STEREO_BM:
            block_matcher = StereoBM()
        else:
            block_matcher = StereoSGBM()

        if self.USE_BM_SETTINGS and os.path.exists(self.BM_SETTINGS_FILE):
            block_matcher.load_settings(self.BM_SETTINGS_FILE)

        calibration = StereoCalibration(input_folder=self.FOLDER_CALIBRATION_DATA)

        wait_key = 0
        with StereoPair(self.device_pair) as pair:
            with CalibratedPair(self.device_pair, calibration, block_matcher) as calibratedPair:
                while wait_key != self.KEY_CODE_QUIT:
                    frames, wait_key = self.__capture_image_pair(pair, True)

                    disparity = calibratedPair.block_matcher.get_disparity(frames).astype(numpy.float32)
                    norm_coeff = 1.0 / disparity.max()
                    cv2.imshow("disparity", disparity * norm_coeff)

                    # rectified_pair = calibratedPair.calibration.rectify(frames)
                    points = calibratedPair.get_point_cloud(frames)
                    # point_cloud = points.filter_infinity()

                    coords = points.coordinates[:, 2].reshape_image_batch_tensor((480, 640))
                    cv2.imshow("depth", Filter.normalize(coords, 255.0))

    @staticmethod
    def __save_image_pairs(image_pair_array, folder):
        print("Saving calibration images")

        if not os.path.exists(folder):
            os.makedirs(folder)

        for i in range(len(image_pair_array)):
            for side, frame in zip(("left", "right"), image_pair_array[i]):
                number_string = str(i + 1).zfill(len(str(len(image_pair_array))))
                filename = "{}_{}.jpg".format(side, number_string)
                output_path = os.path.join(folder, filename)
                cv2.imwrite(output_path, frame)

        print("Calibration images saved")

    @staticmethod
    def __get_checkerboard_angles(corners_array, image):
        hull = numpy.array(PointCloud.convex_hull(numpy.squeeze(corners_array)))

        hull = numpy.delete(hull, 0, 0)

        corners, _ = numpy.array(StereoVision.__get_corners(hull))
        corners, angles = StereoVision.__get_corners(corners)

        c = 255
        d = 200 / len(corners)
        length = len(corners)

        for i in range(0, length):
            cv2.circle(image, (int(corners[i][0]), int(corners[i][1])), 5, (c, c, c))
            c = c - d
        cv2.imshow("pois", image)

        return corners, angles

    @staticmethod
    def __get_corners(connected):
        length = connected.shape[0]
        corners = []
        for i in range(0, length):
            angle = Vector.turn_of_vectors(
                connected[Segment.get_valid_index(length, i - 1)],
                connected[i],
                connected[Segment.get_valid_index(length, i + 1)]
            )
            if 45 < angle:
                corners.append(connected[i])

        angles = []
        length = len(corners)

        for i in range(0, length):
            angles.append(
                Vector.turn_of_vectors(
                    corners[Segment.get_valid_index(length, i - 1)],
                    corners[i],
                    corners[Segment.get_valid_index(length, i + 1)]
                )
            )

        return corners, angles
