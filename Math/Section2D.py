from __future__ import annotations

import math
import numpy


class Section2D(object):

    # points are list of coordinates (y, x) = (row, col)
    def __init__(self, point1, point2, sort=True):
        self.p1 = []
        self.p2 = []
        self.center = []
        self.isSorted = sort
        self.__set_points(point1, point2, sort)
        self.__calc_by_points()

    def __set_points(self, point1, point2, sort=True):

        if sort and (point1[0] * point1[0]) + (point1[1] * point1[1]) \
                > (point2[0] * point2[0]) + (point2[1] * point2[1]):
            point1, point2 = point2, point1

        self.p1 = point1
        self.p2 = point2
        self.v = [a_i - b_i for a_i, b_i in zip(self.p2, self.p1)]

    def __calc_by_points(self):
        self.length = Section2D.distance_of_points(self.p1, self.p2)
        self.center = numpy.add(self.p1, self.p2) / 2
        self.angle = math.degrees(math.atan2(self.p2[0] - self.p1[0], self.p2[1] - self.p1[1]))
        if self.angle < 0:
            self.angle = 360 + self.angle
        if self.angle > 180:
            self.angle = self.angle - 180

    def coordinates(self, dimension):
        return [self.p1[dimension], self.p2[dimension]]

    def distance(self, other_section):
        return min(self.distance_of_point(other_section.p1), self.distance_of_point(other_section.p2))

    def distance_of_point(self, point):
        numerator = (self.p2[1]-self.p1[1])*(self.p1[0]-point[0]) - (self.p1[1]-point[1])*(self.p2[0]-self.p1[0])
        denominator = math.pow(self.p2[1]-self.p1[1], 2) + math.pow(self.p2[0]-self.p1[0], 2)
        return math.fabs(numerator) / math.sqrt(denominator)

    def distance_to_endpoints(self, point):
        return min(
            Section2D.distance_of_points(self.p1, point),
            Section2D.distance_of_points(self.p2, point)
        )

    def distance_of_endpoints(self, other_section: Section2D):
        return min(
            self.distance_to_endpoints(other_section.p1),
            self.distance_to_endpoints(other_section.p2)
        )

    def orthogonal_distance(self, other_section: Section2D):
        if abs(self.angle) < 45 or (135 < abs(self.angle) < 225) or abs(self.angle) > 315:
            return (
                Section2D.dimension_distance(self, other_section, 1),
                Section2D.dimension_distance(self, other_section, 0)
            )

        return (
                Section2D.dimension_distance(self, other_section, 0),
                Section2D.dimension_distance(self, other_section, 1)
            )

    def projection_of_point(self, point):
        perpendicular = Section2D(point, [point[0] + self.v[1], point[1] - self.v[0]])
        return Section2D.intersection_point_of_lines(self, perpendicular)

    # using direction of surface (defined by 3 points in a specified walking around) normal
    def is_point_on_right(self, point) -> bool:
        v1 = [b - a for a, b in zip(self.p1 + [0], self.p2 + [0])]
        v2 = [b - a for a, b in zip(self.p1 + [0], list(point) + [0])]

        return (v1[0] * v2[1] - v1[1] * v2[0]) < 0

    def append(self, other_section: Section2D):
        distances = [
            Section2D.distance_of_points(self.p1, other_section.p1),
            Section2D.distance_of_points(self.p1, other_section.p2),
            Section2D.distance_of_points(self.p2, other_section.p1),
            Section2D.distance_of_points(self.p2, other_section.p2)
        ]

        new_endpoints = [
            (self.p1, other_section.p1),
            (self.p1, other_section.p2),
            (self.p2, other_section.p1),
            (self.p2, other_section.p2)
        ]

        max_index = distances.index(max(distances))

        self.__set_points(new_endpoints[max_index][0], new_endpoints[max_index][1])

        self.__calc_by_points()

    def is_interior_point(self, point, distance_threshold):
        projection = self.projection_of_point(point)
        projection_length = Section2D.distance_of_points(point, projection)
        projection_distance_1 = Section2D.distance_of_points(self.p1, projection)
        projection_distance_2 = Section2D.distance_of_points(self.p2, projection)

        return \
            projection_length <= distance_threshold \
            and projection_distance_1 <= self.length \
            and projection_distance_2 <= self.length

    def angle_difference(self, other_section: Section2D):
        if self.length == 0 or other_section.length == 0:
            return 0

        scalar_prod = sum(i[0] * i[1] for i in zip(self.v, other_section.v)) / (self.length * other_section.length)
        while scalar_prod > 1:
            scalar_prod = scalar_prod - 1
        while scalar_prod < -1:
            scalar_prod = scalar_prod + 1
        return math.degrees(math.acos(scalar_prod))

    def shift(self, shift_point):
        self.p1 = [x + y for x, y in zip(self.p1, shift_point)]
        self.p2 = [x + y for x, y in zip(self.p2, shift_point)]
        self.__calc_by_points()

    def set_length(self, new_length):
        self.__set_points(
            self.p1,
            self.p1 + numpy.multiply(self.v, (new_length / self.length)),
            sort=self.isSorted
        )
        self.__calc_by_points()

    def rotate(self, alpha, new_point):
        alpha_rad = math.radians(alpha)
        rotation = numpy.array([
            [math.cos(alpha_rad), -math.sin(alpha_rad)],
            [math.sin(alpha_rad), math.cos(alpha_rad)]
        ])

        return Section2D(
            new_point, new_point + numpy.transpose(numpy.matmul(rotation, numpy.transpose(self.v))),
            sort=self.isSorted
        )

    def intersection(self, other: Section2D):
        return Section2D.intersection_point_of_lines(self, other)

    def has_intersection(self, other: Section2D, distance_threshold) -> bool:
        return self.is_interior_point(Section2D.intersection_point_of_lines(self, other), distance_threshold)

    def contains(self, other_section: Section2D, distance_threshold):
        return self.is_interior_point(other_section.p1, distance_threshold) \
               and self.is_interior_point(other_section.p2, distance_threshold)

    @staticmethod
    def distance_of_sections(section1: Section2D, section2: Section2D):
        dist_a = section1.distance(section2)
        dist_b = section2.distance(section1)
        return min(dist_a, dist_b)

    @staticmethod
    def distance_of_points(point1, point2):
        return math.sqrt(math.pow(point1[0]-point2[0], 2) + math.pow(point1[1]-point2[1], 2))

    @staticmethod
    def intersection_point_of_lines(l1: Section2D, l2: Section2D):

        d = (l1.p1[1] - l1.p2[1]) * (l2.p1[0] - l2.p2[0]) - (l1.p1[0] - l1.p2[0]) * (l2.p1[1] - l2.p2[1])
        if d == 0:
            return None

        na = (l1.p1[1] * l1.p2[0] - l1.p1[0] * l1.p2[1]) / d
        nb = (l2.p1[1] * l2.p2[0] - l2.p1[0] * l2.p2[1]) / d

        xn = na * (l2.p1[1] - l2.p2[1]) - nb * (l1.p1[1] - l1.p2[1])
        yn = na * (l2.p1[0] - l2.p2[0]) - nb * (l1.p1[0] - l1.p2[0])
        return [yn, xn]

    @staticmethod
    def dimension_distance(section_1: Section2D, section_2: Section2D, dimension):
        distances = [
            abs(section_1.p1[dimension] - section_2.p1[dimension]),
            abs(section_1.p1[dimension] - section_2.p2[dimension]),
            abs(section_1.p2[dimension] - section_2.p1[dimension]),
            abs(section_1.p2[dimension] - section_2.p2[dimension])
        ]
        return min(distances)

    @staticmethod
    def connect_sections(section_list: [Section2D], compare_dimension,
                         max_section_distance,
                         max_parallel_endpoint_distance, max_perpendicular_endpoint_distance,
                         max_angle_diff=5):
        section_list.sort(key=lambda x: x.p1[compare_dimension])
        has_change = True
        while has_change:
            has_change = False
            selected_a = -1
            selected_b = -1

            for i in range(0, len(section_list) - 1):
                for j in range(i + 1, len(section_list)):
                    if (
                            (
                                    Section2D.distance_of_sections(section_list[i], section_list[j]) <= max_section_distance
                                    and section_list[i].orthogonal_distance(section_list[j])[0] <= max_parallel_endpoint_distance
                                    and section_list[i].orthogonal_distance(section_list[j])[1] <= max_perpendicular_endpoint_distance
                            )
                            or section_list[i].contains(section_list[j], max_perpendicular_endpoint_distance)
                            or section_list[j].contains(section_list[i], max_perpendicular_endpoint_distance)
                    ) \
                            and section_list[i].angle_difference(section_list[j]) < max_angle_diff \
                            and (
                                selected_a < 0
                                or section_list[i].angle_difference(section_list[j])
                                < section_list[selected_a].angle_difference(section_list[selected_b])
                            ):
                        selected_a = i
                        selected_b = j

            if selected_a >= 0:
                section_list[selected_a].append(section_list[selected_b])
                section_list.pop(selected_b)
                has_change = True

    @staticmethod
    def get_sections(hough_lines):
        sections = []

        if hough_lines is not None:
            for line in hough_lines:
                for x1, y1, x2, y2 in line:
                    section = Section2D([y1, x1], [y2, x2])
                    sections.append(section)

        return sections

    @staticmethod
    def get_horizontal_sections(hough_lines, angle_threshold):
        horizontal_sections = []

        if hough_lines is not None:
            for line in hough_lines:
                for x1, y1, x2, y2 in line:
                    section = Section2D([y1, x1], [y2, x2])

                    if section.angle < angle_threshold or section.angle > 180 - angle_threshold:
                        horizontal_sections.append(section)

        return horizontal_sections

    @staticmethod
    def get_vertical_sections(hough_lines, angle_threshold):
        vertical_sections = []

        if hough_lines is not None:
            for line in hough_lines:
                for x1, y1, x2, y2 in line:
                    section = Section2D([y1, x1], [y2, x2])

                    if 90 - angle_threshold < section.angle < 90 + angle_threshold:
                        vertical_sections.append(section)

        return vertical_sections
