import numpy as np

from sklearn.metrics import r2_score

from Math.Distributions import Distributions


class Evaluator(object):
    @staticmethod
    def evaluate_data_array(data_array):
        sorted_data = np.sort(data_array.copy())

        normality = Distributions.is_normal(data_array)

        return \
            sum(normality) / len(normality), \
            np.mean(data_array), np.std(data_array), \
            Evaluator.median(sorted_data[:sorted_data.shape[0] // 2]), \
            Evaluator.median(sorted_data), \
            Evaluator.median(sorted_data[-(sorted_data.shape[0] // 2):])

    @staticmethod
    def evaluate_differences(expected, predicted,
                             cost_function=None,
                             return_data=False,
                             parameters: dict = None
                             ):
        if parameters is None:
            parameters = {}

        if "cost_function" in parameters.keys():
            cost_function = parameters["cost_function"]

        if "return_data" in parameters.keys():
            return_data = parameters["return_data"]

        diffs = np.subtract(expected, predicted)

        if cost_function is None:
            cost_of_diffs = diffs
        else:
            cost_of_diffs = np.vectorize(cost_function(diffs))

        cost_eval = Evaluator.evaluate_data_array(cost_of_diffs)

        abs_diff = np.abs(cost_of_diffs)
        abs_eval = Evaluator.evaluate_data_array(abs_diff)

        relative_diffs = np.divide(diffs, expected)
        relative_eval = Evaluator.evaluate_data_array(relative_diffs)

        abs_relative_diffs = np.abs(relative_diffs)
        abs_relative_eval = Evaluator.evaluate_data_array(abs_relative_diffs)

        r2 = r2_score(expected, predicted)

        if return_data:
            return cost_eval, abs_eval, relative_eval, abs_relative_eval, r2, cost_of_diffs, relative_diffs
        else:
            return cost_eval, abs_eval, relative_eval, abs_relative_eval, r2

    @staticmethod
    def evaluate_ordinal_onehot_differences(expected_classification,
                                            classification_predicted,
                                            cost_function=None):

        expected = np.argmax(expected_classification, axis=1)
        predicted = np.argmax(classification_predicted, axis=1)
        diffs = np.subtract(expected, predicted)

        if cost_function is None:
            cost_of_diffs = diffs
        else:
            cost_of_diffs = np.vectorize(cost_function)(diffs)

        abs_eval = Evaluator.evaluate_data_array(cost_of_diffs)

        abs_diff = np.abs(cost_of_diffs)
        cost_eval = Evaluator.evaluate_data_array(abs_diff)

        return abs_eval, cost_eval

    @staticmethod
    def get_groups_by_sigma(data):
        avg = np.average(data)
        dev = np.std(data)

        item_count = [[], [], [], []]

        for i in range(0, len(data)):
            category = int(abs(avg - data[i]) // dev)
            item_count[category if category <= 3 else 3].append(i)

        return item_count

    @staticmethod
    def median(data_array):
        if data_array.shape[0] % 2 == 1:
            return data_array[(data_array.shape[0] // 2) + 1]
        else:
            return (data_array[data_array.shape[0] // 2] + data_array[(data_array.shape[0] // 2) + 1]) / 2.0
