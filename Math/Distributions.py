import numpy as np

from scipy import stats


class Distributions(object):
    # https://www.statisticshowto.com/probability-distribution/
    # https://machinelearningmastery.com/a-gentle-introduction-to-normality-tests-in-python/

    @staticmethod
    def is_normal(values, alpha=0.05):
        shapiro_s, shapiro_p = stats.shapiro(values)    # Shapiro-Wilk
        k2_s, k2_p = stats.normaltest(values)           # D'Agostino's K^2
        anderson_results = stats.anderson(values)       # Anderson - Darling (Kolmogorov - Smirnov)

        test_results = [
            1 if shapiro_p > alpha else 0,
            1 if k2_p >= alpha else 0
        ]

        no_good_found = True
        for i in range(len(anderson_results.critical_values)):
            if no_good_found and anderson_results.significance_level[i] <= alpha * 100 and \
                    anderson_results.statistic < anderson_results.critical_values[i]:
                test_results.append(1)
                no_good_found = False

        if no_good_found:
            test_results.append(0)

        return test_results
