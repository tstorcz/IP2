from scipy.stats import kstest, normaltest


class NormalDistribution(object):
    @staticmethod
    def test(data, test_type="ks"):
        is_normal = False
        test_type = test_type.lower()

        if test_type == "ks":
            ks_statistic, p_value = kstest(data, 'norm')
            is_normal = p_value > 0.05

        if test_type == "k^2":
            stat, p_value = normaltest(data)
            is_normal = p_value > 0.05

        return is_normal
