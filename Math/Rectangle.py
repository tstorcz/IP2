from __future__ import annotations

from Math.Section2D import Section2D


class Rectangle(object):

    def __init__(self, corners, magnification=1.0):
        corners = [[x * magnification for x in c] for c in corners]
        [top_left, top_right, bottom_right, bottom_left] = corners
        self.top_left = top_left
        self.top_right = top_right
        self.bottom_right = bottom_right
        self.bottom_left = bottom_left
        self.frame = corners

        self.top = Section2D(top_left, top_right, sort=False)
        self.right = Section2D(top_right, bottom_right, sort=False)
        self.bottom = Section2D(bottom_right, bottom_left, sort=False)
        self.left = Section2D(bottom_left, top_left, sort=False)

        self.area = self.top.length * self.right.length

    def aspect_ratio(self):
        a = (self.top.length + self.bottom.length) / 2
        b = (self.right.length + self.left.length) / 2
        return min(a, b) / max(a, b)

    def includes_point(self, point):
        return self.top.is_point_on_right(point) \
               and self.right.is_point_on_right(point) \
               and self.bottom.is_point_on_right(point) \
               and self.left.is_point_on_right(point)

    def intersection_polygon(self, frame: Rectangle) -> []:
        # rectangle can be out on top and bottom of frame
        polygon = []

        # top
        if frame.includes_point(self.top_left):
            polygon.append(self.top_left)
        else:
            polygon.append(self.left.intersection(frame.top))

        if frame.includes_point(self.top_left) != frame.includes_point(self.top_right):
            polygon.append(self.top.intersection(frame.top))

        if frame.includes_point(self.top_right):
            polygon.append(self.top_right)
        else:
            polygon.append(self.right.intersection(frame.top))

        # bottom
        if frame.includes_point(self.bottom_right):
            polygon.append(self.bottom_right)
        else:
            polygon.append(self.right.intersection(frame.top))

        if frame.includes_point(self.bottom_right) != frame.includes_point(self.bottom_left):
            polygon.append(self.bottom.intersection(frame.bottom))

        if frame.includes_point(self.bottom_left):
            polygon.append(self.bottom_left)
        else:
            polygon.append(self.left.intersection(frame.bottom))

        return polygon

    def bounding_box(self, other_rectangle: Rectangle):
        return Rectangle(
            [
                [min(self.top_left[0], self.top_right[0], other_rectangle.top_left[0], other_rectangle.top_right[0]),
                 min(self.top_left[1], self.top_right[1], other_rectangle.top_left[1], other_rectangle.top_right[1])],
                [max(self.top_left[0], self.top_right[0], other_rectangle.top_left[0], other_rectangle.top_right[0]),
                 min(self.top_left[1], self.top_right[1], other_rectangle.top_left[1], other_rectangle.top_right[1])],
                [max(self.bottom_left[0], self.bottom_right[0], other_rectangle.bottom_left[0], other_rectangle.bottom_right[0]),
                 max(self.bottom_left[1], self.bottom_right[1], other_rectangle.bottom_left[1], other_rectangle.bottom_right[1])],
                [min(self.bottom_left[0], self.bottom_right[0], other_rectangle.bottom_left[0], other_rectangle.bottom_right[0]),
                 max(self.bottom_left[1], self.bottom_right[1], other_rectangle.bottom_left[1], other_rectangle.bottom_right[1])],
            ]
        )

    def create_extend(self, new_size):
        diff = [(new_size[0] - self.top.length) / 2, (new_size[1] - self.left.length) / 2]
        return Rectangle(
            [
                [max(self.top_left[0] - diff[0], 0), max(self.top_left[1] - diff[1], 0)],
                [max(self.top_right[0] + diff[0], 0), max(self.top_right[1] - diff[1], 0)],
                [max(self.bottom_right[0] + diff[0], 0), max(self.bottom_right[1] + diff[1], 0)],
                [max(self.bottom_left[0] - diff[0], 0), max(self.bottom_left[1] + diff[1], 0)],
            ]
        )

    def create_shifted(self, shift_x=0, shift_y=0):
        return Rectangle([[p[0] + shift_x, p[1] + shift_y] for p in self.frame])

    @staticmethod
    def create_by_size(top_left, size):
        return Rectangle(
            [
                [max(top_left[0], 0), max(top_left[1], 0)],
                [max(top_left[0]+size[0], 0), max(top_left[1], 0)],
                [max(top_left[0]+size[0], 0), max(top_left[1] + size[1], 0)],
                [max(top_left[0], 0), max(top_left[1] + size[1], 0)]
            ]
        )
