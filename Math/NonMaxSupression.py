import numpy as np


class NonMaxSuppression(object):

    @staticmethod
    def non_max_suppression_fast(boxes, overlap_threshold=0.5):

        if len(boxes) == 0:
            return []

        if boxes.dtype.kind == "i":
            boxes = boxes.astype("float")

        pick = []

        x1 = boxes[:, 0]
        y1 = boxes[:, 1]
        x2 = boxes[:, 2]
        y2 = boxes[:, 3]
        # compute the area of the bounding boxes and sort the bounding
        # boxes by the bottom-right y-coordinate of the bounding box
        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        indices = np.argsort(y2)
        # keep looping while some indexes still remain in the indexes
        # list
        while len(indices) > 0:
            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(indices) - 1
            i = indices[last]
            pick.append(i)
            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            xx1 = np.maximum(x1[i], x1[indices[:last]])
            yy1 = np.maximum(y1[i], y1[indices[:last]])
            xx2 = np.minimum(x2[i], x2[indices[:last]])
            yy2 = np.minimum(y2[i], y2[indices[:last]])
            # compute the width and height of the bounding box
            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)
            # compute the ratio of overlap
            overlap = (w * h) / area[indices[:last]]
            # delete all indexes from the index list that have
            indices = np.delete(indices, np.concatenate(([last], np.where(overlap > overlap_threshold)[0])))
        # return only the bounding boxes that were picked using the
        # integer data type
        return boxes[pick].astype("int")

    @staticmethod
    def non_max_suppression(images, rect_range, combined=False):
        # returned 3-tuple: index, [coordinates], value

        max_items = []
        for i in range(0, len(images)):
            image = images[i]
            max_items.append([])
            while np.max(image) > 0:
                max_index = np.unravel_index(np.argmax(image, axis=None), image.shape)
                max_value = image[max_index]
                max_items[-1].append([i, max_index, max_value])

                image[
                    max(0, max_index[0] - rect_range[0]):min(image.shape[0], max_index[0] + rect_range[0]),
                    max(0, max_index[1] - rect_range[1]):min(image.shape[1], max_index[1] + rect_range[1])
                ] = 0

        if combined:
            pois = []
            all_max_items = []
            for mi in max_items:
                all_max_items.extend(mi)

            # take the one with max correlation
            all_max_items.sort(key=lambda tup: tup[2], reverse=True)
            while len(all_max_items) > 0:
                pois.append(all_max_items[0])
                max_item = all_max_items.pop(0)

                to_suppress = [
                    t for t in all_max_items
                    if max_item[1][0] - rect_range[0] <= t[1][0] <= max_item[1][0] + rect_range[0]
                    and max_item[1][1] - rect_range[1] <= t[1][1] <= max_item[1][1] + rect_range[1]
                ]

                for x in to_suppress:
                    all_max_items.remove(x)

            return pois

        return max_items
