import math


class Vector(object):
    @staticmethod
    def __dot_product(v1, v2):
        return sum((a * b) for a, b in zip(v1, v2))

    @staticmethod
    def determinant(v, w):
        return v[0] * w[1] - v[1] * w[0]

    @staticmethod
    def length(v):
        return math.sqrt(Vector.__dot_product(v, v))

    @staticmethod
    def turn_of_vectors(p1, p2, p3):
        v1 = p2 - p1
        v2 = p3 - p2
        lv1 = Vector.length(v1)
        lv2 = Vector.length(v2)
        if lv1 == 0 or lv2 == 0:
            return 0

        dp = round(Vector.__dot_product(v1, v2), 4)
        le = round((lv1 * lv2), 4)
        if abs(dp) > abs(le):
            return 90

        return math.degrees(math.acos(dp / le))
