import math
import numpy as np

from MachineLearning.DataSources.BatchLabeledDataSource import BatchLabeledDataSource
from MachineLearning.DataSources.IndexManagers.BatchIndexManager import BatchIndexManager
from MachineLearning.DataSources.IndexGenerators.BreadthFirstRandomBatchIndexGenerator import BreadthFirstBatchIndexGenerator


# A total batch size has to be specified for the DataSource (and can be updated later)
# Stores data-label pairs with minimum rate of items in bach
# !!! batch_count is automatically specified by stored data sets (largest of individual batch counts)
class StratifiedDataSource(object):

    def __init__(self, batch_size, identifier=None):
        self.__identifier = identifier
        self.__batch_size = batch_size
        self.data_sources = []
        self.batch_count = 0
        self.__total_length = 0
        self.__shuffle_indices = None

    def get_id(self):
        return self.__identifier

    def add_data(self, new_data, new_labels, item_rate_in_batch):
        self.data_sources.append(
            [
                max(item_rate_in_batch, 0.001),
                BatchLabeledDataSource(
                    BatchIndexManager(
                        extend_incomplete=True,
                        batch_index_generator=BreadthFirstBatchIndexGenerator()
                    )
                )
            ]
        )

        self.__total_length += new_data.shape[0]

        self.data_sources[-1][1].add_data(new_data, new_labels)

        self.set_batch_size(self.__batch_size)

    def shuffle_indices(self):
        data_count = 0
        for i in range(0, len(self.data_sources)):
            data_count += self.data_sources[i][1].get_data().shape[0]

        self.__shuffle_indices = np.arange(0, data_count, dtype=int)
        np.random.shuffle(self.__shuffle_indices)

    def get_data(self):
        all_data = self.data_sources[0][1].get_data().copy()
        for i in range(1, len(self.data_sources)):
            all_data = np.append(all_data, self.data_sources[i][1].get_data(), 0)

        return all_data if self.__shuffle_indices is None else all_data[self.__shuffle_indices]

    def get_labels(self):
        all_labels = self.data_sources[0][1].get_labels().copy()
        for i in range(1, len(self.data_sources)):
            all_labels = np.append(all_labels, self.data_sources[i][1].get_labels(), 0)

        return all_labels if self.__shuffle_indices is None else all_labels[self.__shuffle_indices]

    def __get_batch_count(self):
        self.batch_count = 0

        for data in self.data_sources:
            if self.__batch_size * data[0] == 0:
                auto_batch_count = data[1].index_manager.get_length()
            else:
                auto_batch_count = int(
                    data[1].index_manager.get_length() / (self.__batch_size * data[0])
                )

            if self.batch_count < auto_batch_count:
                self.batch_count = auto_batch_count

    def get_total_length(self):
        return self.__total_length  # self.batch_count * self.__batch_size

    def get_length(self, data_index):
        return self.data_sources[data_index][1].get_length()

    def set_batch_size(self, new_batch_size):
        self.__batch_size = new_batch_size
        self.__get_batch_count()

        for data in self.data_sources:
            data[1].set_batch_parameters(
                self.batch_count,
                math.floor(max(self.__batch_size * data[0], 1))
            )

    def get_current_batch(self):
        data = None
        label = None
        for ds in self.data_sources:
            data_part, label_part = ds[1].get_current_batch()
            if data is None:
                data = data_part
            else:
                data = np.append(data, data_part, axis=0)

            if label is None:
                label = label_part
            else:
                label = np.append(label, label_part, axis=0)

        return data, label

    def reset_batch_index(self):
        for data in self.data_sources:
            data[1].index_manager.reset_batch_index()

    def next_batch(self, reset=False):
        ret_val = True
        for data in self.data_sources:
            ret_val = ret_val and data[1].next_batch(reset)

        return ret_val

    def get_next_batch(self):
        self.next_batch()
        return self.get_current_batch()
