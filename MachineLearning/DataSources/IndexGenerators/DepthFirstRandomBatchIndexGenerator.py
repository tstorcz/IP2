import random
from MachineLearning.DataSources.IndexGenerators.DepthFirstBatchIndexGenerator import DepthFirstBatchIndexGenerator


class DepthFirstRandomBatchIndexGenerator(DepthFirstBatchIndexGenerator):

    def __init__(self):
        super().__init__()

    def _next_random(self):
        return random.randrange(len(self._remaining_indices))
