from MachineLearning.DataSources.IndexGenerators.DepthFirstBatchIndexGenerator import DepthFirstBatchIndexGenerator


class BreadthFirstBatchIndexGenerator(DepthFirstBatchIndexGenerator):
    def __init__(self):
        super().__init__()

    def create_batches(self):
        batch_indices = [[] for _ in range(self._batch_count)]
        first_shorter_batch_index = -1

        if self._list_size > 0:
            self._remaining_indices = list(range(0, self._list_size))
            while len(batch_indices[0]) < self._batch_size and len(self._remaining_indices) > 0:
                first_shorter_batch_index = self._next_cycle(batch_indices)

            if len(self._remaining_indices) > 0:
                batch_indices.append(self._remaining_indices)

        return batch_indices, first_shorter_batch_index

    def _next_cycle(self, batch_indices):
        first_shorter_batch_index = -1
        for i in range(len(batch_indices)):
            if len(self._remaining_indices) > 0:
                batch_indices[i].append(
                    self._remaining_indices.pop(0)
                )
                first_shorter_batch_index = i

        return first_shorter_batch_index + 1
