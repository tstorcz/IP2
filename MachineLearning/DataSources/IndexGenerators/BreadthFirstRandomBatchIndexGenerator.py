import random
from MachineLearning.DataSources.IndexGenerators.BreadthFirstBatchIndexGenerator import BreadthFirstBatchIndexGenerator


class BreadthFirstRandomBatchIndexGenerator(BreadthFirstBatchIndexGenerator):
    def __init__():
        super().__init__()

    def _next_cycle(self, batch_indices):
        first_shorter_batch_index = -1
        for i in range(len(batch_indices)):
            if len(self._remaining_indices) > 0:
                batch_indices[i].append(
                    self._remaining_indices.pop(random.randrange(len(self._remaining_indices)))
                )
                first_shorter_batch_index = i

        return first_shorter_batch_index + 1
