import numpy as np


class TimeSeries1DDataSource(object):

    def __init__(self, x, y_true):

        self.max_interval_length = y_true.shape[0]
        self.y_true = y_true
        self.x_data = x

    def add_new_data(self, new_x, new_y):
        self.x_data = np.append(self.x_data, new_x, 1)
        self.y_true = np.append(self.y_true, new_y, 1)

    def get_interval(self, x_start, time_steps):
        if x_start + time_steps > self.max_interval_length:
            raise IndexError()

        indices = x_start + np.arange(0.0, time_steps)
        batch_x = np.take(self.x_data, indices)
        batch_y = np.take(self.y_true, indices)

        return TimeSeries1DDataSource.__get_batch_tuple(indices, batch_x, batch_y)

    def get_random_intervals(self, batch_count, time_steps):
        start_indices = np.random.rand(batch_count, 1) * (self.max_interval_length - time_steps)
        indices = (start_indices + np.arange(0.0, time_steps)).astype(int)
        batch_x = np.take(self.x_data, indices)
        batch_y = np.take(self.y_true, indices)

        return TimeSeries1DDataSource.__get_batch_tuple(indices, batch_x, batch_y, time_steps)

    @staticmethod
    # returned arrays
    #   indices
    #   data
    #   labels
    def __get_batch_tuple(indices, batch_x, batch_y, batch_size):
        return indices, batch_x, batch_y
