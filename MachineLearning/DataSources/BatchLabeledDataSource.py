from MachineLearning.DataSources.IndexManagers.BatchIndexManager import BatchIndexManager
from MachineLearning.DataSources.BatchDataSet import BatchDataSet


class BatchLabeledDataSource:

    def __init__(self, index_manager=None):
        if index_manager is None:
            index_manager = BatchIndexManager()

        self.index_manager = index_manager

        self.data = BatchDataSet(index_manager)
        self.label = BatchDataSet(index_manager)

    def get_length(self):
        return self.index_manager.get_length()

    def add_data(self, data, label):
        if data.shape[0] != label.shape[0]:
            raise ValueError()

        self.data.add_data(data)
        self.label.add_data(label)

    def set_batch_count(self, new_batch_count):
        self.data.set_batch_count(new_batch_count)
        self.label.set_batch_count(new_batch_count)

    def set_batch_size(self, new_batch_size):
        self.data.set_batch_size(new_batch_size)
        self.label.set_batch_size(new_batch_size)

    def set_batch_parameters(self, new_batch_count, new_batch_size):
        self.data.set_batch_parameters(new_batch_count, new_batch_size)
        self.label.set_batch_parameters(new_batch_count, new_batch_size)

    def get_data(self):
        return self.data.get_data()

    def get_labels(self):
        return self.label.get_data()

    def get_current_batch(self):
        return (
            self.data.get_current_batch(),
            self.label.get_current_batch()
        )

    def reset_batch(self):
        self.index_manager.reset_batch_index()

    def next_batch(self, reset_batch_index=False):
        return self.index_manager.set_next_batch_index(reset_batch_index)
