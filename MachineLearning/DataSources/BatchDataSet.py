import numpy as np

from MachineLearning.DataSources.IndexManagers.BatchIndexManager import BatchIndexManager


class BatchDataSet:

    def __init__(self, index_manager=None):
        super().__init__()

        if index_manager is None:
            self.__batch_index_manager = BatchIndexManager()
        else:
            self.__batch_index_manager = index_manager

        self.__data = None
        self.__batch_size = 1
        self.__batch_count = 1
        self.__batch_size_automatic = 1

        self.__batch_type = 'none'

    def get_length(self):
        return self.__batch_index_manager.get_length()

    def add_data(self, new_data):
        if self.__data is None:
            self.__data = new_data
        else:
            if isinstance(self.__data, np.ndarray):
                self.__data = np.append(self.__data, new_data, 0)
            else:
                self.__data.append(new_data)

        if self.__batch_type == 'none':
            self.__batch_index_manager.set_parameters(self.__data.shape[0])
        elif self.__batch_type == 'by_size':
            self.__batch_index_manager.set_parameters(self.__data.shape[0], batch_size=self.__batch_size)
        elif self.__batch_type == 'by_count':
            self.__batch_index_manager.set_parameters(self.__data.shape[0], batch_count=self.__batch_count)
        elif self.__batch_type == 'by_count_and_size':
            self.__batch_index_manager.set_parameters(
                self.__data.shape[0],
                batch_count=self.__batch_count,
                batch_size=self.__batch_size
            )

    def set_batch_count(self, new_batch_count):
        self.__batch_type = 'by_count'
        self.__batch_count = new_batch_count
        self.__batch_index_manager.set_parameters(
            self.__data.shape[0] if self.__data is not None else 0,
            batch_count=self.__batch_count
        )

    def set_batch_size(self, new_batch_size):
        self.__batch_type = 'by_size'
        self.__batch_size = new_batch_size
        self.__batch_index_manager.set_parameters(
            self.__data.shape[0] if self.__data is not None else 0,
            batch_size=self.__batch_size
        )

    def set_batch_parameters(self, new_batch_count, new_batch_size):
        self.__batch_type = 'by_count_and_size'
        self.__batch_count = new_batch_count
        self.__batch_size = new_batch_size
        self.__batch_index_manager.set_parameters(
            self.__data.shape[0] if self.__data is not None else 0,
            new_batch_count,
            new_batch_size
        )

    def get_data(self):
        return self.__data

    def get_current_batch(self):
        return self.get_items_by_index(
            self.__batch_index_manager.get_current_batch_indices()
        )

    def reset_batch(self):
        self.__batch_index_manager.reset_batch_index()

    def next_batch(self, reset_batch_index=False):
        return self.__batch_index_manager.set_next_batch_index(reset_batch_index)

    def get_items_by_index(self, indices):
        return self.__data[indices]
