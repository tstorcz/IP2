import numpy as np


class TimeSeriesCollectionDataSource(object):

    def __init__(self, x, y_true):
        # shape[0] is the number of time series in the collection
        self.max_interval_length = y_true.shape[1]
        self.y_true = y_true
        self.x_data = x

    def add_new_data(self, new_x, new_y):
        self.x_data = np.append(self.x_data, new_x, 1)
        self.y_true = np.append(self.y_true, new_y, 1)

    def get_random_index_of_series(self):
        return np.random.rand(self.x_data.shape[0], 1)

    def get_interval(self, indices_of_series, x_start, time_steps):
        if x_start + time_steps > self.max_interval_length:
            raise IndexError()

        indices = x_start + np.arange(0.0, time_steps)
        selected_x = np.take(self.x_data, indices_of_series, 0)
        selected_y = np.take(self.y_true, indices_of_series, 0)
        batch_x = np.take(selected_x, indices, 1)
        batch_y = np.take(selected_y, indices, 1)

        return TimeSeriesCollectionDataSource.__get_batch_tuple(indices, batch_x, batch_y, time_steps)

    def get_random_intervals(self, indices_of_series, batch_count, time_steps):
        start_indices = np.random.rand(batch_count, 1) * (self.max_interval_length - time_steps)
        indices = (start_indices + np.arange(0.0, time_steps)).astype(int)
        selected_x = np.take(self.x_data, indices_of_series, 0)
        selected_y = np.take(self.y_true, indices_of_series, 0)
        batch_x = np.take(selected_x, indices, 1)
        batch_y = np.take(selected_y, indices, 1)

        return TimeSeriesCollectionDataSource.__get_batch_tuple(indices, batch_x, batch_y, time_steps)

    @staticmethod
    # returned arrays:
    #   indices
    #   data
    #   labels
    def __get_batch_tuple(indices, batch_x, batch_y, batch_size):
        return indices, batch_x, batch_y
