from BatchLabeledDataSource import BatchLabeledDataSource


class BatchDataSource(object):

    # training, validation, test: [samples, labels] list of numpy.ndarray-s
    # index_manager: train set index manager (default: DataBatchManager)
    def __init__(self,
                 train=None,
                 validation=None,
                 test=None,
                 index_manager=None):

        self.train_set = BatchLabeledDataSource(index_manager)
        self.train_set.add_data(train[0], train[1])
        self.validation_set = validation
        self.test_set = test

        self.structure = [
            train[0].shape[0] if train is not None else 0,
            validation[0].shape[0] if validation is not None else 0,
            test[0].shape[0] if test is not None else 0
        ]

    def get_current_training_batch(self):
        return self.train_set.get_current_batch()

    def get_next_training_batch(self):
        self.train_set.next_batch()
        return self.train_set.get_current_batch()

    def get_validation_item(self, index):
        if index <= self.structure[1]:
            return self.validation_set[0][index], self.validation_set[1][index]

        return None

    def get_test_item(self, index):
        if index <= self.structure[2]:
            return self.test_set[0][index], self.test_set[1][index]

        return None
