import random
import numpy
import tensorflow as tf


class DataSource(object):

    @staticmethod
    def reshape_image_batch_tensor(image_tensor, image_shape):
        tf.reshape(image_tensor, -1, image_shape[0], image_shape[1], image_shape[2])

    @staticmethod
    def __get_data_set_tuple(collection):
        # converts array oc arrays to 2-tuple
        if collection is not None:
            return tuple(collection)
        return numpy.asarray([]), numpy.asarray([])

    @staticmethod
    def one_hot_from_index(index_array, classes):
        targets = numpy.array(index_array).reshape(-1)
        return numpy.eye(classes)[targets]

    # training, validation, test: [samples, labels] list of numpy.ndarray-s
    # batch_size: size of trainer batches
    def __init__(self,
                 train=None,
                 validation=None,
                 test=None,
                 batch_size=50):

        self.test_set = DataSource.__get_data_set_tuple(test)
        self.validation_set = DataSource.__get_data_set_tuple(validation)
        self.train_set = DataSource.__get_data_set_tuple(train)
        self.structure = [
            train[0].shape[0] if train is not None else 0,
            validation[0].shape[0] if validation is not None else 0,
            test[0].shape[0] if test is not None else 0
        ]

        self.current_batch_index = 0
        self.batch_size = 0
        self.max_batch_index = 0
        self.set_batch_size(batch_size)

    def set_batch_size(self, new_batch_size):
        self.batch_size = new_batch_size
        self.max_batch_index = int(self.structure[0] / self.batch_size)
        self.set_batch_index(self.current_batch_index)

    def get_random_training_batch(self, batch_size=None):
        if batch_size is None:
            batch_size = self.batch_size

        batch_indices = DataSource.__get_random_indices(self.structure[0], batch_size)

        return (
            self.train_set[0][batch_indices],
            self.train_set[1][batch_indices]
        )

    def reset_batch_index(self):
        self.current_batch_index = 0

    def set_batch_index(self, index):
        self.current_batch_index = index

        if self.current_batch_index > self.max_batch_index:
            self.current_batch_index = self.max_batch_index + 1

        return 0 <= self.current_batch_index <= self.max_batch_index

    def get_current_training_batch(self):
        return self.get_training_batch(
            self.current_batch_index * self.batch_size,
            self.batch_size
        )

    def get_next_training_batch(self):
        self.set_batch_index(self.current_batch_index + 1)
        return self.get_current_training_batch()

    def get_training_batch(self, start_index, batch_size):
        if 0 <= self.current_batch_index <= self.max_batch_index:
            return DataSource.__get_batch(self.train_set, start_index, batch_size)

        return None

    def get_validation_item(self, index):
        if index <= self.structure[1]:
            return self.validation_set[0][index], self.validation_set[1][index]

        return None

    def get_test_item(self, index):
        if index <= self.structure[2]:
            return self.test_set[0][index], self.test_set[1][index]

        return None

    @staticmethod
    def __get_random_indices(upper_bound, random_count):
        indices = list(range(0, upper_bound))
        return [indices.pop(random.randrange(len(indices))) for _ in range(0, random_count)]

    @staticmethod
    def __get_batch(source_tuple_of_sample_arrays, first_item_index, batch_size):
        return (
            source_tuple_of_sample_arrays[0][
                first_item_index:
                min(first_item_index + batch_size, source_tuple_of_sample_arrays[0].shape[0])
            ],
            source_tuple_of_sample_arrays[1][
                first_item_index:
                min(first_item_index + batch_size, source_tuple_of_sample_arrays[1].shape[0])
            ]
        )
