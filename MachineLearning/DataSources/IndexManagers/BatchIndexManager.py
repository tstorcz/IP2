import math
import random

from MachineLearning.DataSources.IndexGenerators.DepthFirstBatchIndexGenerator import DepthFirstBatchIndexGenerator


class BatchIndexManager:

    def __init__(self, list_size=0, batch_count=0, batch_size=0,
                 drop_incomplete=False, extend_incomplete=False,
                 batch_index_generator=None
                 ):
        self._list_size = 0
        self._batch_count = 0
        self._batch_size = 0
        self._batch_auto_size = 0
        self._current_batch_index = 0
        self.__drop_incomplete = drop_incomplete
        self.__extend_incomplete = extend_incomplete

        self._batch_indices = []
        self._batch_extension_count = []
        self._first_shorter_batch_index = -1

        if batch_index_generator is not None:
            self._index_generator = batch_index_generator
        else:
            self._index_generator = DepthFirstBatchIndexGenerator(self._list_size, self._batch_count, self._batch_size)

        self.set_parameters(list_size, batch_count, batch_size)

        self.reset_batch_index()

    def set_parameters(self, list_size, batch_count=0, batch_size=0):
        list_size = int(list_size)

        batch_count = int(batch_count)
        batch_size = int(batch_size)

        self._list_size = list_size

        if self._list_size <= 0:
            self._list_size = 0
            self._batch_count = 1
            self._batch_size = 1
            self._batch_auto_size = 0
        else:
            if batch_count == 0 and batch_size == 0:
                batch_count = 1
                batch_size = list_size

            if batch_size > 0:
                self._batch_size = batch_size
            else:

                if self.__drop_incomplete:
                    # creates only full batches, unused items could exist
                    self._batch_size = int(math.floor(list_size / batch_count))
                else:
                    # use all items, incomplete bach could exist
                    self._batch_size = int(math.ceil(list_size / batch_count))

            if batch_count > 0:
                self._batch_count = batch_count
                if self.__drop_incomplete:
                    # creates only full automatic batches, unused items could exist
                    self._batch_auto_size = int(math.floor(list_size / self._batch_count))
                else:
                    # use all items, incomplete automatic bach could exist
                    self._batch_auto_size = int(math.ceil(list_size / self._batch_count))
            else:
                self._batch_auto_size = self._batch_size
                if self.__drop_incomplete:
                    self._batch_count = int(math.floor(list_size / batch_size))
                    self._list_size = list_size = self._batch_count * batch_size
                else:
                    self._batch_count = int(math.ceil(list_size / batch_size))

        self._index_generator.set_parameters(
            self._list_size, self._batch_count, self._batch_size
        )

        self._index_generator.create_batches()

        self.generate_batch_indices()

    def get_length(self):
        return self._list_size

    def get_batch_count(self):
        return self._batch_count

    def get_batch_size(self):
        return self._batch_size

    def get_current_batch_index(self):
        return self._current_batch_index

    def reset_batch_index(self):
        self._current_batch_index = 0

    def set_batch_index(self, index):
        self._current_batch_index = index

        if self._current_batch_index >= self._batch_count:
            self._current_batch_index = self._batch_count + 1

        return 0 <= self._current_batch_index < self._batch_count

    def set_next_batch_index(self, reset_batch_index=False):
        can_continue = self.set_batch_index(self._current_batch_index + 1)

        if not can_continue and reset_batch_index:
            self.reset_batch_index()
            can_continue = True

        return can_continue

    def generate_batch_indices(self):
        self.reset_batch_index()

        self._batch_indices, self._first_shorter_batch_index = self._index_generator.create_batches()

        self._batch_extension_count = [0] * len(self._batch_indices)
        if self.__extend_incomplete and self._list_size > 0:
            for batch_index in range(0, len(self._batch_indices)):
                batch = self._batch_indices[batch_index]
                extension_count = self._batch_size - len(batch)
                if len(batch) < self._batch_size:
                    batch.extend(
                        self._get_indices_out_of_batch(
                            batch_index,
                            len(batch),
                            extension_count
                        )
                    )

                self._batch_extension_count[batch_index] = extension_count

    def get_current_batch_indices(self):
        return self._batch_indices[self.get_current_batch_index()].copy()

    def get_current_batch_extension_count(self):
        return self._batch_extension_count[self.get_current_batch_index()]

    def _get_indices_out_of_batch(self, batch_index, items_to_skip, new_item_count):
        current_batch_size = len(self._batch_indices[batch_index])
        indices = list(range(0, self._list_size - current_batch_size))
        selected_indices = [indices.pop(random.randrange(len(indices))) for _ in range(0, new_item_count)]
        start_index = self._calc_start_of_batch(batch_index)
        end_index = start_index + len(self._batch_indices[batch_index])
        selected_indices = [n + current_batch_size if start_index <= n < end_index else n for n in selected_indices]

        selected_indices = [
            self._batch_indices
            [self._calc_bach_of_raw_index(n)[0]]
            [self._calc_bach_of_raw_index(n)[1]]
            for n in selected_indices
        ]

        return selected_indices

    def _calc_start_of_batch(self, batch_index):
        full_size = len(self._batch_indices[0]) - self._batch_extension_count[0]
        incomplete_size = len(self._batch_indices[-1]) - self._batch_extension_count[-1]

        start = 0
        if batch_index < self._first_shorter_batch_index:
            start += batch_index * full_size
        else:
            start = self._first_shorter_batch_index * full_size
            start += (batch_index - self._first_shorter_batch_index) * incomplete_size

        return start

    def _calc_bach_of_raw_index(self, raw_index):
        full_size = len(self._batch_indices[0]) - self._batch_extension_count[0]
        incomplete_size = len(self._batch_indices[-1]) - self._batch_extension_count[-1]

        border_raw_index = self._first_shorter_batch_index * full_size
        if raw_index < border_raw_index:
            return raw_index // full_size, raw_index % full_size

        raw_index -= border_raw_index
        return raw_index // incomplete_size + self._first_shorter_batch_index, raw_index % incomplete_size
