import numpy as np


class TimeSeriesDataSource(object):
    # data source priority: y_true, generator_function, linear(zeros)
    def __init__(self, x_min, x_max, number_of_points=None,
                 y_true=None, generator_function=None,
                 time_steps=None):
        self.x_min = x_min
        self.x_max = x_max
        self.time_steps = time_steps

        if y_true is not None:
            self.number_of_points = y_true.shape[0]
        else:
            if generator_function is not None:
                self.number_of_points = number_of_points
            else:
                self.number_of_points = x_max - x_min

        self.resolution = (x_max - x_min) / self.number_of_points
        self.x_data = np.linspace(x_min, x_max, self.number_of_points)

        if y_true is not None:
            self.y_true = y_true
        else:
            if generator_function is not None:
                self.y_true = generator_function(self.x_data)
            else:
                self.y_true = np.zeros(self.number_of_points)

    def add_new_data(self, new_y):
        self.x_max = self.x_max + self.resolution
        self.x_data = np.append(self.x_data, [self.x_max], 1)
        self.y_true = np.append(self.y_true, [new_y], 1)

    def get_batch(self, x_start, time_steps=None, return_batch_x=False):
        if time_steps is None:
            time_steps = self.time_steps
        batch_x = x_start + np.arange(0.0, time_steps + 1) * self.resolution
        batch_y = self.y_true[batch_x]

        return TimeSeriesDataSource.__get_batch_tuple(batch_x, batch_y, return_batch_x)

    def get_random_batches(self, batch_count, time_steps=None, return_batch_x=False):
        if time_steps is None:
            time_steps = self.time_steps
        start_indices = np.random.rand(batch_count, 1) * (self.number_of_points - time_steps)
        indices = (start_indices + np.arange(0.0, time_steps + 1)).astype(int)
        batch_x = np.take(self.x_data, indices)
        batch_y = np.take(self.y_true, indices)

        return TimeSeriesDataSource.__get_batch_tuple(batch_x, batch_y, time_steps, return_batch_x)

    @staticmethod
    # returned arrays
    #   training data (y): [0..n-1],
    #   data to predict (y): [1..n]
    #   time moments (X): [0..n] if requested
    def __get_batch_tuple(batch_x, batch_y, batch_size, return_batch_x=False):
        y_batches = \
            batch_y[:, :-1].reshape(-1, batch_size, 1), \
            batch_y[:, 1:].reshape(-1, batch_size, 1)
        if return_batch_x:
            return y_batches + (batch_x,)
        else:
            return y_batches
