import math
import numpy
import random


class KMeans(object):

    def __init__(self, number_of_clusters, max_iterations):
        self.number_of_clusters = number_of_clusters
        self.max_iterations = max_iterations
        self.__centroids = []
        self.default_centroids = None

    @property
    def centroids(self):
        return self.__centroids

    @centroids.setter
    def centroids(self, new_centroids):
        self.__centroids = new_centroids
        self.default_centroids = new_centroids

    def fit(self, data_set):
        if isinstance(self.default_centroids, list):
            self.__centroids = self.default_centroids
        else:
            self.__get_random_centroids(data_set)

        iterations = 0
        old_centroids = None

        while not self.__should_stop(old_centroids, iterations):
            old_centroids = self.__centroids
            iterations += 1

            labels = self.estimate(data_set)

            self.__get_centroids(data_set, labels)

        return iterations < self.max_iterations

    def __get_random_centroids(self, data_set):
        self.__centroids = []
        centroid_indices = []
        for ci in range(0, self.number_of_clusters):
            index = random.randint(0, len(data_set) - 1 - ci)
            for i in centroid_indices:
                if i <= index:
                    index += 1

            centroid_indices.append(index)
            self.__centroids.append(data_set[index])

    def __should_stop(self, old_centroids, iterations):
        if iterations >= self.max_iterations:
            return True

        if not isinstance(old_centroids, list):
            return False

        for c in range(0, len(old_centroids)):
            for d in range(0, KMeans.__get_dimensions(old_centroids[c])):
                if KMeans.__get_item(old_centroids[c], d) != KMeans.__get_item(self.__centroids[c], d):
                    return False

        return True

    def estimate(self, test_data_set):
        labels = [0] * len(test_data_set)
        for point_index in range(0, len(test_data_set)):
            labels[point_index] = 0
            min_distance = KMeans.__calculate_distance(test_data_set[point_index], self.__centroids[0])
            for centroid_index in range(0, len(self.__centroids)):
                distance = KMeans.__calculate_distance(test_data_set[point_index], self.__centroids[centroid_index])
                if min_distance > distance:
                    min_distance = distance
                    labels[point_index] = centroid_index

        return labels

    @staticmethod
    def __calculate_distance(p1, p2):
        sum = 0

        for i in range(0, KMeans.__get_dimensions(p1)):
            sum += math.pow(KMeans.__get_item(p2, i) - KMeans.__get_item(p1, i), 2)

        return math.sqrt(sum)

    def __get_centroids(self, data_set, labels):
        dimensions = KMeans.__get_dimensions(data_set[0])
        sums = numpy.zeros((len(self.__centroids), dimensions + 1))
        for pi in range(0, len(data_set)):
            sums[labels[pi], dimensions] += 1
            for di in range(0, dimensions):
                sums[labels[pi], di] += KMeans.__get_item(data_set[pi], di)

        number_of_centroids = len(self.__centroids)
        self.__centroids = []
        for ci in range(0, number_of_centroids):
            if dimensions > 1:
                self.__centroids.append([])
                for di in range(0, dimensions):
                    if sums[ci, dimensions] == 0:
                        self.__centroids[ci].append(0)
                    else:
                        self.__centroids[ci].append((sums[ci, di] / sums[ci, dimensions]))
            else:
                if sums[ci, dimensions] == 0:
                    self.__centroids.append(0)
                else:
                    self.__centroids.append((sums[ci, 0] / sums[ci, dimensions]))

    @staticmethod
    def __get_dimensions(value):
        if isinstance(value, list) or isinstance(value, numpy.ndarray) or isinstance(value, tuple):
            return len(value)
        else:
            return 1

    @staticmethod
    def __get_item(value, index):
        if isinstance(value, list) or isinstance(value, numpy.ndarray) or isinstance(value, tuple):
            return value[index]
        else:
            return value
