import tensorflow as tf
import typing

from NeuralNetwork import NeuralNetwork


class CustomNeuralNetwork(NeuralNetwork):
    def __init__(self, shape_of_input, number_of_output):
        super().__init__(shape_of_input, number_of_output)

        self.input = tf.keras.Input(shape=shape_of_input)
        self.shape_of_input = shape_of_input
        self.last_output = self.input
        self.dropout_rate = 0
        self.model = None

    def add_layer(self, new_layer: tf.keras.layers.Layer):
        self.last_output = new_layer(self.last_output)

    def add_layer_block(self, new_layer: typing.Callable):
        self.last_output = new_layer(self.last_output)
