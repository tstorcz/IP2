import numpy as np
import tensorflow as tf


class AutoEncoder(tf.keras.Model):
    def __init__(self, input_units, encoded_units,
                 hidden_activation=None,
                 output_activation=None):
        super(AutoEncoder, self).__init__()
        self.input_units = input_units
        self.encoded_units = encoded_units

        self.inference_net = tf.keras.Sequential()
        self.inference_net.add(tf.keras.layers.InputLayer(input_shape=(input_units,)))

        self.generative_net = tf.keras.Sequential()
        self.generative_net.add(tf.keras.layers.InputLayer(input_shape=(encoded_units,)))

        for i in range(1, input_units - encoded_units + 1, 1):
            self.inference_net.add(
                tf.keras.layers.Dense(
                    units=input_units - i,
                    activation=hidden_activation
                )
            )

            decoder_unit_count = encoded_units + i
            self.inference_net.add(
                tf.keras.layers.Dense(
                    units=decoder_unit_count,
                    activation=hidden_activation if decoder_unit_count < input_units else None
                )
            )

        if output_activation is not None:
            self.inference_net.add(
                tf.keras.layers.Dense(
                    units=input_units,
                    activation=output_activation
                )
            )

    @tf.function
    def sample(self, eps=None):
        if eps is None:
            eps = tf.random.normal(shape=(100, self.encoded_units))
        return self.decode(eps, apply_sigmoid=True)

    def encode(self, x):
        mean, logvar = tf.split(self.inference_net(x), num_or_size_splits=2, axis=1)
        return mean, logvar

    @staticmethod
    def reparameterize(mean, logvar):
        eps = tf.random.normal(shape=mean.shape)
        return eps * tf.exp(logvar * .5) + mean

    def decode(self, z, apply_sigmoid=False):
        logits = self.generative_net(z)
        if apply_sigmoid:
            probs = tf.sigmoid(logits)
            return probs

        return logits

    @staticmethod
    def _optimizer(learning_rate=1e-4):
        return tf.keras.optimizers.Adam(learning_rate=learning_rate)

    @staticmethod
    def __log_normal_pdf(sample, mean, logvar, raxis=1):
        log2pi = tf.math.log(2. * np.pi)
        return tf.reduce_sum(
            -.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi),
            axis=raxis)

    @tf.function
    def _compute_loss(self, x):
        mean, logvar = self.encode(x)
        z = self.reparameterize(mean, logvar)
        x_logit = self.decode(z)

        cross_ent = tf.nn.sigmoid_cross_entropy_with_logits(logits=x_logit, labels=x)
        logpx_z = -tf.reduce_sum(cross_ent, axis=[1, 2, 3])
        logpz = AutoEncoder.__log_normal_pdf(z, 0., 0.)
        logqz_x = AutoEncoder.__log_normal_pdf(z, mean, logvar)
        return -tf.reduce_mean(logpx_z + logpz - logqz_x)

    @tf.function
    def _compute_apply_gradients(self, x, optimizer):
        with tf.GradientTape() as tape:
            loss = AutoEncoder._compute_loss(self, x)
        gradients = tape.gradient(loss, self.trainable_variables)
        optimizer.apply_gradients(zip(gradients, self.trainable_variables))

    @tf.function
    def train(self, dataset):
        for data_item in dataset:
            self._compute_apply_gradients(self, data_item, self._optimizer())
