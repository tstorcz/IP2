import numpy as np
import tensorflow as tf

from MachineLearning.NeuralNets.GenericNeuralNetwork import GenericNeuralNetwork


# Virtual abstract class, should not be subject of instantiation!
# On Inheritance, override protected methods to implement expected structure
class NeuralNetwork(GenericNeuralNetwork):

    def __init__(self, shape_of_input, number_of_output):
        super().__init__(number_of_output)

        if type(shape_of_input) is not tuple:
            if type(shape_of_input) is not list:
                shape_of_input = [shape_of_input]
            shape_of_input = tuple(shape_of_input)

        self.shape_of_input = shape_of_input

        self.model = tf.keras.Sequential()
        self.last_train_history = None

    def _initialize_layers(self):
        self.model.add(
            tf.keras.layers.InputLayer(
                input_shape=self.shape_of_input
            )
        )

    def predict(self, x):
        return self.model.predict(np.reshape(x, (-1,) + self.shape_of_input))

    def reset_similar(self, threshold):
        for layer in self.model.layers:
            params = layer.get_weights()
            corr = np.absolute(np.corrcoef(params[0]))
            for i in range(corr.shape[0]):
                corr[i, i] = 0

            if corr[0, 0] > threshold:
                layer.set_weights(params)
