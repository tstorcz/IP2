import numpy as np
import tensorflow as tf
from keras.models import load_model


# Virtual abstract class, should not be subject of instantiation!
# On Inheritance, override protected methods to implement expected structure
class GenericNeuralNetwork(object):

    def __init__(self, number_of_output):
        self.loss = tf.keras.losses.MSE
        self.metrics = [tf.keras.metrics.Accuracy(), tf.keras.metrics.MSE]
        self.optimizer = tf.keras.optimizers.Adam()

        self.number_of_output = number_of_output
        self.learning_rate = 0

        self.last_train_history = None

        self.model = None
        self.__is_loaded = False

        self._last_train_epochs = 1
        self._last_train_batch_size = 32
        self._last_train_steps_per_epoch = None

    def is_loaded(self):
        return self.__is_loaded

    def _initialize_layers(self):
        pass

    def _add_final_layers(self):
        pass

    def compile(self,
                learning_rate=0.001,
                loss=None,
                optimizer=None,
                metrics=None):

        self.learning_rate = learning_rate

        if loss is None:
            loss = self.loss

        if optimizer is None:
            if self.optimizer is not None:
                optimizer = self.optimizer
            else:
                optimizer = tf.keras.optimizers.Adam(self.learning_rate)

        if metrics is None:
            metrics = self.metrics

        self.model.compile(
            loss=loss,
            optimizer=optimizer,
            metrics=metrics
        )

    def training_step(self, x_batch, y_batch):
        metrics = self.model.train_on_batch(x_batch, y_batch)
        return metrics

    def train(self, x, y, epochs=1, batch_size=32, steps_per_epoch=None, verbose=False):
        self._last_train_epochs = epochs
        self._last_train_batch_size = batch_size
        self._last_train_steps_per_epoch = steps_per_epoch

        self.last_train_history = self.model.fit(
            x, y,
            epochs=epochs, batch_size=batch_size, steps_per_epoch=steps_per_epoch,
            verbose=verbose
        )

        return self.last_train_history

    def fit(self, x, y):
        return self.train(x, y, self._last_train_epochs, self._last_train_batch_size, self._last_train_steps_per_epoch)

    def score(self, x, y_true, sample_weight=None):
        y_predicted = self.predict(x)
        score = 1 - ((np.subtract(y_true, y_predicted) ** 2).sum() / ((np.subtract(y_true, y_true.mean()) ** 2).sum()))
        return score

    def evaluate(self, x, y):
        return self.model.evaluate(x, y)

    def predict(self, x):
        return self.model.predict(x)

    def summary(self):
        return self.model.summary()

    def to_json(self):
        return self.model.to_json()

    def save(self, file_path):
        self.model.save(file_path)

    def load(self, file_path):
        self.model = load_model(file_path)
        # no need for model compilation
        self.__is_loaded = True

    def save_weights(self, file_path):
        self.model.save_weights(filepath=file_path)

    def load_weights(self, file_path):
        self.model.load_weights(file_path)

    def create_optimizer(self, optimizer_type, learning_rate):
        if optimizer_type.lower() == "adam":
            self.optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)

        return self.optimizer

    def _get_binary_classification_structure(self):
        return [
                [self.number_of_output, tf.keras.activations.sigmoid]
            ]

    def _get_probability_calculator_structure(self):
        return [
                [self.number_of_output, None],
                [self.number_of_output, tf.keras.activations.softmax]
            ]

    def _get_regression_calculator_structure(self):
        return [
                [self.number_of_output, tf.keras.activations.linear]
            ]