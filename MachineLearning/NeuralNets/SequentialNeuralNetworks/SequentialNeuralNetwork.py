import tensorflow as tf


from MachineLearning.NeuralNets.NeuralNetwork import NeuralNetwork


# Virtual abstract class, should not be subject of instantiation!
# On Inheritance, override protected methods to implement expected structure
class SequentialNeuralNetwork(NeuralNetwork):

    def __init__(self, shape_of_input, number_of_output):
        super().__init__(shape_of_input, number_of_output)

        self._initialize_layers()
        self._add_final_layers()

    def _initialize_layers(self):
        self.model.add(
            tf.keras.layers.InputLayer(
                input_shape=self.shape_of_input
            )
        )

    def _add_final_layers(self):
        pass

    def add_layer(self, layer):
        self.model.add(layer)

    @staticmethod
    def binary_activation(x):
        return tf.keras.backend.round(x)

    @staticmethod
    def binary_activation_sign(x):
        cond = tf.greater(x, tf.zeros(tf.shape(x)))
        out = tf.where(cond, tf.ones(tf.shape(x)), tf.ones(tf.shape(x))*-1)
        return tf.keras.backend.round(out)
