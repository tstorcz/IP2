import tensorflow as tf

from MachineLearning.NeuralNets.SequentialNeuralNetworks.SequentialNeuralNetwork import SequentialNeuralNetwork


class RecurrentSequentialNeuralNetwork(SequentialNeuralNetwork):

    def __init__(self, time_steps, structure):
        self.structure = structure
        number_of_input = structure[0]
        if type(number_of_input) is not tuple:
            if type(number_of_input) is not list:
                number_of_input = [number_of_input]
            number_of_input = tuple(number_of_input)

        self.time_steps = time_steps
        super().__init__((time_steps,) + number_of_input, structure[3])

    def _initialize_layers(self):
        super()._initialize_layers()

        self.model.add(
            tf.keras.layers.Reshape(
                target_shape=self.shape_of_input
            )
        )

        self.model.add(
            RecurrentSequentialNeuralNetwork._recurrent_layer_factory(
                units=self.structure[1],
                activation=self.structure[2]
            )
        )

        self.model.add(
            tf.keras.layers.Dense(
                units=self.number_of_output,
                activation=None
            )
        )

    @staticmethod
    def _recurrent_layer_factory(units, activation):
        return tf.keras.layers.SimpleRNN(
                units=units,
                activation=activation
            )
