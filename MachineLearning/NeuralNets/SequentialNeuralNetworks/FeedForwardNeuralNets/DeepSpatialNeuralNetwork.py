import tensorflow as tf

from MachineLearning.NeuralNets.SequentialNeuralNetworks.SequentialNeuralNetwork import SequentialNeuralNetwork


class DeepSpatialNeuralNetwork(SequentialNeuralNetwork):

    # shape_of_images: [x, y] the channel size has to be 3
    # conv3d_parameters:
    #       [padding_size, kernel size, kernel_count, activation]
    # 2D kernel size of single 3D convolution layer
    #       3rd dimension of kernel is specified by OneHot input encoding
    # layer_parameters: list of list of parameters of each convolutional layers
    #   [
    #       [
    #           zero_padding_count,
    #           kernel_shape,
    #           output_feature_count (kernel count),
    #           activation_function,
    #           max_pooling_window_size (1=No max pooling)
    #       ],
    #       [...]
    #   ]
    #   for example: [[5, 32, None, 2], [5, 64, tf.nn.relu, 2]]
    # dense_layer_parameters: list of list of parameters of each dense layers
    #   [[number_of_neurons, activation_function], [...]]
    #   for example: [[1024, tf.nn.relu], [10, tf.nn.softmax]]
    def __init__(self,
                 shape_of_input,
                 conv3d_parameters,
                 conv2d_layer_parameters,
                 dense_layer_parameters,
                 number_of_output=None,
                 use_batch_norm=False):
        if number_of_output is None:
            number_of_output = dense_layer_parameters[-1][0]
        self.conv3d_layer_parameters = conv3d_parameters
        self.conv2d_layer_parameters = conv2d_layer_parameters
        self.dense_layer_parameters = dense_layer_parameters
        self.use_batch_norm = use_batch_norm
        self.dropout_rate = 0
        super().__init__(shape_of_input, number_of_output)

        self.create_optimizer("adam", learning_rate=0.05)

    def _initialize_layers(self):
        super()._initialize_layers()

        if self.conv3d_layer_parameters is not None:
            self.add_conv3d_layers(self.conv3d_layer_parameters)
        else:
            self.model.add(
                tf.keras.layers.Reshape(
                    target_shape=(self.shape_of_input[0], self.shape_of_input[1], 1) if len(self.shape_of_input) == 2 else self.shape_of_input
                )
            )

        if self.conv2d_layer_parameters is not None:
            self.add_conv2d_layers()

        if self.dense_layer_parameters is not None:
            self.add_dense_layers()

    def add_conv3d_layers(self, layer_parameters=None):
        if layer_parameters is None:
            layer_parameters = self.conv3d_layer_parameters

        self.model.add(
            tf.keras.layers.Reshape(
                target_shape=(self.shape_of_input[0], self.shape_of_input[1], self.shape_of_input[2], 1)
            )
        )

        if layer_parameters[0] > 0:
            self.model.add(
                tf.keras.layers.ZeroPadding3D(
                    padding=(layer_parameters[0], layer_parameters[0], 0)
                )
            )

        self.model.add(
            tf.keras.layers.Conv3D(
                kernel_size=(layer_parameters[1], layer_parameters[1], self.shape_of_input[-1]),
                filters=layer_parameters[2],
                activation=layer_parameters[3],
                padding='valid'
            )
        )

        self.model.add(
            tf.keras.layers.Reshape(
                target_shape=(self.shape_of_input[0], self.shape_of_input[1], layer_parameters[2])
            )
        )

    def add_conv2d_layers(self, parameters=None):
        if parameters is None:
            parameters = self.conv2d_layer_parameters

        # convolutional layers with batch normalization and pooling
        for layer_parameters in parameters:
            if layer_parameters[0] > 0:
                self.model.add(
                    tf.keras.layers.ZeroPadding2D(
                        padding=(layer_parameters[0], layer_parameters[0])
                    )
                )

            self.model.add(
                tf.keras.layers.Conv2D(
                    kernel_size=layer_parameters[1],
                    filters=layer_parameters[2],
                    activation=layer_parameters[3],
                    padding='valid'
                )
            )

            if self.use_batch_norm:
                self.model.add(
                    tf.keras.layers.BatchNormalization()
                )

            if layer_parameters[4] >= 1:
                self.model.add(
                    tf.keras.layers.MaxPool2D(
                        pool_size=(layer_parameters[4], layer_parameters[4])
                    )
                )

    def add_dense_layers(self, parameters=None):
        if parameters is None:
            parameters = self.dense_layer_parameters

        # flattening between conv and dense layers
        if len(parameters) > 0:
            self.model.add(
                tf.keras.layers.Flatten()
            )

        # dense layers
        last_index = len(parameters) - 1
        for idx, layer_parameters in enumerate(parameters):
            self.model.add(
                tf.keras.layers.Dense(
                    units=layer_parameters[0],
                    activation=layer_parameters[1]
                )
            )
            if idx < last_index and self.dropout_rate > 0:
                self.model.add(
                    tf.keras.layers.Dropout(
                        rate=self.dropout_rate
                    )
                )

    def get_structure_descriptor(self):
        descriptor = "DSNN"

        if self.is_loaded():
            descriptor = "{0}_loaded".format(descriptor)
        else:
            if self.conv3d_layer_parameters is not None:
                descriptor = "{0}_{1}D3c{2}".format(
                    descriptor, self.conv3d_layer_parameters[0], self.conv3d_layer_parameters[1]
                )

            if self.conv2d_layer_parameters is not None:
                for conv_layer in self.conv2d_layer_parameters:
                    descriptor = "{0}_{1}p{2}c{3}".format(descriptor, conv_layer[0], conv_layer[1], conv_layer[2])

            descriptor += "__"

            for dense_layer in self.dense_layer_parameters:
                descriptor = "{0}_d{1}".format(descriptor, dense_layer[0])

        return descriptor
