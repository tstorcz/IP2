import tensorflow as tf

from SequentialNeuralNetworks.SequentialNeuralNetwork import SequentialNeuralNetwork


class AutoEncoder(SequentialNeuralNetwork):
    # encoder_stack_parameters: [[neuron_count, activation_function]]
    # encoders are defined to the middle of the autoencoder
    # final encoder is the symmetry axis of the structure
    # decoders are reflections of encoders
    def __init__(self, input_count, encoder_stack_parameters, tied_weights=True,
                 output_activation_function=tf.keras.activations.sigmoid):

        self.input_count = input_count
        self.encoder_stack_parameters = encoder_stack_parameters
        self.tied_weights = tied_weights
        self.output_activation_function = output_activation_function

        super().__init__(input_count, input_count)

    def _initialize_layers(self):
        # input added by default
        super()._initialize_layers()

        # encoders
        neuron_count = [self.input_count]

        for layer_parameters in self.encoder_stack_parameters:
            if layer_parameters[0] == -1:
                neuron_count.append(neuron_count[-1] - 1)
            else:
                neuron_count.append(layer_parameters[0])

            self.model.add(
                tf.keras.layers.Dense(
                    units=neuron_count[-1],
                    activation=layer_parameters[1]
                )
            )

        self.encoder = self.model.layers[-1]
        self.encoder_index = len(self.model.layers) - 1

        # symmetric decoders
        for idx in range(len(self.encoder_stack_parameters) - 2, 0, -1):
            self.model.add(
                tf.keras.layers.Dense(
                    neuron_count[idx+1],
                    activation=self.encoder_stack_parameters[idx][1],
                )
            )

        # output
        if self.output_activation_function is not None:
            self.model.add(
                tf.keras.layers.Dense(
                    units=self.input_count,
                    activation=self.output_activation_function
                )
            )

    def predict(self, x):
        output = self.model.predict(x)
        return output

