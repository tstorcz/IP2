import tensorflow as tf

from MachineLearning.NeuralNets.SequentialNeuralNetworks.SequentialNeuralNetwork import SequentialNeuralNetwork


class DeepSequentialNeuralNetwork(SequentialNeuralNetwork):

    # shape_of_images: [x, y, channels]
    # layer_parameters: list of list of parameters of each convolutional layers
    #   [
    #       [
    #           kernel_shape,
    #           output_feature_count (kernel count),
    #           activation_function,
    #           max_pooling_window_size (1=No max pooling)
    #       ],
    #       [...]
    #   ]
    #   for example: [[5, 32, None, 2], [5, 64, tf.nn.relu, 2]]
    # dense_layer_parameters: list of list of parameters of each dense layers
    #   [[number_of_neurons, activation_function], [...]]
    #   for example: [[1024, tf.nn.relu], [10, tf.nn.softmax]]
    def __init__(self,
                 shape_of_image,
                 convolutional_layer_parameters,
                 dense_layer_parameters,
                 number_of_output=None):
        if number_of_output is None:
            number_of_output = dense_layer_parameters[-1][0]
        self.convolutional_layer_parameters = convolutional_layer_parameters
        self.dense_layer_parameters = dense_layer_parameters
        super().__init__(shape_of_image, number_of_output)

    def _initialize_layers(self):
        super()._initialize_layers()

        if self.convolutional_layer_parameters is not None:
            self.add_convolutional_layers()

        if self.dense_layer_parameters is not None:
            self.add_dense_layers()

    def add_convolutional_layers(self, parameters=None):
        if parameters is None:
            parameters = self.convolutional_layer_parameters

        # convolutional layers with batch normalization and pooling
        for layer_parameters in parameters:
            self.model.add(
                tf.keras.layers.ZeroPadding2D(
                    padding=(int(layer_parameters[0] / 2), int(layer_parameters[0] / 2))
                )
            )

            self.model.add(
                tf.keras.layers.Conv2D(
                    kernel_size=layer_parameters[0],
                    filters=layer_parameters[1],
                    activation=layer_parameters[2],
                    padding='valid'
                )
            )

            self.model.add(
                tf.keras.layers.BatchNormalization()
            )

            if layer_parameters[3] >= 1:
                self.model.add(
                    tf.keras.layers.MaxPool2D(
                        pool_size=(layer_parameters[3], layer_parameters[3])
                    )
                )

    def add_dense_layers(self, parameters=None):
        if parameters is None:
            parameters = self.dense_layer_parameters

        # flattening between conv and dense layers
        if len(parameters) > 0:
            self.model.add(
                tf.keras.layers.Flatten()
            )

        # dense layers
        last_index = len(parameters) - 1
        for idx, layer_parameters in enumerate(parameters):
            self.model.add(
                tf.keras.layers.Dense(
                    units=layer_parameters[0],
                    activation=layer_parameters[1]
                )
            )
            if idx < last_index and self.dropout_rate > 0:
                self.model.add(
                    tf.keras.layers.Dropout(
                        rate=self.dropout_rate
                    )
                )

    def get_structure_descriptor(self):
        descriptor = "DSNN"
        for conv_layer in self.convolutional_layer_parameters:
            descriptor = "{0}_{1}p{2}c{3}".format(descriptor, conv_layer[0], conv_layer[1], conv_layer[2])

        descriptor += "__"

        for dense_layer in self.dense_layer_parameters:
            descriptor = "{0}_d{1}".format(descriptor, dense_layer[0])

        return descriptor
