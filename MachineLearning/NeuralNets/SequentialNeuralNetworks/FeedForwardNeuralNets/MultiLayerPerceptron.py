import tensorflow as tf

from MachineLearning.NeuralNets.SequentialNeuralNetworks.SequentialNeuralNetwork import SequentialNeuralNetwork


class MultiLayerPerceptron(SequentialNeuralNetwork):

    # According to TensorFlow documentation, neuron output = activation(dot(input, kernel) + bias)
    #   So kernel is the weight matrix of the neuron

    # Ref:
    #       https://www.tensorflow.org/api_docs/python/tf/keras/layers/Dense
    #       https://keras.io/layers/core/

    # parameters:
    #   structure: [input_count, hidden_dense_layer_parameters, output_parameters]
    #
    #   hidden_dense_layer_parameters:
    #       list of parameter descriptors of each HIDDEN Dense layers without final layers, like:
    #       [
    #           [unit_count, activation_function],
    #           [...]
    #       ]
    #
    #       for example:
    #       [
    #           [10, tf.keras.activations.relu],
    #           [20, tf.keras.activations.relu]
    #       ]
    #
    #   output_parameters:
    #       [output_count, output_type]
    #       output_type: "binary", "probability", "regression" or the activation function can be directly specified
    def __init__(self, structure):
        self.structure = structure
        super().__init__(structure[0], structure[-1][0])
        self.loss = self._loss_function()

    def get_structure_descriptor(self):
        descriptor = "MLP"

        if self.is_loaded():
            descriptor = "{0}_loaded".format(descriptor)
        else:
            if self.structure[1] is not None:
                for dense_layer in self.structure[1]:
                    descriptor = "{0}_d{1}".format(descriptor, dense_layer[0])

        return descriptor

    def _initialize_layers(self):
        super()._initialize_layers()
        if self.structure[1] is not None:
            for layer_descriptor in self.structure[1]:
                self.model.add(
                    tf.keras.layers.Dense(
                        units=layer_descriptor[0],
                        activation=layer_descriptor[1]
                    )
                )

    def _add_final_layers(self):
        final_structure = []
        if self.structure[-1][1].lower() == "binary":
            final_structure = self.__get_binary_classification_structure()
        elif self.structure[-1][1].lower() == "probability":
            final_structure = self.__get_probability_calculator_structure()
        elif self.structure[-1][1].lower() == "regression":
            final_structure = self.__get_regression_calculator_structure()
        else:
            final_structure = self.structure[-1]

        for layer_descriptor in final_structure:
            self.model.add(
                tf.keras.layers.Dense(
                    units=layer_descriptor[0],
                    activation=layer_descriptor[1]
                )
            )

    def __get_binary_classification_structure(self):
        return [
                [self.structure[-1][0], tf.keras.activations.sigmoid]
            ]

    def __get_probability_calculator_structure(self):
        return [
                [self.structure[-1][0], None],
                [self.structure[-1][0], tf.keras.activations.softmax]
            ]

    def __get_regression_calculator_structure(self):
        return [
                [self.structure[-1][0], tf.keras.activations.linear]
            ]

    def _loss_function(self):
        if self.structure[-1][1].lower() == "binary":
            return tf.keras.losses.binary_crossentropy
        elif self.structure[-1][1].lower() == "probability":
            return tf.keras.losses.categorical_crossentropy
        elif self.structure[-1][1].lower() == "regression":
            return tf.keras.losses.MSE

        return tf.keras.losses.MSE
