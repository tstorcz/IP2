import tensorflow as tf

from MachineLearning.NeuralNets.SequentialNeuralNetworks.SequentialNeuralNetwork import SequentialNeuralNetwork


class LinearSVM(SequentialNeuralNetwork):

    def __init__(self, number_of_inputs):
        self.structure = [number_of_inputs, 1]
        super().__init__(number_of_inputs, 1)

    def _initialize_layers(self):
        super()._initialize_layers()

    def _add_final_layers(self):
        self.model.add(
            tf.keras.layers.Dense(
                self.number_of_output,
                activation='linear', kernel_regularizer=tf.keras.regularizers.l2()
            )
        )

    def _loss_function(self):
        return tf.keras.losses.hinge
