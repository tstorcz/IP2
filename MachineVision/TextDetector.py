import cv2 as cv
import math


class TextDetector(object):

    EAST_PATH = "../../Models/frozen_east_text_detection.pb"

    def __init__(self, work_magnitude, confidence_threshold, nms_threshold):
        self.work_magnitude = work_magnitude
        self.confidence_threshold = confidence_threshold
        self.nms_threshold = nms_threshold

    def __decode_bounding_boxes(self, scores, geometry):
        detections = []
        confidences = []

        ############ CHECK DIMENSIONS AND SHAPES OF geometry AND scores ############
        assert len(scores.shape) == 4, "Incorrect dimensions of scores"
        assert len(geometry.shape) == 4, "Incorrect dimensions of geometry"
        assert scores.shape[0] == 1, "Invalid dimensions of scores"
        assert geometry.shape[0] == 1, "Invalid dimensions of geometry"
        assert scores.shape[1] == 1, "Invalid dimensions of scores"
        assert geometry.shape[1] == 5, "Invalid dimensions of geometry"
        assert scores.shape[2] == geometry.shape[2], "Invalid dimensions of scores and geometry"
        assert scores.shape[3] == geometry.shape[3], "Invalid dimensions of scores and geometry"
        height = scores.shape[2]
        width = scores.shape[3]
        for y in range(0, height):

            # Extract data from scores
            scoresData = scores[0][0][y]
            x0_data = geometry[0][0][y]
            x1_data = geometry[0][1][y]
            x2_data = geometry[0][2][y]
            x3_data = geometry[0][3][y]
            anglesData = geometry[0][4][y]
            for x in range(0, width):
                score = scoresData[x]

                # If score is lower than threshold score, move to next x
                if score < self.confidence_threshold:
                    continue

                # Calculate offset
                offsetX = x * 4.0
                offsetY = y * 4.0
                angle = anglesData[x]

                # Calculate cos and sin of angle
                cosA = math.cos(angle)
                sinA = math.sin(angle)
                h = x0_data[x] + x2_data[x]
                w = x1_data[x] + x3_data[x]

                # Calculate offset
                offset = (
                [offsetX + cosA * x1_data[x] + sinA * x2_data[x], offsetY - sinA * x1_data[x] + cosA * x2_data[x]])

                # Find points for rectangle
                p1 = (-sinA * h + offset[0], -cosA * h + offset[1])
                p3 = (-cosA * w + offset[0], sinA * w + offset[1])
                center = (0.5 * (p1[0] + p3[0]), 0.5 * (p1[1] + p3[1]))
                detections.append((center, (w, h), -1 * angle * 180.0 / math.pi))
                confidences.append(float(score))

        # Return detections and confidences
        return [detections, confidences]

    def detect(self, image_file_name):
        # Load network
        detector = cv.dnn.readNet(TextDetector.EAST_PATH)

        outNames = [
            "feature_fusion/Conv_7/Sigmoid",
            "feature_fusion/concat_3"
        ]

        # Open a video file or an image file or a camera stream
        cap = cv.VideoCapture(image_file_name)

        # Read frame
        hasFrame, frame = cap.read()

        # Create a 4D blob from frame.
        blob = cv.dnn.blobFromImage(
            frame, 1.0,
            (int(frame.shape[1] * self.work_magnitude), int(frame.shape[0] * self.work_magnitude)),
            (123.68, 116.78, 103.94),
            True, False
        )

        # Run the detection model
        detector.setInput(blob)

        outs = detector.forward(outNames)

        # Get scores and geometry
        scores = outs[0]
        geometry = outs[1]
        [boxes, confidences] = self.__decode_bounding_boxes(scores, geometry)

        # Apply NMS
        indices = cv.dnn.NMSBoxesRotated(
            boxes, confidences,
            self.confidence_threshold,
            self.nms_threshold
        )

        self.show(frame, boxes, indices)

    def show(self, orig, boxes, indices):
        for i in indices:
            # get 4 corners of the rotated rect
            vertices = cv.boxPoints(boxes[i[0]])
            # scale the bounding box coordinates based on the respective ratios
            for j in range(4):
                vertices[j][0] *= self.work_magnitude
                vertices[j][1] *= self.work_magnitude

            for j in range(4):
                p1 = (vertices[j][0], vertices[j][1])
                p2 = (vertices[(j + 1) % 4][0], vertices[(j + 1) % 4][1])
                cv.line(orig, p1, p2, (0, 255, 0), 1)

        # Display the frame
        cv.imshow("EAST: An Efficient and Accurate Scene Text Detector", orig)
