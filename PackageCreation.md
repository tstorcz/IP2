#Creating Python package
##[Create package server](https://pypi.org/project/pypiserver/)
```shell script
pip install pypiserver  
pip install passlib  
cd <Package storage server folder>  
md packages  
# create htpasswd file containing username/password  
# https://hostingcanada.org/htpasswd-generator/  
pypi-server -p 8080 -P htpasswd packages
```  

##Create and upload package
###Prerequisites
Python packages:
- setuptools
- wheel  
  
```pip install -U setuptools wheel```
###Structure - package folder contents
####```__init__.py``` - contains python script to get objects to inject into kernel root
```python
from <folder>.<python file> import <class name>
__all__ = [<class name>]		# list of classes to be added to global kernel
```
####```setup.py``` - used to create the package
```python
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="data_edit_window",
    version="0.0.1",
    author="Tamas Storcz",
    author_email="storcz.tamas@mik.pte.hu",
    description="Window creation for simple data editing",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/tstorcz/EditWindow",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)

```
#### ```setup.cfg```
```text
[metadata]
description-file = README.md
```
####```LICENSE```
```<license description>```
####```README.md```
```<readme markdown>```
###Create the package - by running setup.py
```python setup.py sdist```  
Package will be created in ```dist``` folder and will be named as container folder.
###Upload - from dist using twine
	-With .pypirc file
		twine upload --repository internal dist/*
	-Without .pypirc file
		twine upload --repository-url http://localhost:8080 dist/*

###```.pypirc``` - config file for repository credentials
File content can be created with [Apache MD5 hash](https://hostingcanada.org/htpasswd-generator/)  
Place .pypirc file in path of ```$HOME``` enviroinment variable with the following content:
```yaml
[distutils]
index-servers=
	internal

[internal]
repository: http://localhost:8080
username: <username>
password: <password>
```

### Install package in project
PyCharm built in package handler unable to handle repository protected by password.  
Therefore package has to be installed in PyCharm Python console.  
  
Install diretcly with ```pip``` (using ```.pypirc``` file or manual password)  
```pip install <package name> --index-url <local repository server URL>/simple```
