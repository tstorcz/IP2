# IP2
Image Processing in Python

Source for development, testing and utilization of image processing methods.

Tested in Python 3.7 virtual environment of PyCharm community edition.  
**Does NOT work with 3.8**

[Requirements](Requirements.md)  
[GPU support](GPU_support.md)  
[Modificaitons](Modifications.md)  
[Creating Python package](PackageCreation.md)    
[Install GPU supported tensorflow on Ubuntu 20.04](Install_ubuntu.md)  

## Components
- install_prerequisites.py:  
    - upgrades pip and setuptools
    - optionally uninstalls installed packages
    - installs prerequisites (required packages)
- main.py: starting point of execution
- DataFilter
    - Kalman1D
- Graph
    - PixelGraph
    - Vectorizer
    - (Vertex)
    - (Edge)
- Image
    - Comparer: compare images by different methods
    - Converter
    - EdgeDetection
    - Filter
    - ImageLoader
    - Moments
    - Out: Show/save images in selected format
    - PixelIndex
    - Segment
    - Thinner
- MachineLearning
    - DataSource: Data source for machine learning model creation
    - KMeans
    - TimeSeriesDataSource: Data source for time series analysis
    + NeuralNets
        + Layers
            - Layer
            - InputLayer
            - ConvolutionalLayer
            - MaxPoolingLayer
            - FlatteningLayer
            - DenseLayer
            - RandomDropoutLayer
        - NeuralNetwork (Abstract base class)
        - AutoEncoder
        - DeepNeuralNetwork - with convolutional and dense layers
        - MultiLayerPerceptron
        
- Math
    - Vector
- PointCloud
    - PointCloud
- StereoVision
    - Stereogram (RandomDot)
    - StereoVision: wrapper around CV2 stereo vision tools (with BMTuner)
        - (BMTuner: Tuner of BlockMatcher)
- Tests
    - Graph
    - Image
    - MachineLearning
    - StereoVision
- Tools
    - Converter: data converter between selected representations
    - FileSystem: folder content management
