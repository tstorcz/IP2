import numpy


class PointCloud(object):

    @staticmethod
    def __split(u, v, points):
        # return points on left side of UV
        return [p for p in points if numpy.cross(p - u, v - u) < 0]

    @staticmethod
    def __extend(u, v, points):
        if not points:
            return []

        # find furthest point W, and split search to WV, UW
        w = min(points, key=lambda p: numpy.cross(p - u, v - u))
        p1, p2 = PointCloud.__split(w, v, points), PointCloud.__split(u, w, points)
        return PointCloud.__extend(w, v, p1) + [w] + PointCloud.__extend(u, w, p2)

    @staticmethod
    def convex_hull(points):
        # find two hull points, U, V, and split to left and right search
        u = min(points, key=lambda p: p[0])
        v = max(points, key=lambda p: p[0])
        left, right = PointCloud.__split(u, v, points), PointCloud.__split(v, u, points)

        # find convex hull on each side
        return [v] + PointCloud.__extend(u, v, left) + [u] + PointCloud.__extend(v, u, right) + [v]

    @staticmethod
    def sort_by_angle(points):
        center = numpy.sum(points, axis=0) / len(points)
        clock_angles = []
        for p in points:
            clock_angles.append(PointCloud.__angle_clock(center, p))

        return numpy.array([x for _, x in sorted(zip(clock_angles, points.tolist()))])

    @staticmethod
    def __angle_clock(p1, p2):
        ang1 = numpy.arctan2(*p1[::-1])
        ang2 = numpy.arctan2(*p2[::-1])
        return numpy.rad2deg((ang1 - ang2) % (2 * numpy.pi))
