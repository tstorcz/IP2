import tensorflow as tf

from MachineLearning.NeuralNets.SequentialNeuralNetworks.FeedForwardNeuralNets.DeepSequentialNeuralNetwork \
    import DeepSequentialNeuralNetwork


class MiteNet(DeepSequentialNeuralNetwork):

    def __init__(self, shape_of_image, convolutional_layer_parameters, dense_layer_parameters):
        super().__init__(shape_of_image, convolutional_layer_parameters, dense_layer_parameters)

        self.loss = tf.keras.losses.BinaryCrossentropy()
        self.metrics = [tf.keras.metrics.binary_accuracy, tf.keras.metrics.Precision(), tf.keras.metrics.Recall()]
