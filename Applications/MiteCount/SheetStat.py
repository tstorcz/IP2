class SheetStat(object):

    def __init__(self,
                 file_name_with_path=None, file_name=None,
                 paper_corners=None, paper_area=-1, paper_aspect_ratio=-1, label=None,
                 roi_count=None, mite_count=None,
                 operation_time=None,
                 log_line=None):
        if log_line is None:
            self.file_name_with_path = file_name_with_path
            self.file_name = file_name

            self.paper_corners = paper_corners
            self.paper_area = paper_area
            self.paper_aspect_ratio = paper_aspect_ratio
            self.label = label

            self.roi_count = 0 if roi_count is None else roi_count
            self.mite_count = 0 if mite_count is None else mite_count

            self.operation_time = 0 if operation_time is None else operation_time
        else:
            log_line_parts = log_line.split(";")
            self.file_name_with_path = log_line_parts[0]
            self.file_name = log_line_parts[1]

            self.paper_corners = log_line_parts[2]
            self.paper_area = log_line_parts[3]
            self.paper_aspect_ratio = log_line_parts[4]
            self.label = log_line_parts[5]

            self.mite_count = int(log_line_parts[6])

            self.operation_time = int(log_line_parts[7])

        self.has_corners = type(self.paper_corners) == list and len(self.paper_corners) == 4
        self.has_label = self.label != "N/A" and self.label != "xxx"

    def __str__(self):
        return "{0};{1};{2};{3:.0f};{4:.2f};{5};{6};{7:.2f}".format(
            self.file_name_with_path, self.file_name,
            self.paper_corners, self.paper_area, self.paper_aspect_ratio,
            self.label,
            self.mite_count,
            self.operation_time
        )
