import numpy as np

from Image.Converter import Converter
from Image.ImageData import ImageData
from Math.NonMaxSupression import NonMaxSuppression


class ExtractGroundTruth(object):

    def __init__(
            self,
            original_image, mask_image_rgb,
            part_width, part_height,
            mask_hue_center, mask_hue_range, mask_sat_threshold=200, mask_val_threshold=200
    ):
        self.__part_width = part_width
        self.__part_height = part_height
        self.__original_image = original_image

        image_hsv = Converter.convert_image_data_from_rgb(mask_image_rgb, 'HSV')
        image_hue = image_hsv[:, :, 0].astype(np.int)
        image_sat = image_hsv[:, :, 1]
        image_val = image_hsv[:, :, 2]

        image_hue = image_hue + 255 - mask_hue_center - mask_hue_range
        image_hue[image_hue < 0] += 255
        image_hue[image_hue > 255] -= 255

        image_hue[(image_sat < mask_sat_threshold) | (image_val < mask_val_threshold)] = 0
        image_hue[image_hue < (255 - mask_hue_range * 2)] = 0

        pois = NonMaxSuppression.non_max_suppression([image_hue], [self.__part_height // 2, self.__part_width // 2])

        self.pois = []
        for poi in pois[0]:
            self.pois.append(poi[1])

        self.cropper = ImageData(
                data=original_image,
                crop_size=(part_height, part_width)
            )

    def get_part_size(self):
        return self.__part_height, self.__part_width

    def get_image_part(self, center):
        return self.cropper.crop(center=center)

    def get_part_count(self):
        return len(self.pois)

    def get_part(self, index):
        return self.get_image_part(self.pois[index])
