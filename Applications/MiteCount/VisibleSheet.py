from Math.Rectangle import Rectangle


class VisibleSheet(object):
    def __init__(self, sheet_corners, frame: Rectangle):
        self.sheet_corners = sheet_corners
        self.frame = frame
        self.visible_edges = []
        self.__compute_visible_edges()

    # TODO: compute rectangle intersection using Rectangle.intersection
    def __compute_visible_edges(self):
        if not self.frame.includes_point(self.sheet_corners[0][0]):
            if not self.frame.includes_point(self.sheet_corners[1][0]):
                # both top out of frame
                a = 1
            else:
                # top left out of frame
                a = 1
        else:
            if not self.frame.includes_point(self.sheet_corners[1][0]):
                # top right out of frame
                a = 1

        if not self.frame.includes_point(self.sheet_corners[2][0]):
            if not self.frame.includes_point(self.sheet_corners[3][0]):
                # both bottom out of frame
                a = 1
            else:
                # bottom right out of frame
                a = 1
        else:
            if not self.frame.includes_point(self.sheet_corners[3][0]):
                # bottom left out of frame
                a = 1

        self.visible_edges = []

    def get_top_left(self):
        return self.visible_edges
