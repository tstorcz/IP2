from sqlite3 import OperationalError

from DataBase.SQLite import SQLite


class LogDatabase(object):

    __create_log_table = """CREATE TABLE IF NOT EXISTS %table_name% (
                                            id integer PRIMARY KEY,
                                            file_name_with_path text NOT NULL,
                                            file_name text NOT NULL,
                                            sheet_corners text,
                                            sheet_size integer,
                                            label text,
                                            mite_count integer,
                                            execution_time real
                                        );"""
    __create_index_command = '''CREATE UNIQUE INDEX %table_name%_paths ON %table_name%(file_name_with_path);'''
    __select_by_full_path_command = '''SELECT * FROM %table_name% WHERE file_name_with_path=?;'''
    __select_by_name_command = '''SELECT * FROM %table_name% WHERE file_name_with_path=?;'''
    __select_by_id_command = '''SELECT * FROM %table_name% WHERE id=?;'''
    __insert_command = '''INSERT INTO %table_name% 
                    (file_name_with_path, file_name, sheet_corners, sheet_size, label, mite_count, execution_time) 
                    VALUES (?, ?, ?, ?, ?, ?, ?);'''
    __insert_without_stats_command = '''INSERT INTO %table_name% (file_name_with_path, file_name) VALUES (?, ?);'''
    __update_command = '''UPDATE %table_name% SET
                            file_name=?, 
                            sheet_corners=?,
                            sheet_size=?,
                            label=?,
                            mite_count=?, 
                            execution_time=?
                            WHERE file_name_with_path=?;'''

    __update_by_id_command = '''UPDATE %table_name% SET
                                file_name_with_path=?,
                                file_name=?, 
                                sheet_corners=?,
                                sheet_size=?,
                                label=?,
                                roi_count=?, 
                                mite_count=?, 
                                execution_time=?,
                                rotation=?
                                WHERE id=?;'''

    __update_rotation_command = '''UPDATE %table_name% SET
                                rotation=?
                                WHERE id=?;'''

    __update_mites_command = '''UPDATE %table_name% SET
                                    mite_count=?
                                    WHERE id=?;'''

    __update_roi_mites_command = '''UPDATE %table_name% SET
                                        roi_count=?,
                                        mite_count=?
                                        WHERE id=?;'''

    def __init__(self, database_file_name, table_name=None):

        if table_name is None:
            table_name = "log_entry"

        self.__create_log_table = LogDatabase.__create_log_table.replace("%table_name%", table_name)
        self.__create_index_command = LogDatabase.__create_index_command.replace("%table_name%", table_name)
        self.__select_by_full_path_command = \
            LogDatabase.__select_by_full_path_command.replace("%table_name%", table_name)
        self.__select_by_name_command = LogDatabase.__select_by_name_command.replace("%table_name%", table_name)
        self.__select_by_id_command = LogDatabase.__select_by_id_command.replace("%table_name%", table_name)
        self.insert_command = LogDatabase.__insert_command.replace("%table_name%", table_name)
        self.insert_without_stats_command = LogDatabase.__insert_without_stats_command.replace(
            "%table_name%",
            table_name
        )
        self.update_command = LogDatabase.__update_command.replace("%table_name%", table_name)
        self.update_rotation_command = LogDatabase.__update_rotation_command.replace("%table_name%", table_name)
        self.update_mites_command = LogDatabase.__update_mites_command.replace("%table_name%", table_name)
        self.update_roi_mites_command = LogDatabase.__update_roi_mites_command.replace("%table_name%", table_name)
        self.update_by_id_command = LogDatabase.__update_by_id_command.replace("%table_name%", table_name)

        self.__database = SQLite(database_file_name)
        self.__database.open()
        self.__database.execute(self.__create_log_table)
        try:
            self.__database.execute(self.__create_index_command)
        except OperationalError:
            # Suppress exception
            # print("Index has been created earlier")
            pass
        self.__database.close()

    def active(self, new_state: bool):
        self.__database.is_active = new_state

    def open(self):
        self.__database.open()

    def commit(self):
        self.__database.commit()

    def close(self):
        self.__database.close()

    def insert(self, command, parameters):
        if command is None:
            command = self.insert_without_stats_command

        return self.__database.execute(command, parameters)

    def get_by_full_path(self, file_name_with_path):
        return self.__database.fetch_rows(self.__select_by_full_path_command, [file_name_with_path])

    def get_by_name(self, file_name):
        return self.__database.fetch_rows(self.__select_by_name_command, [file_name])

    def get_by_id(self, file_id):
        return self.__database.fetch_rows(self.__select_by_id_command, [file_id])

    def update(self, update_command=None, parameters=None):
        if update_command is None:
            update_command = self.__update_command

        if parameters is None:
            parameters = []

        self.__database.execute(update_command, parameters)
