import cv2
import math
import numpy as np

from skimage import morphology
from skimage.feature import match_template

from Image.Filter import Filter
from Image.Kernel import Kernel
from Math.Rectangle import Rectangle
from Math.Section2D import Section2D

from Applications.MiteCount.ImageLabel import ImageLabel
from Applications.MiteCount.SheetStat import SheetStat


class BackgroundSheet(object):
    PAPER_AREA = 13500000
    PAPER_ASPECT_RATIO = 3120.0 / 4320.0
    PAPER_MIN_SIDE_LENGTH = 350
    PAPER_SIDE_ANGLE_DIFF = 5
    PAPER_EDGE_CORRELATION_THRESHOLD = 0.8
    BLACK_FRAME = 10
    FRAME_X = 700
    FRAME_Y = 500
    MIN_LINE_LENGTH = 40
    MAX_LINE_GAP = 10
    MAX_SECTION_DISTANCE = 10
    MAX_PARALLEL_SECTION_ENDPOINT_DISTANCE = 200
    MAX_PERPENDICULAR_SECTION_ENDPOINT_DISTANCE = 15

    LABEL_IMAGE_PADDING_Y = 2
    LABEL_IMAGE_PADDING_X = 150

    def __init__(self,
                 image_file_name, image_gray,
                 edge_kernel,
                 paper_corners=None,
                 template_magnification=1.0,
                 image_preprocessed=False,
                 ocr_model_path=None
                 ):
        self.image_file_name = image_file_name

        self.edge_kernel = edge_kernel
        self.selected_lines = []
        self.label_image = None

        self.__template_magnification = template_magnification
        self.__paper_area = int(BackgroundSheet.PAPER_AREA * self.__template_magnification ** 2)
        self.__black_frame = int(BackgroundSheet.BLACK_FRAME * self.__template_magnification)
        self.__paper_min_side_length = int(BackgroundSheet.PAPER_MIN_SIDE_LENGTH * self.__template_magnification)
        self.__frame_x = int(BackgroundSheet.FRAME_X * self.__template_magnification)
        self.__frame_y = int(BackgroundSheet.FRAME_Y * self.__template_magnification)
        self.__min_line_length = int(BackgroundSheet.MIN_LINE_LENGTH * self.__template_magnification)
        self.__max_line_gap = int(BackgroundSheet.MAX_LINE_GAP * self.__template_magnification)
        self.__max_section_distance = BackgroundSheet.MAX_SECTION_DISTANCE * self.__template_magnification
        self.__max_parallel_section_endpoint_distance = \
            BackgroundSheet.MAX_PARALLEL_SECTION_ENDPOINT_DISTANCE * self.__template_magnification
        self.__max_perpendicular_section_endpoint_distance = \
            BackgroundSheet.MAX_PERPENDICULAR_SECTION_ENDPOINT_DISTANCE * self.__template_magnification

        if ocr_model_path is not None:
            ImageLabel.init_singleton_ocr_reader(ocr_model_path)

        if image_preprocessed:
            self.image_gray = image_gray
        else:
            self.image_gray = self.__preprocess_gray_image(image_gray)
        self.size_y, self.size_x = self.image_gray.shape

        if paper_corners is None:
            self.__get_sheet_corners()
        else:
            self.paper_corners = paper_corners

    def __get_sheet_corners(self):
        rotated_kernels = Kernel.rotate_kernel(self.edge_kernel, [0, 90, 180, 270])
        zero_mean_rotated_kernels = Kernel.zero_mean_kernels(rotated_kernels)
        correlations = []
        self.selected_lines = []
        correlations.append(
            match_template(self.image_gray[:, -self.__frame_x:], zero_mean_rotated_kernels[0],
                           pad_input=True)
        )
        correlations[-1][correlations[-1] < BackgroundSheet.PAPER_EDGE_CORRELATION_THRESHOLD] = 0
        correlations.append(
            match_template(self.image_gray[:self.__frame_y, :], zero_mean_rotated_kernels[1],
                           pad_input=True)
        )
        correlations[-1][correlations[-1] < BackgroundSheet.PAPER_EDGE_CORRELATION_THRESHOLD] = 0
        correlations.append(
            match_template(self.image_gray[:, :self.__frame_x], zero_mean_rotated_kernels[2],
                           pad_input=True)
        )
        correlations[-1][correlations[-1] < BackgroundSheet.PAPER_EDGE_CORRELATION_THRESHOLD] = 0
        correlations.append(
            match_template(self.image_gray[-self.__frame_y:, :], zero_mean_rotated_kernels[3],
                           pad_input=True)
        )
        correlations[-1][correlations[-1] < BackgroundSheet.PAPER_EDGE_CORRELATION_THRESHOLD] = 0

        area_shifts = [
            [0, self.size_x - self.__frame_x],
            [0, 0],
            [0, 0],
            [self.size_y - self.__frame_y, 0]
        ]

        for src_idx in range(0, len(correlations)):
            correlation = correlations[src_idx].copy()
            correlation[correlation > 0.7] = 1
            correlation = morphology.skeletonize(correlation)

            correlation = correlation * 255

            lines = cv2.HoughLinesP(correlation.astype(np.uint8),
                                    rho=1, theta=1 * np.pi / 180, threshold=20,
                                    minLineLength=self.__min_line_length,
                                    maxLineGap=self.__max_line_gap)

            if src_idx % 2 == 0:
                self.selected_lines.append(Section2D.get_vertical_sections(lines, 10))
            else:
                self.selected_lines.append(Section2D.get_horizontal_sections(lines, 10))

            # ImageOut.show_image_with_data(
            #     correlation,
            #     sections=self.selected_lines[src_idx],
            #     show_section_center=True
            # )

            Section2D.connect_sections(
                self.selected_lines[src_idx], 1 - src_idx % 2,
                max_section_distance=self.__max_section_distance,
                max_parallel_endpoint_distance=self.__max_parallel_section_endpoint_distance,
                max_perpendicular_endpoint_distance=self.__max_perpendicular_section_endpoint_distance
            )

            # ImageOut.show_image_with_data(
            #     correlation,
            #     sections=self.selected_lines[src_idx],
            #     show_section_center=True
            # )

            self.selected_lines[src_idx] = [
                elem for elem in self.selected_lines[src_idx] if elem.length > self.__paper_min_side_length
            ]

            for line in self.selected_lines[src_idx]:
                line.shift(area_shifts[src_idx])

            if src_idx % 2 == 0:
                self.selected_lines[src_idx] = [elem for elem in self.selected_lines[src_idx] if
                                                elem.center[1] < self.size_x - self.__black_frame]
            else:
                self.selected_lines[src_idx] = [elem for elem in self.selected_lines[src_idx] if
                                                elem.center[0] < self.size_y - self.__black_frame]

        # self.__paper_corner_selection_by_area_and_ratio()
        self.__paper_corner_selection_by_distance_to_center()
        self.paper_corners = [[int(y), int(x)] for [y, x] in self.paper_corners]

    def __paper_corner_selection_by_distance_to_center(self):
        self.paper_corners = []

        if len(min(self.selected_lines, key=lambda x: len(x))) > 0:
            for v1 in sorted(self.selected_lines[0], key=lambda x: x.center[1]):
                for h1 in sorted(self.selected_lines[1], key=lambda x: x.center[0], reverse=True):
                    for v2 in sorted(self.selected_lines[2], key=lambda x: x.center[1], reverse=True):
                        for h2 in sorted(self.selected_lines[3], key=lambda x: x.center[0]):
                            if abs(v1.angle_difference(h1) - 90) < BackgroundSheet.PAPER_SIDE_ANGLE_DIFF \
                                    and abs(h1.angle_difference(v2) - 90) < BackgroundSheet.PAPER_SIDE_ANGLE_DIFF \
                                    and abs(v2.angle_difference(h2) - 90) < BackgroundSheet.PAPER_SIDE_ANGLE_DIFF \
                                    and abs(h2.angle_difference(v1) - 90) < BackgroundSheet.PAPER_SIDE_ANGLE_DIFF:
                                p1 = Section2D.intersection_point_of_lines(v1, h1)
                                p2 = Section2D.intersection_point_of_lines(h1, v2)
                                p3 = Section2D.intersection_point_of_lines(v2, h2)
                                p4 = Section2D.intersection_point_of_lines(h2, v1)
                                self.paper_corners = [p2, p1, p4, p3]
                                return

    def __paper_corner_selection_by_area_and_ratio(self):
        self.paper_corners = []
        area_difference = -1
        image_area = Rectangle.create_by_size([0, 0], self.image_gray.shape)
        for v1 in self.selected_lines[0]:
            for h1 in self.selected_lines[1]:
                p1 = Section2D.intersection_point_of_lines(v1, h1)
                if image_area.includes_point(p1):
                    for v2 in self.selected_lines[2]:
                        p2 = Section2D.intersection_point_of_lines(h1, v2)
                        if image_area.includes_point(p2):
                            for h2 in self.selected_lines[3]:
                                p3 = Section2D.intersection_point_of_lines(v2, h2)
                                p4 = Section2D.intersection_point_of_lines(h2, v1)
                                if image_area.includes_point(p3) and image_area.includes_point(p4):
                                    rectangle = Rectangle([p2, p1, p4, p3])
                                    curr_area_diff = math.fabs(self.__paper_area - rectangle.area())
                                    if math.fabs(rectangle.aspect_ratio() - BackgroundSheet.PAPER_ASPECT_RATIO) < 0.02 \
                                            and (area_difference < 0 or area_difference > curr_area_diff):
                                        area_difference = curr_area_diff
                                        self.paper_corners = [p2, p1, p4, p3]

    def get_label_image(self, frame_kernels, number_kernels):
        self.label_image = None
        if len(self.paper_corners) == 4:
            right_edge = Section2D(self.paper_corners[1], self.paper_corners[2])
            top = int(self.paper_corners[2][0] - right_edge.length / 3) + BackgroundSheet.LABEL_IMAGE_PADDING_Y

            label_image = self.image_gray[
                           top:
                           int(self.paper_corners[2][0]) - BackgroundSheet.LABEL_IMAGE_PADDING_Y,
                           int(min(right_edge.center[1], self.paper_corners[2][1]))
                           - BackgroundSheet.LABEL_IMAGE_PADDING_X:
                           ]

            self.label_image = ImageLabel(
                BackgroundSheet.__image_requantize(label_image),
                frame_kernels, number_kernels, magnification=self.__template_magnification
            )

            if not self.label_image.has_label:
                left_edge = Section2D(self.paper_corners[0], self.paper_corners[3])

                label_image = np.rot90(
                    self.image_gray[
                              int(self.paper_corners[0][0]) + BackgroundSheet.LABEL_IMAGE_PADDING_Y:
                              int(left_edge.center[0]) - BackgroundSheet.LABEL_IMAGE_PADDING_Y,
                              :int(max(left_edge.center[1],
                                       self.paper_corners[0][1]))
                              + BackgroundSheet.LABEL_IMAGE_PADDING_X
                              ],
                    2
                )

                other_label_image = ImageLabel(
                    BackgroundSheet.__image_requantize(label_image),
                    frame_kernels, number_kernels, magnification=self.__template_magnification
                )

                if other_label_image.has_label:
                    self.label_image = other_label_image

        return self.label_image

    def get_stats(self):
        if len(self.paper_corners) == 4:
            rect = Rectangle(self.paper_corners)
        else:
            rect = None

        return SheetStat(file_name_with_path=self.image_file_name,
                         paper_corners=self.paper_corners,
                         paper_area=rect.area if rect is not None else -1,
                         paper_aspect_ratio=rect.aspect_ratio() if rect is not None else -1,
                         label="N/A" if self.label_image is None else self.label_image.label
                         )

    def get_paper_rect(self, magnification=1):
        if len(self.paper_corners) == 4:
            return Rectangle(self.paper_corners, magnification=magnification)

        return None

    def __preprocess_gray_image(self, image):
        image = Filter.grid_rescale_image(image, 3,
                                          [
                                            (image.shape[0] / self.__frame_y) - 1,
                                            (image.shape[1] / self.__frame_x) - 1
                                          ])
        image[image < 0] = 0
        image[image > 255] = 255
        image = BackgroundSheet.__image_requantize(image, image.max())
        return image

    @staticmethod
    def __image_requantize(image, value_max=None, value_min=None):
        if value_max is None:
            value_max = np.max(image)
        if value_min is None:
            value_min = np.min(image)
        value_interval = value_max - value_min
        d = 255.0 / value_interval
        return (image - value_min) * d
