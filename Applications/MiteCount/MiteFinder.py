import os.path

import numpy as np
import PIL.ImageDraw as ImageDraw

from skimage.feature import match_template

from Image.Converter import Converter
from Image.ImageData import ImageData
from Image.ImageLoader import ImageLoader
from Image.ImageOut import ImageOut
from Math.NonMaxSupression import NonMaxSuppression

from Tools.Files.FileSystem import FileSystem


class MiteFinder(object):

    def __init__(self,
                 image_data: np.ndarray,
                 mite_kernels,
                 mite_correlations_threshold=0.8,
                 rectangle_mask=None,
                 paper_rectangle=None,
                 crop_size=None,
                 mite_identifier=None
                 ):
        self.image_data = None
        self.rectangle_mask = rectangle_mask
        self.mite_kernels = mite_kernels
        self.mite_kernel_size = mite_kernels[0].shape
        self.mite_kernel_radius = [*map(lambda x: int(x/2), mite_kernels[0].shape)]
        self.roi_locations = []
        self.roi_location_coefficients = []
        self.mite_indices = []
        self.paper_rectangle = paper_rectangle
        self.__mite_correlations_threshold = mite_correlations_threshold
        self.mite_identifier = mite_identifier

        if crop_size is None:
            self.crop_size = self.mite_kernel_size
        else:
            self.crop_size = crop_size

        self.set_image(image_data)

    def __create_rectangle_mask(self):
        mask_image = ImageOut.create_image_from_data(np.zeros(self.image_data.shape), 'L')
        draw = ImageDraw.Draw(mask_image)
        draw.polygon(
            (
                (self.rectangle_mask.top_left[1], self.rectangle_mask.top_left[0]),
                (self.rectangle_mask.top_right[1], self.rectangle_mask.top_right[0]),
                (self.rectangle_mask.bottom_right[1], self.rectangle_mask.bottom_right[0]),
                (self.rectangle_mask.bottom_left[1], self.rectangle_mask.bottom_left[0])
            ),
            fill=1, outline=0
        )
        self.rectangle_mask = ImageLoader.get_image_pixels(mask_image)

    def __roi_identification(self, source_image, threshold):
        gray_scale = Converter.convert_image_data_from_rgb(source_image, 'gray')

        # feature_image contains maximum of all correlations in each points
        feature_image = np.zeros(gray_scale.shape)
        for kernel_index in range(0, len(self.mite_kernels)):
            correlation = match_template(
                gray_scale,
                self.mite_kernels[kernel_index],
                pad_input=True
            )
            feature_image = np.maximum(feature_image, correlation)

        # clear image frame
        feature_image[0:self.mite_kernel_radius[0], :] = 0
        feature_image[-self.mite_kernel_radius[0]:, :] = 0
        feature_image[:, 0:self.mite_kernel_radius[1]] = 0
        feature_image[:, -self.mite_kernel_radius[1]:] = 0

        # suppress low intensity items
        feature_image[feature_image < threshold] = 0

        # suppress non-maxes
        pois = NonMaxSuppression.non_max_suppression(
            [feature_image],
            [self.mite_kernel_size[0] // 2, self.mite_kernel_size[1] // 2]
        )

        self.roi_locations = []
        self.roi_location_coefficients = []
        self.mite_indices = []

        for poi in pois[0]:
            if self.paper_rectangle is None or self.paper_rectangle.includes_point(poi[1]):
                self.roi_locations.append(poi[1])
                self.roi_location_coefficients.append(poi[2])

    def __mite_identification(self):
        if self.mite_identifier is not None:
            idx = 0
            while idx < len(self.roi_locations):
                suspected = self.crop(self.roi_locations[idx])

                if self.mite_identifier.is_mite(suspected):
                    self.mite_indices.append(idx)

    def set_image(self, image_data):
        self.image_data = image_data

        if self.rectangle_mask is not None and self.image_data is not None:
            self.__create_rectangle_mask()
            source_image = np.multiply(self.image_data, self.rectangle_mask)
        else:
            source_image = self.image_data

        if source_image is not None:
            self.__roi_identification(source_image, self.__mite_correlations_threshold)

            if self.mite_identifier is not None:
                self.__mite_identification()

    def _save_roi_images(self, target_folder, image_id, file_prefix, roi_indices=None):
        path = FileSystem.create_folder(target_folder, "image_{0:05d}".format(image_id))

        if roi_indices is None:
            roi_indices = range(0, len(self.roi_locations))

        for i in roi_indices:
            ImageOut.save_image(
                ImageData.crop_image(self.image_data, center=self.roi_locations[i], size=self.crop_size),
                os.path.join(
                    path,
                    '{0}_{1:05d}_{2:04d}_y{3:04.0f}_x{4:04.0f}_c{5:03.0f}_{6}.jpg'.format(
                        file_prefix, image_id, i,
                        self.roi_locations[i][0], self.roi_locations[i][1],
                        self.roi_location_coefficients[i] * 1000,
                        "p" if i in self.mite_indices else "n"
                    )
                ),
                mode="RGB"
            )

    def save_roi_images(self, target_folder, image_id):
        self._save_roi_images(target_folder, image_id, "roi", range(0, len(self.roi_locations)))

    def save_mite_images(self, target_folder, image_id):
        self._save_roi_images(target_folder, image_id, "mite", self.mite_indices)

    def crop(self, center):
        return ImageData.crop_image(self.image_data, size=self.crop_size, center=center)

    def roi_count(self):
        return len(self.roi_locations)

    def mite_count(self):
        return len(self.mite_indices)