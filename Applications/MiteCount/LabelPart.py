from Math.Rectangle import Rectangle


class LabelPart(object):
    PROBABILITY_THRESHOLD = 0.6

    SIZE_X_MIN = 21
    SIZE_Y_MIN = 35
    SIZE_X_MAX = 68
    SIZE_Y_MAX = 65
    SIZE_X_CROP = 75
    SIZE_Y_CROP = 80
    SHIFT_Y = 55

    def __init__(self, descriptor):
        self.rectangle = Rectangle(
            descriptor[0]
        )

        self.size = [
            self.rectangle.bottom_right[0] - self.rectangle.top_left[0],
            self.rectangle.bottom_right[1] - self.rectangle.top_left[1]
        ]

        self.area = self.size[0] * self.size[0]

        self.label = descriptor[1]
        self.probability = descriptor[2]

        self.is_valid = self.probability >= LabelPart.PROBABILITY_THRESHOLD

    def is_valid(self):
        return self.probability >= LabelPart.PROBABILITY_THRESHOLD \
               and LabelPart.SIZE_X_MIN <= self.size[0] <= LabelPart.SIZE_X_MAX \
               and LabelPart.SIZE_Y_MIN <= self.size[1] <= LabelPart.SIZE_Y_MAX

    def get_ocr_label_rectangle(self):
        return self.rectangle.create_extend([LabelPart.SIZE_X_CROP, LabelPart.SIZE_Y_CROP * 4])

    def get_ocr_rectangle(self):
        return self.rectangle.create_extend([LabelPart.SIZE_X_CROP, LabelPart.SIZE_Y_CROP])

    @staticmethod
    def create_ocr_rectangle(center):
        return Rectangle.create_by_size(
            [center[0] - LabelPart.SIZE_X_CROP / 2, center[1] - LabelPart.SIZE_Y_CROP / 2],
            [LabelPart.SIZE_X_CROP, LabelPart.SIZE_Y_CROP]
        )

    @staticmethod
    def shift_box(box, x_shift=None, y_shift=None):
        new_box = box.copy()
        for coord in new_box:
            if x_shift is not None:
                coord[0] += x_shift

            if y_shift is not None:
                coord[1] += y_shift

        return new_box
