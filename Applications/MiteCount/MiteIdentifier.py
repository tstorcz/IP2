import tensorflow as tf

from Applications.MiteCount.MiteDSNN import MiteDSNet


class MiteIdentifier:

    def __init__(self, cnn_state_path):
        self.model = MiteDSNet(
            (27, 27, 1),
            None, None, [[1, tf.keras.activations.sigmoid]],
            use_batch_norm=True
        )
        self.model.load(cnn_state_path)

    def is_mite(self, image):
        return self.model.predict(image) >= 0.5
