import tensorflow as tf

from MachineLearning.NeuralNets.SequentialNeuralNetworks.FeedForwardNeuralNets.DeepSpatialNeuralNetwork \
    import DeepSpatialNeuralNetwork


class MiteDSNet(DeepSpatialNeuralNetwork):

    def __init__(self, shape_of_image, conv3d_layer_parameters, conv2d_layer_parameters, dense_layer_parameters,
                 use_batch_norm=False):
        super().__init__(
            shape_of_image,
            conv3d_layer_parameters,
            conv2d_layer_parameters,
            dense_layer_parameters,
            use_batch_norm=use_batch_norm
        )

        self.loss = tf.keras.losses.BinaryCrossentropy()
        self.metrics = [tf.keras.metrics.binary_accuracy, tf.keras.metrics.Precision(), tf.keras.metrics.Recall()]
