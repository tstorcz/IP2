import bisect

from os import path

from LogDatabase import LogDatabase
from SheetStat import SheetStat


class LogCollection(object):

    def __init__(self, log_file_name, sort_lambda, key_exporter_method):
        self.log_file_name = log_file_name
        self.log_entries = []
        self.key_list = []

        self.__read_log(sort_lambda, key_exporter_method)

    def __read_log(self, sort_lambda, key_exporter_method):
        self.log_entries = []
        self.key_list = []
        if path.exists(self.log_file_name):
            file = open(self.log_file_name, "r")
            for line in file:
                ss = SheetStat(log_line=line)
                self.log_entries.append(ss)
            file.close()
            self.log_entries.sort(key=sort_lambda)
            self.key_list = key_exporter_method(self.log_entries)

    def get_log_index(self, lookup_key):
        idx = bisect.bisect_left(self.key_list, lookup_key)
        if idx != len(self.key_list) and self.key_list[idx] == lookup_key:
            return idx
        return -1

    def load_logs_into_database(self,
                                log_database: LogDatabase
                                ):
        if log_database is None:
            raise ValueError

        ec = 1
        log_database.open()
        for entry in self.log_entries:
            print(ec)
            ec = ec + 1
            found = log_database.get_by_full_path(entry.file_name_with_path)
            if len(found) == 0:
                log_database.insert(
                    [
                        entry.file_name_with_path, entry.file_name,
                        entry.paper_corners, entry.label,
                        entry.roi_count, entry.operation_time
                    ]
                )
            else:
                log_database.update(
                    [
                        entry.file_name,
                        entry.paper_corners, entry.label,
                        entry.roi_count, entry.operation_time,
                        entry.file_name_with_path
                    ]
                )
        log_database.close()
