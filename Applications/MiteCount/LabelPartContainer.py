import math

from Math.Rectangle import Rectangle

from Applications.MiteCount.LabelPart import LabelPart


class LabelPartContainer(object):

    BORDER = 5

    def __init__(self, descriptor_list):
        self.parts = []
        self.part_height = -1
        for descriptor in descriptor_list:
            new_part = LabelPart(descriptor)

            self.parts.append(new_part)
            if self.part_height < 0 or self.part_height > self.parts[-1].area:
                self.part_height = self.parts[-1].rectangle.left.length

        self.parts.sort(key=lambda x: x.rectangle.top_left[1], reverse=False)

        self.text = ""
        self.numbers = []

        for part in self.parts:
            self.text += part.label

    def get_bounding_box(self):
        box = self.parts[0].rectangle

        for label in self.parts:
            box = box.bounding_box(label.rectangle)

        extension = math.ceil(((self.part_height * 3) - box.left.length) / self.part_height) * self.part_height

        box = Rectangle(
            [
                [box.top_left[0], max(box.top_left[1] - extension - LabelPartContainer.BORDER, 0)],
                [box.top_right[0], max(box.top_right[1] - extension - LabelPartContainer.BORDER, 0)],
                [box.bottom_right[0], box.bottom_right[1] + extension + LabelPartContainer.BORDER],
                [box.bottom_left[0], box.bottom_left[1] + extension + LabelPartContainer.BORDER]
            ]
        )

        return box
