import easyocr
import numpy as np

from skimage.feature import match_template

from Math.NonMaxSupression import NonMaxSuppression
from Math.Section2D import Section2D
from Math.Rectangle import Rectangle

from Applications.MiteCount.LabelPart import LabelPart
from Applications.MiteCount.LabelPartContainer import LabelPartContainer


class ImageLabel(object):
    LABEL_IMAGE_WIDTH_RATIO = 4.5
    LABEL_IMAGE_HEIGHT_RATIO = 3.5

    NUMBER_PADDING = 30
    NUMBER_FRAME_SIZE = [346, 90]
    NUMBER_FRAME_SIZE_RANGE = [60, 16]
    NUMBER_FRAME_ANGLE_RANGE = 5
    CORRELATION_THRESHOLD = 0.5

    __ocr_reader = None

    @staticmethod
    def init_singleton_ocr_reader(model_path):
        if ImageLabel.__ocr_reader is None:
            ImageLabel.__ocr_reader = easyocr.Reader(
                ['en'],
                model_storage_directory="" if model_path is None else model_path,
                download_enabled=model_path is None,
                gpu=True
            )

    @staticmethod
    def init_ocr_reader(model_path):
        __ocr_reader = easyocr.Reader(
            ['en'],
            model_storage_directory=model_path,
            download_enabled=False,
            gpu=True
        )

    def __init__(self, image, frame_kernels, number_kernels, label=None, magnification=1.0):
        self.image = image if isinstance(image, np.ndarray) else np.array(image)

        self.frame_correlations = []
        self.number_correlations = []
        self.number_pois = []
        self.left_edge = None
        self.numbers = []
        self.label = "N/A"
        self.accuracy = 0
        self.has_label = False
        self.has_full_label = False
        self.number_frame_size = [int(i * magnification) for i in ImageLabel.NUMBER_FRAME_SIZE]
        self.number_frame_size_range = [int(i * magnification) for i in ImageLabel.NUMBER_FRAME_SIZE_RANGE]

        self.nms_frame_corner_range = (
            int(frame_kernels[0].shape[0] / 2),
            int(frame_kernels[0].shape[1] / 2)
        )
        self.nms_rect_range = (
            int((number_kernels[0].shape[0] + ImageLabel.NUMBER_PADDING * magnification) / 2),
            int(number_kernels[0].shape[1] / 2)
        )

        if label is None:
            label_parts = LabelPartContainer(
                ImageLabel.__ocr_reader.readtext(self.image.astype(np.uint8))
            )

            if len(label_parts.parts) > 0:
                base_rect = label_parts.parts[0].get_ocr_label_rectangle()
                image_part = ImageLabel.__crop_image(self.image, base_rect)
                base_pois = self.__get_number_pois(number_kernels, image=image_part)

                if len(base_pois) > 0:
                    base_rect = LabelPart.create_ocr_rectangle(
                        [base_pois[0][1][1], base_pois[0][1][0]]
                    ).create_shifted(
                        base_rect.top_left[0], base_rect.top_left[1]
                    )

                    for i in range(0, 7):
                        y_shift = (-2 + i) * LabelPart.SHIFT_Y
                        if base_rect.top_left[1] + y_shift >= 0 \
                                and base_rect.bottom_left[1] + y_shift < self.image.shape[0]:
                            src_rect = base_rect.create_shifted(shift_y=y_shift)
                            ocr_image = ImageLabel.__crop_image(self.image, src_rect)

                            ocr_result = ImageLabel.__ocr_reader.readtext(ocr_image.astype(np.uint8))
                            number_pois = self.__get_number_pois(number_kernels, image=ocr_image)

                            self.numbers.append(ImageLabel.__select_single_number(ocr_result, number_pois))

            self.__remove_invalid_numbers(-2)

            valid = 0
            for n in self.numbers:
                if n >= 0:
                    valid += 1

            if valid >= 3:
                self.__remove_invalid_numbers(-1)

            self.__validate_numbers()

            print(self.numbers)

        else:
            self.label = label
            self.has_label = True
            self.has_full_label = True

    def __segment(self, frame_kernels, number_kernels):
        self.numbers = [-1, -1, -1]

        self.__get_number_frame(frame_kernels)
        self.number_correlations = self.__get_number_pois(number_kernels)
        self.number_pois = NonMaxSuppression.non_max_suppression(
            self.number_correlations, self.nms_rect_range, combined=True
        )
        self.__select_numbers()

    def __get_number_frame(self, frame_kernels):
        self.label_frame_center = None

        img = self.image - 128
        self.frame_correlations = self.__get_number_pois(frame_kernels, image=img, threshold=0.65)

        frame_corner_pois = NonMaxSuppression.non_max_suppression(
            self.frame_correlations,
            self.nms_frame_corner_range
        )

        sides = []
        for idx in range(0, len(self.frame_correlations)):
            next_idx = idx + 1 if idx + 1 < len(self.frame_correlations) else idx + 1 - len(self.frame_correlations)
            sides.append(self.__get_frame_sides(frame_corner_pois[idx], frame_corner_pois[next_idx], idx % 2))

        centers = []
        for idx in range(0, len(sides)):
            for s in sides[idx]:
                sr = s.rotate(90, s.center)
                sr.set_length(self.number_frame_size[(idx + 1) % 2] / 2)
                centers.append(sr.p2)

        votes = []
        for c1 in centers:
            vote_count = 0
            for c2 in centers:
                if Section2D.distance_of_points(c1, c2) < self.number_frame_size[1] / 2:
                    vote_count = vote_count + 1
            votes.append((c1, vote_count))

        votes.sort(key=lambda tup: tup[1], reverse=True)

        if len(votes) > 0:
            self.label_frame_center = votes[0][0]

    def __get_number_pois(self, kernels, image=None, threshold=None):
        correlations = []

        if image is None:
            image = self.image

        if threshold is None:
            threshold = ImageLabel.CORRELATION_THRESHOLD

        for kernel in kernels:
            kernel_correlation = match_template(image, kernel, pad_input=True)

            kernel_correlation[kernel_correlation < threshold] = 0

            correlations.append(kernel_correlation)

        number_pois = NonMaxSuppression.non_max_suppression(
            correlations,
            self.nms_rect_range, combined=True
        )

        number_pois = list(filter(lambda x: x[0] < 10, number_pois))
        # number_pois = list(filter(lambda x: abs(x[1][1] - (image.shape[1] / 2)) < (image.shape[1] / 5), number_pois))

        number_pois.sort(key=lambda x: x[1][0])

        return number_pois

    def __get_frame_sides(self, point_list_1, point_list_2, dimension_index):
        side_sections = []
        for p1 in point_list_1:
            for p2 in point_list_2:
                s1 = Section2D(p1[1], p2[1], sort=False)
                if abs(s1.length - self.number_frame_size[dimension_index]) < \
                        self.number_frame_size_range[dimension_index] \
                        and \
                        (
                            (
                                    (abs(s1.angle - 90) < ImageLabel.NUMBER_FRAME_ANGLE_RANGE
                                     or abs(s1.angle - 270) < ImageLabel.NUMBER_FRAME_ANGLE_RANGE)
                                    and dimension_index == 0
                            )
                            or
                            (
                                    (abs(s1.angle) < ImageLabel.NUMBER_FRAME_ANGLE_RANGE
                                     or abs(s1.angle - 180) < ImageLabel.NUMBER_FRAME_ANGLE_RANGE
                                     or abs(s1.angle - 360) < ImageLabel.NUMBER_FRAME_ANGLE_RANGE)
                                    and dimension_index == 1
                            )
                        ):
                    side_sections.append(s1)

        return side_sections

    @staticmethod
    def __select_single_number(ocr_result, correlation_result):
        ocr_num = -2
        corr_num = -2
        if len(ocr_result) > 0:
            ocr_result.sort(key=lambda x: x[2])
            if ocr_result[0][2] > LabelPart.PROBABILITY_THRESHOLD and ocr_result[0][1].isnumeric():
                ocr_num = int(ocr_result[0][1])
            else:
                ocr_num = -1

        if len(correlation_result) > 0:
            if correlation_result[0][2] > LabelPart.PROBABILITY_THRESHOLD:
                corr_num = correlation_result[0][0]
            else:
                corr_num = -1

        if ocr_num >= 0 and corr_num >= 0:
            return corr_num

        return max(ocr_num, corr_num)

    def __select_numbers(self):
        self.numbers = [-1, -1, -1]

        if self.label_frame_center is None:
            return

        # filter numbers (drop vertical lines)
        numbers = [t for t in self.number_pois if t[0] < 10]
        # filter numbers (drop out of frame)
        numbers = \
            [t for t in numbers
             if abs(self.label_frame_center[0] - t[1][0]) <= self.number_frame_size[0] / 2
             and abs(self.label_frame_center[1] - t[1][1]) <= self.number_frame_size[1] / 4
             ]

        # sort by Y coordinate
        numbers.sort(key=lambda tup: tup[1][0])

        top = self.label_frame_center[0] - self.number_frame_size[0] / 2
        for i in range(0, min(len(numbers), 3)):
            num_idx = int((numbers[i][1][0] - top) / (self.nms_rect_range[0] * 2))
            self.numbers[num_idx] = numbers[i][0]

        self.label = "{0}{1}{2}".format(
            str(self.numbers[0]) if self.numbers[0] >= 0 else "x",
            str(self.numbers[1]) if self.numbers[1] >= 0 else "x",
            str(self.numbers[2]) if self.numbers[2] >= 0 else "x"
        )
        self.has_label = max(self.numbers) >= 0
        self.has_full_label = min(self.numbers) >= 0

    def __select_numbers_on_image(self):
        self.numbers = [-1, -1, -1]

        numbers = [t for t in self.number_pois if t[0] < 10]
        numbers.sort(key=lambda tup: tup[1][0])

        for i in range(0, min(len(numbers), 3)):
            self.numbers[i] = numbers[i][0]

    def __remove_invalid_numbers(self, value):
        while len(self.numbers) > 3 and self.numbers[0] == value:
            del (self.numbers[0])

        while len(self.numbers) > 3 and self.numbers[-1] == value:
            del (self.numbers[-1])

    def __validate_numbers(self):
        while len(self.numbers) < 3:
            self.numbers.append(-1)

        self.label = ""
        self.accuracy = 0

        for n in self.numbers:
            self.label += str(n) if n >= 0 else "x" if n == -1 else "."
            self.accuracy += 10 if n >= 0 else 3 if n == -1 else 0

        self.has_label = max(self.numbers) >= 0
        self.has_full_label = min(self.numbers) >= 0 and len(self.numbers) == 3

    @staticmethod
    def __crop_image(image, rectangle: Rectangle):
        return image[
               int(rectangle.top_left[1]): int(rectangle.bottom_left[1]),
               int(rectangle.top_left[0]): int(rectangle.top_right[0])
               ]
