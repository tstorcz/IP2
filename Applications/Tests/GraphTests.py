from matplotlib import pyplot as plt
from skimage.morphology import skeletonize

from Graph.PixelGraph import PixelGraph
from Image.Thinner import Thinner


class GraphTests(object):

    @staticmethod
    def graph_test(binary_image, show_image_outputs=False):
        skeleton = skeletonize(binary_image)
        it = Thinner()
        skeleton1 = it.multi_scale_thinning(skeleton)
        pg = PixelGraph(skeleton1)
        descriptor = pg.depth_first_descriptor(pg.first_vertex())

        if show_image_outputs:
            print("Vertex count: {0}, Descriptor length: {1}".format(pg.vertex_count(), len(descriptor)))
            print("Descriptor: {0}".format(descriptor))

            fig, axes = plt.subplots(1, 2)

            ax = axes.ravel()

            ax[0].imshow(binary_image)
            ax[0].set_title('original')
            ax[0].axis('off')

            ax[1].imshow(skeleton1)
            ax[1].set_title('skeleton')
            ax[1].axis('off')

            font_dict_dark = {
                'family': 'serif',
                'color': 'darkred',
                'size': 6}

            font_dict_light = {
                'family': 'serif',
                'color': 'white',
                'size': 6}

            for _, vertex in pg.Vertices.items():
                for edge in vertex.Edges:
                    if not edge.IsReversed:
                        pxs = [p[0] for p in edge.Points]
                        pys = [p[1] for p in edge.Points]
                        ax[0].plot(pys, pxs)
                        ax[0].text(
                            (edge.Points[0][1] + edge.Points[-1][1]) / 2,
                            (edge.Points[0][0] + edge.Points[-1][0]) / 2,
                            "{0:.2f}".format(edge.length()),
                            fontdict=font_dict_dark)
                        ax[1].text(
                            (edge.Points[0][1] + edge.Points[-1][1]) / 2,
                            (edge.Points[0][0] + edge.Points[-1][0]) / 2,
                            "{0:.2f}".format(edge.length()),
                            fontdict=font_dict_light)

            fig.tight_layout()
            plt.show()

        return descriptor