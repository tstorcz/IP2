import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import os

from sklearn import datasets
from skimage.feature import hog
from skimage import data, exposure

from MachineLearning.KMeans import KMeans
from MachineLearning.DataSources.DataSource import DataSource
from MachineLearning.DataSources.IndexManagers.BreadthFirstBatchIndexManager import BreadthFirstBatchIndexManager
from MachineLearning.DataSources.StratifiedDataSource import StratifiedDataSource
from MachineLearning.DataSources.TimeSeriesDataSource import TimeSeriesDataSource
from MachineLearning.NeuralNets.AutoEncoders.AutoEncoder import AutoEncoder
from MachineLearning.NeuralNets.SequentialNeuralNetworks.FeedForwardNeuralNets.DeepSequentialNeuralNetwork import DeepSequentialNeuralNetwork
from MachineLearning.NeuralNets.SequentialNeuralNetworks.FeedForwardNeuralNets.MultiLayerPerceptron import MultiLayerPerceptron
from MachineLearning.NeuralNets.SequentialNeuralNetworks.RecurrentNeuralNets.RecurrentNeuralNetwork import RecurrentSequentialNeuralNetwork
from MachineLearning.NeuralNets.SequentialNeuralNetworks.SVMs.LinearSVM import LinearSVM
from Tools.Measure.StopWatch import StopWatch
from Visualization.Visualizer import Visualizer


class MachineLearningTests(object):

    @staticmethod
    def k_means_test():
        X = np.array([[1, 2], [1, 4], [1, 0], [4, 2], [4, 4], [4, 0]])
        print(X)

        kmeans = KMeans(2, 100)
        kmeans.centroids = [[0, 2], [4, 2]]
        kmeans.fit(X)
        print(kmeans.centroids)
        print(kmeans.estimate(X))
        print(kmeans.estimate([[0, 0], [4, 4]]))

    @staticmethod
    def perceptron_test():
        labeled_data = np.array(
            [
                [0, 0, -1],
                [0, 1, 1],
                [1, 0, 1],
                [1, 1, 1]
            ]
        )

        X = labeled_data[:, 0:2]
        y = labeled_data[:, 2:]

        epochs = 5000

        model = MultiLayerPerceptron(
            [
                2,
                [
                    [1, tf.keras.activations.linear]
                ],
                [1, "binary"]
            ]
        )
        model.compile(learning_rate=0.05)
        model.train(X, y, epochs=epochs, verbose=False)

        Visualizer.visualize_2d(X, y, model)

    @staticmethod
    def perceptron_test_2(labeled_data):
        perceptron = MultiLayerPerceptron(
            [
                9,
                [
                    [144, tf.keras.activations.relu],
                    [36, tf.keras.activations.relu],
                    [36, tf.keras.activations.relu]
                ],
                [2, 'binary']
            ]
        )
        perceptron.compile(learning_rate=0.05)
        x = labeled_data[:, 0:9]
        y = labeled_data[:, 9:]
        loss = 0
        for s in range(0, 50000):
            loss = perceptron.training_step(x, y)
            if s % 1000 == 0:
                print(s, loss)
        print("final loss: ", loss)

    @staticmethod
    def deep_conv_net_test():
        image_shape = [28, 28, 1]
        output_count = 10

        # without path, data will be downloaded automatically all the time
        mnist_dataset = tf.keras.datasets.mnist
        (X_train, y_train), (X_test, y_test) = mnist_dataset.load_data(path=os.getcwd() + "\\..\\..\\DataManipulation\\mnist.nzp")
        mnist_data_source = DataSource(
            train=[
                X_train.reshape(-1, image_shape[0], image_shape[1], image_shape[2]),
                DataSource.one_hot_from_index(y_train, output_count)
            ],
            test=[
                X_test.reshape(-1, image_shape[0], image_shape[1], image_shape[2]),
                DataSource.one_hot_from_index(y_test, output_count)
            ],
            batch_size=50
        )

        batch_x, batch_y = mnist_data_source.get_current_training_batch()
        test_x, test_y = mnist_data_source.get_test_item(0)

        print("Training set")
        print(mnist_data_source.train_set[0].shape)
        print(mnist_data_source.train_set[1].shape)
        print("Training batch")
        print(batch_x.shape)
        print(batch_y.shape)
        print("Test set")
        print(mnist_data_source.test_set[0].shape)
        print(mnist_data_source.test_set[1].shape)
        print("Test item")
        print(test_x.shape)
        print(test_y.shape)

        mnist_sample_net = DeepSequentialNeuralNetwork(
            image_shape,
            [
                [5, 32, tf.keras.activations.relu, 2],
                [5, 64, tf.keras.activations.relu, 2]
            ],
            [
                [1024, tf.keras.activations.relu],
                [output_count, None],
                [output_count, tf.keras.activations.softmax]
            ]
        )

        training_steps = 10000
        learning_rate = 0.01
        dropout_rate = 0
        mnist_sample_net.compile(learning_rate, dropout_rate)

        for s in range(0, training_steps):
            x_batch, y_batch = mnist_data_source.get_random_training_batch()
            loss = mnist_sample_net.training_step(x_batch, y_batch)
            if s % 1000 == 0:
                print(s, loss)

    @staticmethod
    def rnn_test():
        number_of_time_steps = 30
        number_of_inputs = 1
        number_of_rnn_cells = 100
        number_of_outputs = 1
        learning_rate = 0.001
        number_of_iterations = 3000
        batch_count = 40

        data_source = TimeSeriesDataSource(
            0, 10, number_of_points=250,
            time_steps=number_of_time_steps,
            generator_function=np.sin
        )

        rnn = RecurrentSequentialNeuralNetwork(
            number_of_time_steps,
            [
                number_of_inputs,
                number_of_rnn_cells, tf.keras.activations.relu,
                number_of_outputs
            ]
        )
        rnn.compile(learning_rate=learning_rate)

        for i in range(0, number_of_iterations):
            y1, y2, ts = data_source.get_random_batches(batch_count, return_batch_x=True)
            loss = rnn.training_step(y1, y2[:, -number_of_outputs])
            if i % 100 == 0:
                print(i, loss)

        y1, y2, ts = data_source.get_random_batches(1, return_batch_x=True)
        prediction = rnn.predict(y1)
        plt.plot(data_source.x_data, data_source.y_true, label="sin(t)")
        plt.plot(ts.flatten()[:-1], y1.flatten(), "bo", markersize=3, alpha=0.5, label="training data")
        plt.plot(
            ts.flatten()[-number_of_outputs],
            y2.flatten()[-number_of_outputs],
            "ro", markersize=10, alpha=0.5, label="ground truth"
        )
        plt.plot(
            ts.flatten()[-number_of_outputs],
            prediction.flatten()[-number_of_outputs],
            "ko", markersize=5, label="prediction"
        )
        plt.legend()
        plt.show()

    @staticmethod
    def autoencoder_test():
        from sklearn.datasets import make_blobs
        from sklearn.preprocessing import MinMaxScaler

        data = make_blobs(n_samples=100, n_features=3, centers=2, random_state=101)
        scaler = MinMaxScaler()
        scaled_data = scaler.fit_transform(data[0])

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(scaled_data[:, 0], scaled_data[:, 1], scaled_data[:, 2], c=data[1])
        plt.show(block=fig)

        number_of_features = 3
        learning_rate = 0.01
        number_of_steps = 1000

        autoencoder = AutoEncoder(number_of_features, [[-1, tf.keras.activations.sigmoid]])
        autoencoder.compile(learning_rate)
        data_source = DataSource(train=[scaled_data, scaled_data], batch_size=len(scaled_data))

        x_batch, y_batch = data_source.get_current_training_batch()
        for iteration in range(number_of_steps):
            autoencoder.training_step(x_batch, y_batch)

        y_pred = autoencoder.predict(x_batch)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(y_pred[:, 0], y_pred[:, 1], y_pred[:, 2], c=data[1])
        plt.show(block=fig)

        # TODO: implement autoencoder based on custom model
        # URL: https://www.tensorflow.org/tutorials/generative/cvae
        #
        # output_2d = autoencoder.get_encoded()
        #
        # plt.scatter(output_2d[:, 0], output_2d[:, 1], c=data[1])
        # plt.show()

    @staticmethod
    def lsvm_test():

        # Load DataManipulation
        iris = datasets.load_iris()
        X = iris.data[:, :2][iris.target != 2]
        y = iris.target[iris.target != 2]

        # Change labels to +1 and -1
        y = np.where(y == 1, y, -1)

        epochs = 5000

        model = LinearSVM(2)
        model.compile(learning_rate=0.05)
        model.train(X, y, epochs=epochs, verbose=False)

        Visualizer.visualize_2d(X, y, model, output_converter=LinearSVM.binary_activation_sign)

    @staticmethod
    def hog_test():
        image = data.astronaut()
        sw = StopWatch()
        sw.start()

        fd, hog_image = hog(image, orientations=8, pixels_per_cell=(8, 8),
                            cells_per_block=(1, 1), visualize=True, multichannel=True)

        sw.stop()

        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)

        ax1.axis('off')
        ax1.imshow(image, cmap=plt.cm.gray)
        ax1.set_title('Input image')

        # Rescale histogram for better display
        hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 10))

        ax2.axis('off')
        ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
        ax2.set_title('Histogram of Oriented Gradients')
        plt.show()

    @staticmethod
    def create_test_data(from_index, count):
        data_array = np.arange(from_index, from_index + count).reshape(count, 1)
        data_array = np.append(data_array, np.arange(from_index, from_index + count).reshape(count, 1), axis=1)
        data_array = np.append(data_array, np.arange(from_index, from_index + count).reshape(count, 1), axis=1)

        label_array = np.arange(from_index, from_index + count).reshape(count, 1)
        label_array = np.append(label_array, np.arange(from_index, from_index + count).reshape(count, 1), axis=1)

        return data_array, label_array

    @staticmethod
    def data_source_test():
        im = BreadthFirstBatchIndexManager(100, 22, 10)

        manager = StratifiedDataSource(10)

        data_array, label_array = MachineLearningTests.create_test_data(0, 20)
        manager.add_data(data_array, label_array, 0.5)

        data_array, label_array = MachineLearningTests.create_test_data(100, 50)
        manager.add_data(data_array, label_array, 0.5)

        print(manager.get_current_batch())

        while manager.next_batch():
            print(manager.get_current_batch())


MachineLearningTests.data_source_test()