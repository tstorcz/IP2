import cv2
import math
import numpy
import random

from matplotlib import pyplot as plt

from skimage import measure
from skimage import morphology
from skimage.morphology import convex_hull_image
from skimage.morphology import skeletonize

from Graph.Vectorizer import Vectorizer
from Image.Comparer import Comparer
from Image.Converter import Converter
from Image.EdgeDetection import EdgeDetection
from Image.Filter import Filter
from Image.Histogram import Histogram
from Image.ImageLoader import ImageLoader
from Image.ImageOut import ImageOut
from Image.Kernel import Kernel
from Image.Moments import Moments
from Image.Segment import Segment
from Image.Thinner import Thinner
from Image.Threshold import Threshold
from Tools.Files.FileSystem import FileSystem


class ImageTests(object):

    @staticmethod
    def moments_test(leaf_bin):
        im = Moments(leaf_bin)
        zernike = im.zernike(15)
        hu = im.hu()
        print(len(zernike))
        print(len(hu))

    @staticmethod
    def pre_process_test(leaf_bin):
        skeleton = skeletonize(leaf_bin)
        it = Thinner()
        skeleton1 = it.multi_scale_thinning(skeleton)
        medial = morphology.medial_axis(leaf_bin)
        contours = measure.find_contours(leaf_bin, 0.8)
        contour_vectors = Vectorizer.vectorize(contours[0])
        convex_hull = convex_hull_image(leaf_bin)
        convex_hull_contour = measure.find_contours(convex_hull, 0.8)
        convex_hull_contour_vector = Vectorizer.vectorize(convex_hull_contour[0])

        fig, axes = plt.subplots(2, 3)
        ax = axes.ravel()

        ax[0].imshow(leaf_bin)
        ax[0].set_title('original')
        ax[0].axis('off')

        ax[1].imshow(skeleton)
        ax[1].set_title('skeleton')
        ax[1].axis('off')

        ax[2].imshow(skeleton1)
        ax[2].set_title('skeleton 1')
        ax[2].axis('off')

        ax[3].imshow(medial)
        ax[3].set_title('medial axis')
        ax[3].axis('off')

        ax[4].imshow(leaf_bin)
        ax[4].set_title('contour vectors')
        ax[4].axis('off')
        pxs = [p[0] for p in contour_vectors]
        pys = [p[1] for p in contour_vectors]
        ax[4].plot(pys, pxs)

        ax[5].imshow(convex_hull)
        ax[5].set_title('convex hull')
        ax[5].axis('off')
        pxs = [p[0] for p in convex_hull_contour_vector]
        pys = [p[1] for p in convex_hull_contour_vector]
        ax[5].plot(pys, pxs)

        fig.tight_layout()
        plt.show()

    @staticmethod
    def otsu_test():
        image_hsv = ImageLoader.load_image_pixels(
            "C:\\Users\\Storcz Tamas\\Desktop\\PhD\\Works\\Color\\Images\\circular_otsu_10.jpg",
            'HSV'
        )
        hist, _ = numpy.histogram(image_hsv[:, :, 0], bins=numpy.arange(0, 256, 1), density=False)
        # hist = ImageTests.generate_otsu_test_data()

        s, e, icv = Threshold.otsu_cyclic(hist)
        # s, e, icv = Threshold.otsu(hist)

        d = e - s

        print(s, e, d, len(hist) - d, math.sqrt(icv))

        rolled_histogram = numpy.roll(hist, -s)

        max_1 = numpy.nonzero(rolled_histogram[:d] == max(rolled_histogram[:d]))[0][0] + s
        mean_1 = numpy.average(numpy.arange(0, d), weights=rolled_histogram[:d])
        var_1 = math.sqrt(numpy.var(rolled_histogram[:d]))

        max_2 = numpy.nonzero(rolled_histogram[d:] == max(rolled_histogram[d:]))[0][0] + s + d
        mean_2 = numpy.average(numpy.arange(d, len(rolled_histogram)), weights=rolled_histogram[d:])
        var_2 = math.sqrt(numpy.var(rolled_histogram[d:]))

        print(math.fabs(max_1 - mean_1), max_1, mean_1, var_1)
        print(math.fabs(max_2 - mean_2), max_2, mean_2, var_2)
        print("-----")

        plt.plot(numpy.arange(0, len(hist)), hist)
        plt.axvline(x=s, color='g', linestyle='-.')
        plt.axvline(x=e - 1, color='r', linestyle='-.')

        plt.show()

    @staticmethod
    def generate_otsu_test_data():
        lead_in = 1
        step = 0.1
        space = 10
        lead_out = 1

        x1 = numpy.arange(-4, 4, step)
        x2 = numpy.arange(4 + space, 12 + space, step)
        y1 = [ImageTests.otsu_data_generator(x, 0, 200) for n, x in enumerate(x1)]
        y2 = [ImageTests.otsu_data_generator(x, 8 + space, 500) for n, x in enumerate(x2)]

        hist = \
            [abs(int(10 * random.random())) for _ in range(int(lead_in / step))] \
            + y1 \
            + [abs(int(10 * random.random())) for _ in range(int(space / step))] \
            + y2 \
            + [abs(int(10 * random.random())) for _ in range(int(lead_out / step))]
        return hist

    @staticmethod
    def otsu_data_generator(x, loc, coeff, scale=1):
        return (1 / numpy.sqrt(2 * numpy.pi * scale ** 2)) * numpy.exp(-(x - loc) ** 2 / (2 * scale ** 2)) * coeff

    @staticmethod
    def segmentation(image_path_descriptor):
        file_name = image_path_descriptor[0]
        image_hsv = ImageLoader.load_image_pixels(file_name, 'HSV')
        image_rgb = ImageLoader.load_image_pixels(file_name, 'RGB')

        image_hue = image_hsv[:, :, 0]
        image_sat = image_hsv[:, :, 1].astype(numpy.float)
        image_val = image_hsv[:, :, 2].astype(numpy.float)
        image_hue_rel = numpy.multiply(image_sat / 255.0, image_hue / 255.0)
        image_gray = Converter.convert_image_data_from_rgb(image_rgb, 'intensity')
        image_light = Converter.convert_image_data_from_rgb(image_rgb, 'lightness')

        hue_rel_threshold = 0.03

        print("hue")
        bw_hue, hue_start, hue_end, hist = Segment.histogram_threshold(
            image_hue,
            Threshold.otsu_cyclic,
            mask=image_hue_rel > hue_rel_threshold
        )

        hue_reliability = Histogram.test_overlap(hist, hue_start, hue_end, circular_histogram=True)
        print(hue_reliability)
        Histogram.show_segmentation(hist, hue_start, hue_end)

        print("hue weights")
        weight_hue_norm = Segment.get_histogram_segmentation_weights_by_normalized_distance(image_hue, bw_hue)
        print("hue uncertainty average: ", round(numpy.average(weight_hue_norm), 3))
        print("hue uncertainty std. dev.: ", round(math.sqrt(float(numpy.std(weight_hue_norm))), 3))

        print("gray")
        bw_gray, gray_start, gray_end, hist = Segment.histogram_threshold(image_gray, Threshold.otsu)
        print(Histogram.test_overlap(hist, gray_start, gray_end))
        Histogram.show_segmentation(hist, gray_start, gray_end)
        print("gray weights")
        weight_gray_norm = Segment.get_histogram_segmentation_weights_by_normalized_distance(image_gray, bw_gray)
        print("gray uncertainty average: ", round(numpy.average(weight_gray_norm), 3))
        print("gray uncertainty std. dev.: ", round(math.sqrt(float(numpy.std(weight_gray_norm))), 3))

        print("lightness")
        bw_light, light_start, light_end, hist = Segment.histogram_threshold(image_light, Threshold.otsu)
        print(Histogram.test_overlap(hist, light_start, light_end))
        Histogram.show_segmentation(hist, light_start, light_end)
        print("lightness weights")
        weight_light_norm = Segment.get_histogram_segmentation_weights_by_normalized_distance(image_light, bw_light)
        print("lightness uncertainty average: ", round(numpy.average(weight_light_norm), 3))
        print("lightness uncertainty std. dev.: ", round(math.sqrt(float(numpy.std(weight_light_norm))), 3))

        print("dominant (value of hsv)")
        bw_val, val_start, val_end, hist = Segment.histogram_threshold(image_val, Threshold.otsu)
        print(Histogram.test_overlap(hist, val_start, val_end))
        Histogram.show_segmentation(hist, val_start, val_end)
        print("dominant weights")
        weight_val_norm = Segment.get_histogram_segmentation_weights_by_normalized_distance(image_val, bw_val)
        print("dominant uncertainty average: ", round(numpy.average(weight_val_norm), 3))
        print("dominant uncertainty std. dev.: ", round(math.sqrt(float(numpy.std(weight_val_norm))), 3))

        # Scale binary images
        bw_hue = Segment.to_binary(bw_hue, threshold=0.5, high_value=255)
        bw_gray = Segment.to_binary(bw_gray, threshold=0.5, high_value=255)
        bw_light = Segment.to_binary(bw_light, threshold=0.5, high_value=255)
        bw_val = Segment.to_binary(bw_val, threshold=0.5, high_value=255)

        bw_hue[(image_hue_rel <= hue_rel_threshold)] = 128

        corrected = numpy.copy(bw_val)
        if hue_reliability >= 0.95:
            corrected[(image_hue_rel > hue_rel_threshold)] = bw_hue[(image_hue_rel > hue_rel_threshold)]

        corrected = morphology.area_opening(corrected)
        corrected = morphology.area_closing(corrected)

        corrected = Filter.convolution(corrected, Kernel.create_kernel("box_blur"))
        corrected = Segment.to_binary(corrected, threshold=128, high_value=255)

        output_subfolder = "temp_outputs"
        ImageOut.save_image(
            image_hue,
            FileSystem.create_file_name(file_name, "_image_hue"),
            subfolder=output_subfolder,
            mode="L"
        )

        ImageOut.save_image(
            image_sat,
            FileSystem.create_file_name(file_name, "_image_sat"),
            subfolder=output_subfolder,
            mode="L"
        )

        ImageOut.save_image(
            image_hue_rel * 255,
            FileSystem.create_file_name(file_name, "_image_hue_reliability"),
            subfolder=output_subfolder,
            mode="L"
        )

        ImageOut.save_image(
            bw_hue,
            FileSystem.create_file_name(file_name, "_bw_hue"),
            subfolder=output_subfolder,
            mode="L"
        )

        ImageOut.save_image(
            image_gray,
            FileSystem.create_file_name(file_name, "_image_gray"),
            subfolder=output_subfolder,
            mode="L"
        )
        ImageOut.save_image(
            bw_gray,
            FileSystem.create_file_name(file_name, "_bw_gray"),
            subfolder=output_subfolder,
            mode="L"
        )

        ImageOut.save_image(
            image_light,
            FileSystem.create_file_name(file_name, "_image_light"),
            subfolder=output_subfolder,
            mode="L"
        )
        ImageOut.save_image(
            bw_light,
            FileSystem.create_file_name(file_name, "_bw_light"),
            subfolder=output_subfolder,
            mode="L"
        )

        ImageOut.save_image(
            image_val,
            FileSystem.create_file_name(file_name, "_image_val"),
            subfolder=output_subfolder,
            mode="L"
        )
        ImageOut.save_image(
            bw_val,
            FileSystem.create_file_name(file_name, "_bw_val"),
            subfolder=output_subfolder,
            mode="L"
        )

        ImageOut.save_image(
            corrected,
            FileSystem.create_file_name(file_name, "_corrected"),
            subfolder=output_subfolder,
            mode="L"
        )

    @staticmethod
    def rgb_threshold(image_path_descriptor):
        file_name = image_path_descriptor[0]
        image_rgb = ImageLoader.load_image_pixels(file_name, 'RGB')

        print("rgb")
        bw_rgb, weight_rgb = Segment.rgb_threshold(image_rgb)

        plt.imshow(bw_rgb, cmap='gray')
        plt.colorbar()
        plt.show()

        plt.imshow(weight_rgb, cmap='gray')
        plt.colorbar()
        plt.show()

        weight_threshold = 0.08

        weight_rgb[weight_rgb > weight_threshold] = weight_threshold
        weight_rgb = 1 - weight_rgb
        plt.imshow(weight_rgb, cmap='gray')
        plt.colorbar()
        plt.show()

    @staticmethod
    def threshold_mask():
        file_name = "C:\\Sources\\Ragweed_samples\\Selected\\ragweed_009.jpg"
        hsv_image = ImageLoader.load_image_pixels(file_name, 'HSV')
        vividity_mask = Segment.create_distance_mask(hsv_image, [0, 1, 1], 100)

        plt.subplot(221), plt.imshow(hsv_image[:, :, 0], 'gray')
        plt.subplot(222), plt.imshow(hsv_image[:, :, 1], 'gray')
        plt.subplot(223), plt.imshow(hsv_image[:, :, 2], 'gray')
        plt.subplot(224), plt.imshow(vividity_mask, 'gray')
        plt.show()

    @staticmethod
    def segment_images_of_folder():
        FileSystem.process_files_of_folder(
            ImageTests.segmentation,
            "C:\\Sources\\Ragweed_samples\\Selected",
            [".jpg"],
            ["C:\\Sources\\Ragweed_samples\\Selected\\Binary"],
            False
        )

    @staticmethod
    def extract_image_structure(image_path_descriptor):
        leaf = ImageLoader.load_image_pixels(image_path_descriptor[0])

        leaf_bin = Segment.rgb_threshold(leaf)

        # hsv = Converter.convert_image(leaf, Converter.LayerTypes.HSV)
        #
        # intensity = Converter.convert_image(leaf, Converter.LayerTypes.INTENSITY)
        # vivid_mask = Segment.create_vividness_mask(hsv, 30, 30)
        # source = hsv[:, :, 0]
        #
        # plt.subplot(221), plt.imshow((hsv[:,:,1] + hsv[:,:,2])/ 2.0, 'gray')
        # plt.subplot(222), plt.imshow(hsv[:,:,0], 'gray')
        # plt.subplot(223), plt.imshow(hsv[:,:,1], 'gray')
        # plt.subplot(224), plt.imshow(hsv[:,:,2], 'gray')
        # plt.show()
        #
        # source[vivid_mask < 1] = -1;
        #
        # leaf_bin, _, _ = Segment.histogram_threshold(source, Threshold.otsu_cyclic)
        # leaf_bin = Filter.convolution(leaf_bin, "gaussian")
        # leaf_bin = Filter.threshold(leaf_bin, 0.5, 0, 1)
        #
        plt.subplot(221), plt.imshow(leaf, 'gray')
        plt.subplot(222), plt.imshow(leaf_bin, 'gray')
        plt.show()

        # descriptor = graph_test(leaf_bin)
        #
        # if output_folder is not None:
        #     output_file_full_name = os.path.join(output_folder, image_path_descriptor[1].lower() + ".chain")
        #     print("        write descriptor into: {0}".format(output_file_full_name))
        #     text_file = open(output_file_full_name, "w")
        #     text_file.write(" ".join(descriptor))
        #     text_file.close()
        #
        # return descriptor

    @staticmethod
    def compare_input_images():
        image_temp = ImageLoader.load_image_pixels(
            "C:\\Sources\\Ragweed_samples\\Selected_for_shadow\\Used\\Figure 6 intensity.png", "RGB")
        image_intensity = Converter.convert_image_data_from_rgb(image_temp, 'intensity')

        image_temp = ImageLoader.load_image_pixels(
            "C:\\Sources\\Ragweed_samples\\Selected_for_shadow\\Used\\Figure 6 lightness.png", "RGB")
        image_lightness = Converter.convert_image_data_from_rgb(image_temp, 'intensity')

        image_temp = ImageLoader.load_image_pixels(
            "C:\\Sources\\Ragweed_samples\\Selected_for_shadow\\Used\\Figure 6 value.png", "RGB")
        image_value = Converter.convert_image_data_from_rgb(image_temp, 'intensity')

        print("orig max: ", numpy.max(image_intensity))

        print("====== lightness")
        print("=== absolute")
        diff = Comparer.compare_absolute(image_intensity, image_lightness, 255)
        print("avg: ", numpy.average(diff))
        print("std dev: ", numpy.std(diff))
        print("=== relative")
        diff = Comparer.compare_relative(image_intensity, image_lightness)
        print("avg: ", numpy.average(diff))
        print("std dev: ", numpy.std(diff))
        print("=== correlation")
        print("intensity-lightness corr. coeff: ",
              numpy.corrcoef(image_intensity.flatten(), image_lightness.flatten())[0][1])
        print("intensity-lightness diff corr. coeff: ", numpy.corrcoef(image_intensity.flatten(), diff.flatten())[0][1])

        print("====== value")
        print("=== absolute")
        diff = Comparer.compare_absolute(image_intensity, image_value, 255)
        print("avg: ", numpy.average(diff))
        print("std dev: ", numpy.std(diff))
        print("=== relative")
        diff = Comparer.compare_relative(image_intensity, image_value)
        print("avg: ", numpy.average(diff))
        print("std dev: ", numpy.std(diff))
        print("=== correlation")
        print("intensity-value corr. coeff: ", numpy.corrcoef(image_intensity.flatten(), image_value.flatten())[0][1])
        print("intensity-value diff corr. coeff: ", numpy.corrcoef(image_intensity.flatten(), diff.flatten())[0][1])

    @staticmethod
    def get_prediction_errors():
        image_true = Converter.get_binary_image_from_rgb(
            "C:\\Sources\\Ragweed_samples\\Selected_for_shadow\\Binary_manual\\ragweed_011_manual.png"
        )

        image_true = cv2.dilate(image_true, Kernel.create_kernel('ones'), iterations=1)
        true_border = EdgeDetection.find_binary_contour(image_true)

        image_hue = Converter.get_binary_image_from_gray(
            "C:\\Sources\\Ragweed_samples\\Selected_for_shadow\\Outputs\\ragweed_011_bw_hue.jpg"
        )

        image_corrected = Converter.get_binary_image_from_gray(
            "C:\\Sources\\Ragweed_samples\\Selected_for_shadow\\Outputs\\ragweed_011_corrected_val.jpg"
        )

        prediction_error_hue = Comparer.get_specific_predictions(image_true, image_hue, "false_negative")
        prediction_error_corrected = Comparer.get_specific_predictions(image_true, image_corrected, "false_negative")

        prediction_difference = Segment.to_binary(
            Comparer.get_specific_predictions(image_corrected, image_hue, "false_positive"),
            0.5, 255
        )

        print("image size: ", sum(sum(image_true)))
        print("border size: ", sum(sum(true_border)))
        print("hue error: ", sum(sum(prediction_error_hue)))
        print("corrected error: ", sum(sum(prediction_error_corrected)))

        ImageOut.show_images(
            [[prediction_error_hue, prediction_error_corrected],
             [prediction_difference, image_true]]
        )
