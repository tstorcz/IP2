import cv2
import numpy
import time

from matplotlib import pyplot as plt

from StereoVision.StereoVision import StereoVision


class StereoVisionTests(object):

    @staticmethod
    def basic_stereo_test():
        image_number = '01'

        file_name_suffix = ''
        # file_name_suffix = '_laplacian_5x5'
        # file_name_suffix = '_sobel'

        left_file_name = '../images/im_' + image_number + '_l' + file_name_suffix + '.png'
        right_file_name = '../images/im_' + image_number + '_r' + file_name_suffix + '.png'

        print("Left file: ", left_file_name)
        print("Right file: ", right_file_name)

        img_left = cv2.imread(left_file_name, 0)
        img_right = cv2.imread(right_file_name, 0)

        cycle_count = 22
        step = 1
        block_size = 17
        threshold = 0

        depth_map = numpy.ones(img_left.shape) * -16
        disparity = depth_map
        max_count = numpy.zeros(img_left.shape)

        for i in range(1, cycle_count + 1):
            search_range = i * step * 16.0
            start_time = time.time()
            stereo = cv2.StereoBM_create(numDisparities=int(search_range), blockSize=block_size)
            disparity = stereo.compute(img_left, img_right)
            # disparity[max_count > 0] = 0

            print(i, threshold, numpy.amax(disparity), numpy.amax(disparity) - threshold, time.time() - start_time)

            indices = disparity > threshold
            depth_map[indices] = disparity[indices]
            max_count[indices] = max_count[indices] + 1

            threshold += step * 256

        print(depth_map.size, numpy.count_nonzero(depth_map), depth_map.size - numpy.count_nonzero(depth_map))

        depth_threshold = 1550
        depth_map[depth_map < depth_threshold] = depth_threshold
        max_value = numpy.amax(depth_map)
        dig_depth_map = numpy.digitize(depth_map, range(0, int(max_value), int(max_value / 16)))

        # fig, axes = plt.subplots(3, 2)
        # ax = axes.ravel()
        #
        # ax[0].imshow(img_left, 'gray')
        # ax[0].set_title('left image')
        # ax[0].axis('off')
        #
        # ax[1].imshow(img_right, 'gray')
        # ax[1].set_title('right image')
        # ax[1].axis('off')
        #
        # ax[2].imshow(depth_map, 'RdYlBu')
        # ax[2].set_title('depth map')
        # ax[2].axis('off')
        #
        # ax[3].imshow(dig_depth_map, 'RdYlBu')
        # ax[3].set_title('digitized depth map')
        # ax[3].axis('off')
        #
        # ax[4].imshow(disparity, 'RdYlBu')
        # ax[4].set_title('last disparity')
        # ax[4].axis('off')
        #
        # ax[5].imshow(max_count, 'RdYlBu')
        # ax[5].set_title('max count')
        # ax[5].axis('off')
        #
        # fig.tight_layout()
        plt.imshow(dig_depth_map, 'RdYlBu')
        plt.show()

    @staticmethod
    def stereo_vision():
        sv = StereoVision([2, 1], 640, 480, 2)

        sv.online_camera_calibration()
        # sv.offline_calibration()
        sv.scan_point_cloud()