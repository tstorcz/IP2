import math
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from DataSources.TimeSeriesDataSource import TimeSeriesDataSource
from MultiLayerPerceptron import MultiLayerPerceptron
from RecurrentNeuralNetwork import RecurrentSequentialNeuralNetwork


class PalliativTests(object):

    @staticmethod
    def dense_net(input_count, hidden_neuron_count):
        learning_rate = 0.001

        model = MultiLayerPerceptron(
            [
                input_count,
                [
                    [hidden_neuron_count, tf.keras.activations.relu]
                ],
                [1, "binary"]
            ]
        )

        model.compile(learning_rate=learning_rate)

        return model

    @staticmethod
    def dense_net_train(model, x, y, difference_minimum=0.00001, plato_iteration_count=50, epochs=1000):
        diff = 1
        prev = 10
        iteration = 0
        minimum = 1
        minimum_iteration = 0
        while diff > difference_minimum and iteration - minimum_iteration < plato_iteration_count\
                and iteration < epochs:
            iteration += 1
            train_history = model.train(x, y, epochs=1, verbose=False)
            loss = train_history.history['loss'][0]
            if prev <= 1:
                diff = math.fabs(prev - loss)

            if minimum > loss:
                minimum = loss
                minimum_iteration = iteration

            prev = loss

        return iteration

    @staticmethod
    def rnn_test():
        number_of_time_steps = 30
        number_of_inputs = 1
        number_of_rnn_cells = 100
        number_of_outputs = 1
        learning_rate = 0.001
        number_of_iterations = 3000
        batch_count = 40

        data_source = TimeSeriesDataSource(
            0, 10, number_of_points=250,
            time_steps=number_of_time_steps,
            generator_function=np.sin
        )

        rnn = RecurrentSequentialNeuralNetwork(
            number_of_time_steps,
            [
                number_of_inputs,
                number_of_rnn_cells, tf.keras.activations.relu,
                number_of_outputs
            ]
        )
        rnn.compile(learning_rate=learning_rate)

        for i in range(0, number_of_iterations):
            y1, y2, ts = data_source.get_random_batches(batch_count, return_batch_x=True)
            loss = rnn.training_step(y1, y2[:, -number_of_outputs])
            if i % 100 == 0:
                print(i, loss)

        y1, y2, ts = data_source.get_random_batches(1, return_batch_x=True)
        prediction = rnn.predict(y1)
        plt.plot(data_source.x_data, data_source.y_true, label="sin(t)")
        plt.plot(ts.flatten()[:-1], y1.flatten(), "bo", markersize=3, alpha=0.5, label="training data")
        plt.plot(
            ts.flatten()[-number_of_outputs],
            y2.flatten()[-number_of_outputs],
            "ro", markersize=10, alpha=0.5, label="ground truth"
        )
        plt.plot(
            ts.flatten()[-number_of_outputs],
            prediction.flatten()[-number_of_outputs],
            "ko", markersize=5, label="prediction"
        )
        plt.legend()
        plt.show()
