import json
import numpy as np
import random
import os

from matplotlib import pyplot as plt
from PIL import Image

from Image.Converter import Converter
from Image.Filter import Filter
from Image.ImageLoader import ImageLoader
from Image.ImageOut import ImageOut
from Image.Kernel import Kernel
from Tools.Files.FileSystem import FileSystem
from Tools.Measure.StopWatch import StopWatch

from Applications.MiteCount.BackgroundSheet import BackgroundSheet
from Applications.MiteCount.ImageLabel import ImageLabel
from Applications.MiteCount.LogDatabase import LogDatabase
from Applications.MiteCount.MiteFinder import MiteFinder


class BeeStats(object):

    def __init__(self, base_path, kernel_path, output_path, ground_truth_path, magnitude=1.0,
                 output_kernel_size=(19, 19), mite_image_output_folder=None,
                 database_file_name="sheet_stats.db", database_table_name=None, use_database=False,
                 ocr_model_path=None, mite_identifier=None
                 ):
        self.magnitude = magnitude
        self.source_path = base_path
        self.kernel_path = kernel_path
        self.output_kernel_size = output_kernel_size
        self.output_path = output_path
        self.ground_truth_path = ground_truth_path

        self.mite_image_output_folder = mite_image_output_folder
        self.ocr_model_path = ocr_model_path
        self.mite_identifier = mite_identifier

        self.thumb_path = os.path.join(self.output_path, "thumbnails")
        FileSystem.create_folder(self.thumb_path)

        self.edge_kernel = []
        self.label_frame_kernels = []
        self.label_number_kernels = []
        self.mite_kernels = []

        self.__set_kernels()

        self.database = LogDatabase(
            os.path.join(self.source_path, database_file_name),
            table_name=database_table_name
        )
        self.database.active(use_database)

    def __get_kernel_image(self, image_name, magnitude=None, threshold_method=None):

        if magnitude is None:
            magnitude = self.magnitude

        if threshold_method is None:
            threshold_method = BeeStats.kernel_threshold_binary

        kernel_full_name = os.path.join(self.kernel_path, image_name)

        if magnitude != 1:
            thumb_name = ImageOut.create_thumbnail(
                kernel_full_name,
                dst_file_name="kernel_thumb",
                magnitude=magnitude
            )
        else:
            thumb_name = kernel_full_name

        kernel = ImageLoader.load_image_pixels(thumb_name, 'L').astype(np.float)

        if threshold_method is not None:
            threshold_method(kernel)

        return kernel

    @staticmethod
    def get_kernels_from_path(path, rotation_angles, output_kernel_size):
        mite_kernel_names = FileSystem.get_file_names_of_folder(path, is_recursive=False)
        kernel_collection = []

        for kernel_name in mite_kernel_names:
            mite_kernel = ImageLoader.load_image_pixels(kernel_name[0], 'L').astype(np.float)

            # Filter.high_pass(mite_kernel, 200, -1)
            # Filter.low_pass(mite_kernel, 200, 1)

            kernel_collection.extend(
                Kernel.rotate_kernel(
                    mite_kernel, rotation_angles,
                    size=output_kernel_size
                )
            )

        return kernel_collection

    @staticmethod
    def kernel_threshold_3_ways(kernel):
        Filter.high_pass(kernel, 85, -1)
        Filter.low_pass(kernel, 170, 1)
        Filter.low_pass(kernel, 85, 0)

    @staticmethod
    def kernel_threshold_binary(kernel):
        Filter.high_pass(kernel, 200, -1)
        Filter.low_pass(kernel, 200, 1)

    def __set_kernels(self):
        self.edge_kernel = self.__get_kernel_image("frame\\edge_r_31.jpg", magnitude=1)

        # self.label_frame_kernels = [
        #     self.__get_kernel_image("frame_tl.jpg", threshold_method=BeeStats.kernel_threshold_3_ways),
        #     # , self.__get_kernel_image("line_v_38x70_l3.jpg")
        #     # , self.__get_kernel_image("line_v_38x70_l7.jpg")
        #     # , self.__get_kernel_image("line_v_38x70_r3.jpg")
        #     # , self.__get_kernel_image("line_v_38x70_r7.jpg")
        # ]

        self.label_frame_kernels = [self.__get_kernel_image("frame\\frame_tl_8.jpg", magnitude=1)]
        self.label_frame_kernels.append(np.rot90(self.label_frame_kernels[-1]))
        self.label_frame_kernels.append(np.rot90(self.label_frame_kernels[-1]))
        self.label_frame_kernels.append(np.rot90(self.label_frame_kernels[-1]))

        self.label_number_kernels = [
            self.__get_kernel_image("number\\0.jpg", magnitude=0.5),
            self.__get_kernel_image("number\\1.jpg", magnitude=0.5),
            self.__get_kernel_image("number\\2.jpg", magnitude=0.5),
            self.__get_kernel_image("number\\3.jpg", magnitude=0.5),
            self.__get_kernel_image("number\\4.jpg", magnitude=0.5),
            self.__get_kernel_image("number\\5.jpg", magnitude=0.5),
            self.__get_kernel_image("number\\6.jpg", magnitude=0.5),
            self.__get_kernel_image("number\\7.jpg", magnitude=0.5),
            self.__get_kernel_image("number\\8.jpg", magnitude=0.5),
            self.__get_kernel_image("number\\9.jpg", magnitude=0.5),
            self.__get_kernel_image("frame\\line_v_38x79.jpg", magnitude=0.5),
        ]

        self.mite_kernels = BeeStats.get_kernels_from_path(
            os.path.join(self.kernel_path, "mite"),
            rotation_angles=Kernel.get_rotation_angles(-90, 90, 8),
            output_kernel_size=self.output_kernel_size
        )

    def process_images(self, image_names):
        full_watch = StopWatch()
        full_watch.start()
        print("Processing {0} files...".format(len(image_names)))
        self.database.open()

        image_names = sorted(image_names, key=lambda x: x[0])

        #for i in range(10000, 12000):
        for i in range(0, len(image_names)):
            image_name = image_names[i]
            image_name_with_relative_path = image_name[0][len(self.source_path) + 1:len(image_name[0])]

            found = self.database.get_by_name(
                image_name_with_relative_path
            )

            if len(found) == 0:
                row_id = self.database.insert(
                    self.database.insert_without_stats_command,
                    [image_name_with_relative_path, image_name[1]]
                )
            else:
                row_id = found[0][0]

            try:
                rot = ImageLoader.get_image_rotation_angle(image_name[0])
                print(i + 1, found, rot)

                stats = self.process_single_image(image_name, row_id)
                print(i + 1, stats)

                self.database.update(
                    self.database.update_by_id_command,
                    [
                        stats.file_name_with_path[len(self.source_path) + 1:len(stats.file_name_with_path)],
                        stats.file_name,
                        json.dumps(stats.paper_corners), int(stats.paper_area), stats.label,
                        stats.roi_count, stats.roi_count, stats.operation_time,
                        rot[1],
                        row_id
                    ]
                )

            except Exception as ex:
                print("Failed processing file: {0} error: {1}".format(image_name[0], ex))

        self.database.commit()
        self.database.close()
        full_watch.stop()
        print("processing {0} files: {1}s".format(len(image_names), int(round(full_watch.total_time()))))

    def process_single_image(self, image_name, image_id):
        watch = StopWatch()
        watch.start()

        print("-----------------------------------------------------------")
        print(image_name)

        thumb_paper_name = ImageOut.create_thumbnail(
            image_name[0],
            dst_path=self.thumb_path,
            dst_file_name="<name>_thumb.<ext>",
            magnitude=self.magnitude
        )

        thumb_label_name = ImageOut.create_thumbnail(
            image_name[0],
            dst_path=self.thumb_path,
            dst_file_name="<name>_thumb_q.<ext>",
            magnitude=0.5
        )
        watch.split("thumbs created")

        sheet = self.get_paper(
            ImageLoader.load_oriented_image_pixels(thumb_paper_name, 'L'),
            image_name[0]
        )
        watch.split("paper")

        sheet.label_image = self.get_label(thumb_label_name)
        watch.split("label")

        stats = sheet.get_stats()
        stats.file_name = image_name[1]

        mite_finder = self.mite_identification(
            image_name[0],
            image_id,
            sheet.get_paper_rect(magnification=1.0/self.magnitude)
        )
        stats.roi_count = mite_finder.roi_count()
        stats.roi_count = 0
        watch.split("mite count")

        watch.stop()
        stats.operation_time = watch.total_time()

        return stats

    def get_paper(self, image_gray, image_file_name):

        if image_gray.shape[1] > image_gray.shape[0]:
            image_gray = np.rot90(image_gray)

        sheet = BackgroundSheet(
            image_file_name,
            image_gray,
            self.edge_kernel,
            template_magnification=self.magnitude,
            ocr_model_path=self.ocr_model_path
        )

        return sheet

    def get_label(self, image_name):
        im = Image.open(image_name).convert("L")
        if im.size[0] > im.size[1]:
            im = im.rotate(90, expand=True)

        size = [im.size[0] / ImageLabel.LABEL_IMAGE_WIDTH_RATIO, im.size[1] / ImageLabel.LABEL_IMAGE_HEIGHT_RATIO]
        top_left = [0, 0]
        label_image = im.crop(
            (top_left[0], top_left[1], top_left[0] + size[0], top_left[1] + size[1])
        ).rotate(180, expand=True)

        label_image_data = np.array(label_image)

        label = ImageLabel(label_image_data, self.label_frame_kernels, self.label_number_kernels)

        if not label.has_full_label:
            top_left = [im.size[0] - size[0] - 1, im.size[1] - size[1] - 1]
            label_image_data = im.crop((top_left[0], top_left[1], top_left[0] + size[0], top_left[1] + size[1]))
            other_label = ImageLabel(label_image_data, self.label_frame_kernels, self.label_number_kernels)

            if other_label.accuracy > label.accuracy:
                label = other_label

        return label

    def mite_identification(self, image_name, image_id, rectangle=None):
        image = ImageLoader.load_oriented_image_pixels(image_name)

        if image.shape[1] > image.shape[0]:
            image = np.rot90(image)

        mite_finder = MiteFinder(
            image,
            self.mite_kernels,
            mite_correlations_threshold=0.50,
            paper_rectangle=rectangle,
            mite_identifier=self.mite_identifier
        )

        if self.mite_image_output_folder is not None:
            mite_finder.save_roi_images(self.mite_image_output_folder, image_id)

        return mite_finder

    def check_mite_identification(self, sheet, mite_finder):
        image_ground_truth = self.get_mite_ground_truth_image(sheet.image_file_name)

        [y, x] = [[y for y, x in mite_finder.roi_locations], [x for y, x in mite_finder.roi_locations]]
        plt.gray()
        plt.imshow(image_ground_truth.astype(np.uint8))
        plt.scatter(x, y, s=5)
        plt.show()

    def get_mite_ground_truth_image(self, file_name):
        return ImageLoader.load_oriented_image_pixels(
            FileSystem.create_file_name(file_name, "szamoltjelöltellenörzött", self.ground_truth_path)
        )

    @staticmethod
    def create_random_sample_parts(image,
                                   sample_size_y, sample_size_x, sample_count,
                                   y_frame=0, x_frame=0,
                                   out_file_name=None):
        rnd = random.Random()

        for i in range(0, sample_count):
            y = rnd.randrange(y_frame, image.shape[0] - sample_size_y - y_frame)
            x = rnd.randrange(x_frame, image.shape[1] - sample_size_x - x_frame)

            part_bw = image[y:y + sample_size_y, x:x + sample_size_x]
            part_rgb = Converter.convert_image_data_to_rgb(part_bw, source_type="grayscale")

            if out_file_name is not None:
                ImageOut.save_image(
                    part_rgb,
                    out_file_name.replace("{x}", str(x)).replace("{y}", str(y)).replace("{i}", str(i)),
                    mode="RGB"
                )
