import json
import os

from PIL import Image

from Applications.MiteCount.LogDatabase import LogDatabase
from Image.ImageOut import ImageOut
from Math.Rectangle import Rectangle
from Tools.Files.FileSystem import FileSystem


database_path = r"D:\MIK\mite\Atkafotok\sheet_stats.db"
table_name = "log_entry_2"
image_path = r"D:\MIK\mite\Atkafotok"
roi_path = r"D:\MIK\mite\mites"
paper_magnification = 0.2

# portrait image dimensions
image_width = 3456
image_height = 4608
object_size = 21


def process_images(database, folder_names):
    database.open()

    # for i in range(3300, 4000):
    for folder_name in folder_names:
        image_id = int(folder_name.split("_")[1].strip())
        found = database.get_by_id(image_id)

        if len(found) == 1 and found[0][4] > 0:
            target_folder_name = os.path.join(folder_name, "out")
            FileSystem.create_folder(folder_name, "out")
            roi_list = FileSystem.get_file_names_of_folder(folder_name)

            paper_corners = json.loads(found[0][3])
            paper_corners = [[i / paper_magnification for i in r] for r in paper_corners]
            paper_rect = Rectangle(paper_corners)

            for roi in roi_list:

                if found[0][8] == 90:
                    coords = [image_height - int(roi[1].split("_")[4][1:]), int(roi[1].split("_")[3][1:])]
                elif found[0][8] == 180:
                    coords = [
                        image_width - int(roi[1].split("_")[3][1:]),
                        image_height - int(roi[1].split("_")[4][1:])
                    ]
                elif found[0][8] == 270:
                    coords = [image_height - int(roi[1].split("_")[4][1:]), int(roi[1].split("_")[3][1:])]
                else:
                    coords = [int(roi[1].split("_")[3][1:]), int(roi[1].split("_")[4][1:])]

                img = Image.open(os.path.join(image_path, found[0][1]))
                ImageOut.show_images(
                    [
                        img,
                        img.crop((coords[1], coords[0], coords[1] + object_size, coords[0] + object_size)),
                        Image.open(os.path.join(folder_name, roi[1] + ".jpg"))
                    ]
                )

                if not paper_rect.includes_point(coords):
                    FileSystem.move_file(roi[0], os.path.join(target_folder_name, roi[1]))

    database.close()


log_database = LogDatabase(
            database_path,
            table_name=table_name
        )

folder_list = FileSystem.get_folders_of_folder(roi_path)

process_images(log_database, folder_list)
