import numpy as np

from MachineLearningTests import MachineLearningTests
from DataSources.DataSource import DataSource


def get_cog(arr):
    cog_y = 0.0
    cog_x = 0.0
    sum = 0.0
    for i in range(0, 9):
        cog_y += arr[i] * ((i // 3) + 1)
        cog_x += arr[i] * ((i % 3) + 1)
        sum += arr[i]

    if sum != 0:
        return int(cog_y/sum) - 1, int(cog_x/sum) - 1

    return 0, 0


def get_cog_labels():
    labeled_data = []
    for v in range(0, 2**9):
        arr = [int(x) for x in list('{0:09b}'.format(v))]
        one_hot_array = np.reshape(DataSource.one_hot_from_index(1 if get_cog(arr) == (1, 1) else 0, 2), -1).tolist()
        arr.extend(one_hot_array)
        labeled_data.append(arr)
    return np.asarray(labeled_data)


# MachineLearningTests.k_means_test()
# MachineLearningTests.perceptron_test()
# MachineLearningTests.perceptron_test_2(get_cog_labels())
# MachineLearningTests.deep_conv_net_test()
# MachineLearningTests.rnn_test()
# !MachineLearningTests.autoencoder_test()
# MachineLearningTests.lsvm_test()
MachineLearningTests.hog_test()
