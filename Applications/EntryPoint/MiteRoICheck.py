import matplotlib.pyplot as plt
import numpy as np
import os
import sys

from Image.ImageLoader import ImageLoader
from Image.Kernel import Kernel
from Tools.Files.FileSystem import FileSystem

from Applications.MiteCount.MiteFinder import MiteFinder


def project(data):
    mi = min(data)
    ma = max(data)
    d = ma - mi
    return [(float(i) - mi) / d for i in data]


def normalize(data):
    s = sum(data)
    return [float(i) / s for i in data]


def get_kernels_from_path(path, rotation_angles, extracted_kernel_size):
    mite_kernel_names = FileSystem.get_file_names_of_folder(path, is_recursive=False)
    kernel_collection = []

    for kernel_name in mite_kernel_names:
        mite_kernel = ImageLoader.load_image_pixels(kernel_name[0], 'L').astype(np.float)

        # Filter.high_pass(mite_kernel, 200, -1)
        # Filter.low_pass(mite_kernel, 200, 1)

        kernel_collection.extend(
            Kernel.rotate_kernel(
                mite_kernel, rotation_angles,
                size=extracted_kernel_size
            )
        )

    return kernel_collection


def classify_samples_to_bins(image_names, mite_finder: MiteFinder, number_of_bins, verbose=True, rename_file=False):
    items_in_bins = [0] * number_of_bins
    for image_name in image_names:
        original_image = ImageLoader.load_oriented_image_pixels(image_name[0])
        mite_finder.set_image(original_image)

        level = int(mite_finder.roi_location_coefficients[0] * 100)
        level_string = "_cl{0:02}".format(level)

        if verbose:
            print(image_name[0], level_string)

        file_name = image_name[1]
        level_pos = file_name.find("_cl")
        if level_pos >= 0:
            file_name = file_name[:level_pos] + file_name[level_pos + 5:]

        if rename_file:
            FileSystem.move_file(
                image_name[0],
                os.path.join(os.path.dirname(image_name[0]), file_name + level_string + image_name[2])
            )

        if level == 100:
            items_in_bins[-1] += 1
        else:
            items_in_bins[level // int(100 / number_of_bins)] += 1

    return items_in_bins


def get_image_names(folder, is_recursive=False):
    image_folder_full_name = os.path.join(base_path, folder)
    return FileSystem.get_file_names_of_folder(image_folder_full_name, is_recursive=is_recursive)


base_path = sys.argv[1]
exported_image_size = 38
exported_kernel_size = 27
correlation_threshold = 0.1
class_count = 20

kernel_folder = os.path.join(base_path, "kernels", "mite_{0}".format(exported_image_size))
rotated_kernels = get_kernels_from_path(
    os.path.join(base_path, kernel_folder),
    rotation_angles=Kernel.get_rotation_angles(-90, 90, 8),
    extracted_kernel_size=(exported_kernel_size, exported_kernel_size)
)

source_list = [
    {"folder_name": "mite_samples\\positive_38", "inverse": False, "color": "green"},
    {"folder_name": "mite_samples\\negative_38", "inverse": True, "color": "blue"},
]

bar_width = 0.8 / len(source_list)

fig, (ax1, ax2) = plt.subplots(2, sharex=True)

i = 0

count = []
sum_count = [0] * class_count
grand_sum = 0

mite_finder = MiteFinder(
                None,
                rectangle_mask=None,
                mite_kernels=rotated_kernels,
                mite_correlations_threshold=correlation_threshold,
                crop_size=(exported_image_size, exported_image_size)
            )

for source in source_list:
    image_names = get_image_names(
        source["folder_name"],
        is_recursive=source["recursive_folder"] if "recursive_folder" in source.keys() else False
    )

    count.append(
        classify_samples_to_bins(image_names, mite_finder, class_count, verbose=False)
    )
    for ci in range(0, class_count):
        sum_count[ci] += count[-1][ci]
        grand_sum += count[-1][ci]

    if source["inverse"]:
        cum_sum = [1 - x for x in np.cumsum(normalize(count[-1]))]
    else:
        cum_sum = np.cumsum(normalize(count[-1]))

    x2 = [x + bar_width * i for x in range(0, class_count)]

    ax1.bar(x2, normalize(count[-1]), width=bar_width, align='center', color=source["color"])

    ax2.plot(range(0, class_count), cum_sum, color=source["color"])

    i += 1

x_labels = range(0, 100, int(100 / class_count))
plt.xticks(range(0, class_count), x_labels)

plt.show()

for ci in range(0, class_count):
    out = "{0} -> <{1:.0f}> {2:.0f}%".format(x_labels[ci], sum_count[ci], 100 * sum_count[ci] / grand_sum if grand_sum > 0 else 0)
    for si in range(0, len(count)):
        out = "{0} {1}: <{2:.0f}> {3:.0f}% [{4:.0f}%]".format(
            out, si, count[si][ci],
            100 * count[si][ci] / sum_count[ci] if sum_count[ci] > 0 else 0,
            100 * count[si][ci] / grand_sum if grand_sum > 0 else 0
        )

    print(out)
