import math

import numpy as np
import os
import random
import socket
import sys
import tensorflow as tf

from datetime import datetime

from sklearn.model_selection import train_test_split

from Image.Filter import Filter
from Image.ImageData import ImageData
from Image.ImageLoader import ImageLoader
from Image.Kernel import Kernel
from Image.Transformer import Transformer
from MachineLearning.DataSources.StratifiedDataSource import StratifiedDataSource
from MachineLearning.NeuralNets.SequentialNeuralNetworks.FeedForwardNeuralNets.MultiLayerPerceptron import \
    MultiLayerPerceptron
from Tools.Network.EmailSender import EmailSender
from Tools.Files.FileSystem import FileSystem
from Tools.Logging.Logger import Logger
from Tools.Logging.Logger import LogLevels
from Tools.Logging.ConsoleLogChannel import ConsoleLogChannel
from Tools.Logging.FileLogChannel import FileLogChannel

from Applications.MiteCount.MiteDSNN import MiteDSNet


def get_kernels_from_path(path, rotation_angles, out_kernel_size):
    mite_kernel_names = FileSystem.get_file_names_of_folder(path, is_recursive=False)
    kernel_collection = []

    for kernel_name in mite_kernel_names:
        mite_kernel = ImageLoader.load_image_pixels(kernel_name[0], 'L').astype(np.float)

        kernel_collection.extend(
            Kernel.rotate_kernel(
                mite_kernel, rotation_angles,
                size=out_kernel_size
            )
        )

    return kernel_collection


def get_images_from_file(image_names, image_collection, mode="RGB",
                         transformer=None, mean_threshold=30, var_threshold=0):
    mean_min = None
    var_min = None

    for image_file_descriptor in image_names:
        image = ImageLoader.load_image_pixels(image_file_descriptor[0], mode)

        current_mean = image.mean()
        current_var = image.var()

        if mean_min is None or current_mean < mean_min:
            mean_min = current_mean

        if var_min is None or current_var < var_min:
            var_min = current_var

        if current_mean >= mean_threshold and current_var >= var_threshold:
            if transformer is not None:
                transformed_image = transformer(image)
            else:
                transformed_image = [image]

            image_collection.extend(transformed_image)
        else:
            FileSystem.move_file(
                image_file_descriptor[0],
                FileSystem.add_subfolder(image_file_descriptor[0], "invalid")
            )

    if mean_min is None:
        print("No images found!")
    else:
        print("Image count: {0}, Minimum mean: {1}, var {2}".format(len(image_names), mean_min, math.sqrt(var_min)))


def normalize_image(image):
    new_image = ImageData.crop_image(
        image,
        [network_input_image_size, network_input_image_size],
        center=[int(image.shape[0] // 2), int(image.shape[1] // 2)]
    )

    return [Filter.project(new_image, from_range=(0, 255), to_range=(-1, 1))]


def augment_and_normalize_image(image):
    new_images = Transformer.augmentation(
        image,
        network_input_image_size,
        augmentation_count=augmented_image_count,
        enable_mirroring=False
    )

    augmented_list = []
    for new_image in new_images:
        augmented_list.append(
            Filter.project(new_image, from_range=(0, 255), to_range=(-1, 1))
        )

    return augmented_list


def compute_histogram(image, bin_count=256, blur_radius=1):
    h = np.histogram(image, range(0, bin_count + 1))[0]

    if blur_radius < 0:
        blur_radius = 0

    if blur_radius > 0:
        h = np.convolve(h, np.ones((2 * blur_radius + 1)), mode="valid")

    return [Filter.project(h, None, (0, 1)).reshape(bin_count - 2 * blur_radius).tolist()]


def compute_augmented_histograms(image):
    new_images = Transformer.augmentation(
        image,
        network_input_image_size,
        augmentation_count=augmented_image_count,
        enable_mirroring=False
    )

    augmented_list = []
    for new_image in new_images:
        augmented_list.extend(
            compute_histogram(new_image)
        )

    return augmented_list


def sample_file_correlation_filter(image_names):
    filtered_list = []
    for image_name in image_names:
        level_pos = image_name[1].find("_cl")
        if level_pos >= 0:
            level = int(image_name[1][level_pos + 3:level_pos + 5])
        else:
            level = 0

        if level > 80:
            filtered_list.append(image_name)
    return filtered_list


def get_image_data_from_files(path, file_name_processor, label,
                              train_rate_of_all, items_in_train_batch,
                              train_data_source, train_transformer,
                              test_data_source, test_transformer,
                              train_max_size=0, test_max_size=0
                              ):
    image_file_names = FileSystem.get_file_names_of_folder(path, is_recursive=False, extension_list=[".jpg"])

    if file_name_processor is not None:
        image_file_names = file_name_processor(image_file_names)

    train_files, test_files = train_test_split(image_file_names, train_size=train_rate_of_all)

    while 0 < train_max_size < len(train_files):
        del train_files[random.randint(1, len(train_files)) - 1]

    while 0 < test_max_size < len(test_files):
        del test_files[random.randint(1, len(test_files)) - 1]

    x_train = []
    x_test = []
    get_images_from_file(
        train_files, x_train,
        transformer=train_transformer, mean_threshold=0,
        mode="L" if input_channel_count == 1 else "RGB"
    )
    get_images_from_file(test_files, x_test,
                         transformer=test_transformer,
                         mean_threshold=0,
                         mode="L" if input_channel_count == 1 else "RGB"
                         )

    # train_data_source.add_data(
    #     np.array(x_train), np.array([label] * len(x_train)).reshape(-1, output_channel_count),
    #     item_rate_in_batch=items_in_train_batch
    # )
    # test_data_source.add_data(
    #     np.array(x_test), np.array([label] * len(x_test)).reshape(-1, output_channel_count),
    #     item_rate_in_batch=0
    # )

    train_data_source[0].extend(np.array(x_train))
    train_data_source[1].extend(np.array([label] * len(x_train)).reshape(-1, output_channel_count))

    test_data_source[0].extend(np.array(x_test))
    test_data_source[1].extend(np.array([label] * len(x_test)).reshape(-1, output_channel_count))


def create_data_source(
        pro_samples_path, con_samples_path,
        train_rate_of_all,
        pro_train_transformer, con_train_transformer, test_transformer,
        classes=None  # [pro, con]
):
    if classes is None:
        classes = [1, 0]

    train_data_source = [[], []]
    test_data_source = [[], []]

    get_image_data_from_files(
        pro_samples_path, None, classes[0],  # sample_file_correlation_filter, 1,
        train_rate_of_all,
        items_in_train_batch=positive_rate,
        train_data_source=train_data_source, train_transformer=pro_train_transformer,
        test_data_source=test_data_source, test_transformer=test_transformer
    )
    get_image_data_from_files(
        con_samples_path, None, classes[1],
        train_rate_of_all,
        items_in_train_batch=1-positive_rate,
        train_data_source=train_data_source, train_transformer=con_train_transformer,
        test_data_source=test_data_source, test_transformer=test_transformer,
        train_max_size=len(train_data_source[1]) * 3,
        test_max_size=len(test_data_source[1]) * 3
    )

    train_array = [np.array(train_data_source[0]), np.array(train_data_source[1])]
    test_array = [np.array(test_data_source[0]), np.array(test_data_source[1])]

    return train_array, test_array


def create_deep_net(image_shape, output_count=None):
    hidden_layer_size = 16

    if output_count is None:
        output_count = 1
        for dim in image_shape:
            output_count *= dim

    logger.log("structure", LogLevels.VERBOSE)

    model = MiteDSNet(
        image_shape,
        [1, 3, 4, tf.keras.activations.relu],
        [
            [1, 3, 8, tf.keras.activations.linear, 0],
            [1, 3, 8, tf.keras.activations.linear, 0]
        ],
        [
            [hidden_layer_size, tf.keras.activations.relu],
            [output_count, tf.keras.activations.sigmoid]
        ],
        use_batch_norm=True
    )

    model.create_optimizer("adam", learning_rate=learning_rate)
    model.compile()

    return model


def teach_roi_classifier_network_by_epochs(model, data_source: StratifiedDataSource, training_epochs):
    epoch_index = 0
    while epoch_index < training_epochs:
        has_next_batch = True
        data_source.reset_batch_index()
        while has_next_batch:
            data, label = data_source.get_current_batch()
            model.training_step(data, label)
            has_next_batch = data_source.next_batch()
        epoch_index += 1

    return model


def evaluate_model(model, data_source, epochs_to_log, log_writer,
                   network_id="",
                   prediction_confidence_distance=0.5,
                   log_section_header=None, log_level=LogLevels.VERBOSE):
    logger.log(
        "{0} with {1}".format(model.get_structure_descriptor(), epochs_to_log),
        log_level
    )

    logger.log(
        model.evaluate(data_source[0], data_source[1]),
        log_level
    )

    return find_classification_errors(
        model, data_source, log_writer,
        network_id=str(network_id),
        log_section_header=log_section_header, log_level=log_level,
        prediction_confidence_distance=prediction_confidence_distance
    )


def shuffle(data):
    shuffled_indices = list(range(len(data[0])))
    np.random.shuffle(shuffled_indices)

    return [data[0][shuffled_indices], data[1][shuffled_indices]]


def network_training(
        positive_path,
        negative_path,
        train_rate_of_all,
        sample_image_size,
        log_writer
):
    if saved_model_path is None:
        deep_net = create_deep_net(
            (sample_image_size, sample_image_size, input_channel_count),
            output_count=output_channel_count
        )
    else:
        deep_net = MiteDSNet(
            (sample_image_size, sample_image_size, input_channel_count),
            None, None, [[1, tf.keras.activations.sigmoid]],
            use_batch_norm=True
        )
        deep_net.load(saved_model_path)

    net_desc = deep_net.get_structure_descriptor()

    train_data_source, test_data_source = create_data_source(
        positive_path, negative_path,
        train_rate_of_all,
        pro_train_transformer=augment_and_normalize_image, con_train_transformer=normalize_image,
        test_transformer=normalize_image
    )

    train_data_source = shuffle(train_data_source)

    data_source_description = \
        "Train rate: {0}; Train: {1}; Test: {2}; Batch size: {3}; Positive rate: {4}".format(
            train_rate,
            len(train_data_source[0]),
            len(test_data_source[0]),
            batch_size, positive_rate
        )

    logger.log(data_source_description, LogLevels.VERBOSE)

    f_score = 0
    f_score_max = 0
    f_score_threshold = 0.4
    i = 1
    while f_score < f_score_threshold:
        _, _, f_score = evaluate_model(deep_net, test_data_source, epochs_max, log_writer, network_id=str(i))
        if f_score_max < f_score:
            f_score_max = f_score
            print("Current f-score max: {0}".format(f_score_max))

        if f_score < f_score_threshold:
            i = i + 1
            deep_net = create_deep_net(
                (sample_image_size, sample_image_size, input_channel_count),
                output_count=output_channel_count
            )

    print("{0}th F-score in use: {1:.4f}".format(i, f_score_max))

    evaluate_model(deep_net, test_data_source, epochs_max,
                   log_writer, log_section_header="Pretrain results", log_level=LogLevels.INFO)

    epochs = 0
    if saved_model_path is None and epochs_max > 0:
        while epochs < epochs_max and f_score < 1:
            deep_net.train(
                train_data_source[0],
                train_data_source[1],
                epochs=epochs_step,
                batch_size=batch_size
            )
            epochs += epochs_step
            _, _, f_score = evaluate_model(deep_net, train_data_source, epochs, log_writer, network_id=str(i))
            print("After {0} epochs, the f-score: {1}".format(epochs, f_score))

        logger.log(
            "Training finished after {0} ({1}/{2}) epochs".format(epochs, epochs_step, epochs_max),
            LogLevels.INFO
        )

    _, _, f_score_train = evaluate_model(deep_net, train_data_source, epochs, log_writer,
                                         log_section_header="Train results", log_level=LogLevels.INFO)
    accuracy_message, accuracy, f_score = evaluate_model(deep_net, test_data_source, epochs,
                                                         log_writer, log_section_header="Test results",
                                                         log_level=LogLevels.INFO)

    result_description = "Network {4}: {0}\nDataManipulation source: {1}\nEpochs: {2}[{3}/{4}]\n{5}".format(
        net_desc,
        data_source_description,
        epochs,
        epochs_step,
        epochs_max,
        accuracy_message,
        i
    )

    output_folder = "N/A"
    if (f_score >= f_score_save_threshold or f_score_train == 1) and save_good_model and not deep_net.is_loaded():
        output_folder = save_model(deep_net, net_desc, f_score)

    tf.keras.backend.clear_session()

    return result_description, output_folder


def find_classification_errors(model, data_source, log_writer,
                               log_section_header=None,
                               network_id="",
                               prediction_confidence_distance=0.5,
                               show_details=False,
                               log_level=LogLevels.VERBOSE
                               ):
    prediction_results = {}
    prediction_results_sum = [0, 0]
    data = data_source[0]
    label = data_source[1]

    pred = model.predict(data)

    pred_bin = pred.copy().reshape(-1, output_channel_count)
    pred_bin[pred_bin >= 0.5] = 1
    pred_bin[pred_bin < 0.5] = 0

    pred_diff = np.absolute(np.amax(pred, axis=1) - np.amin(pred, axis=1))

    if log_section_header is not None:
        log_writer.log(log_section_header, log_level)

    valid_results = 0
    for item_index in range(0, data.shape[0]):
        match = True
        if not (pred_bin[item_index] == label[item_index]).all():
            match = False
            if show_details:
                log_writer.log(
                    "        index: {0}, label: {1}, bin_prediction: {2}, real_prediction: {3}".format(
                        item_index, label[item_index], pred_bin, pred
                    ),
                    log_level
                )

        if pred_diff[item_index] > 0.5 or output_channel_count == 1:
            valid_results += 1
            prediction_result = None
            label_key = "{0}".format(label[item_index])
            if label_key in prediction_results.keys():
                prediction_result = prediction_results[label_key]

            if prediction_result is None:
                prediction_result = [0, 0]
                prediction_results[label_key] = prediction_result

            prediction_result[1 if match else 0] += 1
            prediction_results_sum[1 if match else 0] += 1

    accuracy = prediction_results_sum[1] / data.shape[0]
    f_score_0 = get_f_score([prediction_results[k] for k in prediction_results.keys()], class_index=0)
    f_score_1 = get_f_score([prediction_results[k] for k in prediction_results.keys()], class_index=1)
    result_message = "Network: {0}".format(network_id)
    result_message = "{0}\n\tPrediction results: {1} [input shape: {2}]".format(
        result_message, prediction_results, data.shape[0]
    )
    result_message = "{0}\n\tNumber of test items: {1}/{2}".format(result_message, valid_results, data.shape[0])
    result_message = "{0}\n\tAccuracy: {1:.4f}; F-score: {2:.4f}".format(result_message, accuracy, min(f_score_0, f_score_1))
    log_writer.log("\t{0}".format(result_message), log_level)

    return result_message, accuracy, min(f_score_0, f_score_1)


def get_f_score(classification_results, class_index=1, beta=1):
    return ((1 + beta**2) * classification_results[class_index][1]) \
           / (
                   ((1 + beta**2) * classification_results[class_index][1]) +
                   beta ** 2 * classification_results[class_index][0] +
                   classification_results[1 - class_index][0]
           )


def save_model(model, sub_path, error):
    date_time_string = datetime.now().strftime("%y%m%d_%H%M")
    target_folder = f"{sub_path}_{error:.4f}_{date_time_string}"

    full_path = os.path.join(base_path, model_save_path, target_folder)
    if not os.path.exists(full_path):
        os.mkdir(full_path)

    model.save(full_path)

    return full_path


def send_email(result_description, password):
    source_name = socket.gethostname()
    login = "gpu.process.notifier@gmail.com"
    sender_email = "{0}@gpu_0008.com".format(source_name)
    receiver_email = "storcz.tamas@gmail.com"

    sender = EmailSender("smtp.gmail.com", 587)
    sender.set_credentials(login, password)
    sender.send(
        sender_email, receiver_email,
        "training finished on {0}".format(source_name),
        "Deep network training finished on {0}\n{1}".format(source_name, result_description)
    )


def histogram_based_net(input_count, output_count):
    model = MultiLayerPerceptron(
        [
            input_count,
            None,
            [output_count, "binary" if output_count == 1 else "probability"]
        ]
    )
    model.metrics = [tf.keras.metrics.binary_accuracy, tf.keras.metrics.Precision(), tf.keras.metrics.Recall()]
    model.compile()
    return model


def train_histogram_based_net(model, train_data_source, test_data_source, log_writer):
    train_data_source = shuffle(train_data_source)

    epochs = 0
    f_score = 0
    while epochs < epochs_max and f_score < 1:
        model.train(
            train_data_source[0],
            train_data_source[1],
            epochs=epochs_step,
            batch_size=batch_size
        )
        epochs += epochs_step
        _, _, f_score = evaluate_model(model, train_data_source, epochs, log_writer)
        print("After {0} epochs, the f-score: {1}".format(epochs, f_score))

    logger.log(
        "Training finished after {0} ({1}/{2}) epochs".format(epochs, epochs_step, epochs_max),
        LogLevels.INFO
    )

    _, _, f_score_train = evaluate_model(model, train_data_source, epochs, log_writer,
                                         log_section_header="Train results", log_level=LogLevels.INFO,
                                         prediction_confidence_distance=0.7)
    _, _, f_score = evaluate_model(model, test_data_source, epochs,
                                   log_writer, log_section_header="Test results",
                                   log_level=LogLevels.INFO,
                                   prediction_confidence_distance=0.7
                                   )

    if (f_score >= f_score_save_threshold or f_score_train > 0.95) and save_good_model:
        output_folder = save_model(model, model.get_structure_descriptor(), f_score)


base_path = sys.argv[1]
email_password = None if len(sys.argv) <= 2 else sys.argv[2]

saved_model_path = None  # os.path.join(base_path, "\models\DSNN_1p3c8_1p3c8___d16_d1_0.8975_220406_1537")
f_score_save_threshold = 0.95
save_good_model = False
model_save_path = "models"

logger = Logger("logger", LogLevels.INFO)
logger.add_channel(
    FileLogChannel("fileLogChannel", os.path.join(base_path, "_output", "dnn.log"))
)
logger.add_channel(
    ConsoleLogChannel("consoleLogChannel")
)

network_input_image_size = 27
input_channel_count = 3
output_channel_count = 1
augmented_image_count = 5
augmentation_max_shift = 2

train_rate = 0.70
batch_size = 64
positive_rate = 0.2

learning_rate = 0.001
epochs_step = 100
epochs_max = 2000

# db = create_data_source(
#     os.path.join(base_path, "mite_samples", "positive_38"),
#     os.path.join(base_path, "mite_samples", "negative_38"),
#     train_rate,
#     pro_train_transformer=compute_augmented_histograms, con_train_transformer=compute_histogram,
#     test_transformer=compute_histogram,
#     classes=[[1, 0], [0, 1]]
# )
#
# net = histogram_based_net(db[0][0].shape[1], db[0][1].shape[1])
# train_histogram_based_net(net, db[0], db[1], log_writer=logger)

result_desc, out_folder = network_training(
    os.path.join(base_path, "mite_samples", "positive_38"),
    os.path.join(base_path, "mite_samples", "negative_38"),
    train_rate,
    sample_image_size=network_input_image_size,
    log_writer=logger)

if email_password is not None:
    send_email(result_desc, email_password)
