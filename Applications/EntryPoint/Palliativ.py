import numpy as np

from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split

from Tools.Logging.FileLogChannel import FileLogChannel
from Tools.Logging.ConsoleLogChannel import ConsoleLogChannel
from Tools.Logging.Logger import LogLevels
from Tools.Logging.Logger import Logger
from Applications.Tests.PalliativTest import PalliativTests


def load_data(file_name):
    file = open(file_name, 'r')

    x_list = []
    y_list = []
    days_list = []

    curr_x = []
    curr_id = -1

    features = 8

    # skip header
    file.readline()
    while True:
        line = file.readline().strip()
        if not line:
            break

        # 4, 8!, 1, 1!, 1
        words = line.split(",")

        new_id = float(words[2])
        new_x = [*map(lambda w: float(w), words[4:12])]
        new_y = [*map(lambda w: float(w), words[13:14])]
        new_days = [int(words[12])]

        if curr_id == new_id:
            curr_x.extend(new_x)
            curr_y = new_y

            while len(curr_x) > lines * features:
                curr_x = curr_x[features:]

            if len(curr_x) == lines * features:
                x_list.append(curr_x.copy())
                y_list.append(curr_y.copy())
                days_list.append(new_days.copy())
        else:
            curr_x = []
            curr_id = new_id

    return x_list, y_list, days_list


def preprocess_data(x_list, y_list):
    x_np = np.array(x_list)

    x_norm = ((2 * (x_np - x_np.min(axis=0))) / (x_np.max(axis=0) - x_np.min(axis=0))) - 1.0

    # highlight inflection point
    # y_np = np.zeros(len(y_list))
    #
    # for ch_i in range(0, len(y_list) - 1):
    #     if y_list[ch_i] != y_list[ch_i + 1]:
    #         y_np[ch_i - 7:ch_i + 7] = 1

    y_np = np.array(y_list)

    return x_norm, y_np


logger = Logger(logger_id="results", log_level=LogLevels.VERBOSE)
logger.add_channel(
    FileLogChannel(
        channel_id="fileLog",
        file_name="D:\\OneDrive\\StorczTamas\\OneDrive\\MIK\\Palliativ\\df_90.log"
    )
)
logger.add_channel(
    ConsoleLogChannel(channel_id="consoleLog")
)

logger.log("---=== DF 90 - border ===---", LogLevels.INFO)

neuron_count = 1000
neuron_count_step = 250

for lc in range(0, 43, 7):
    lines = lc if lc > 0 else 1

    x, y, days = load_data("D:\\OneDrive\\StorczTamas\\OneDrive\\MIK\\Palliativ\\df_90.csv")
    x, y = preprocess_data(x, y)

    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.3, stratify=y)

    pt = PalliativTests()

    for i in range(0, 17):
        model = pt.dense_net(
            input_count=X_train.shape[1],
            hidden_neuron_count=neuron_count + neuron_count_step * i
        )

        iterations = pt.dense_net_train(model, X_train, y_train, epochs=500)

        y_train_pred = model.predict(X_train)
        y_train_pred_bin = y_train_pred >= 0.5
        train_class_rep = classification_report(y_train, y_train_pred_bin, output_dict=True)

        y_test_pred = model.predict(X_test)
        y_test_pred_bin = y_test_pred >= 0.5
        test_class_rep = classification_report(y_test, y_test_pred_bin, output_dict=True)

        logger.log("--------------------", LogLevels.INFO)
        logger.log("hidden neurons={0}".format(neuron_count + neuron_count_step * i), LogLevels.INFO)
        logger.log("lines={0}".format(lines), LogLevels.INFO)
        logger.log("iterations={0}".format(iterations), LogLevels.INFO)
        logger.log("train: {0}".format(train_class_rep), LogLevels.INFO)
        logger.log("test: {0}".format(test_class_rep), LogLevels.INFO)
