import socket
import sys

from Tools.Network.EmailSender import EmailSender


def send_email(result_description, password):
    source_name = socket.gethostname()
    login = "tstorcz@gmail.com"
    sender_email = "{0}@oracle-gpu.com".format(source_name)
    receiver_email = "storcz.tamas@gmail.com"

    sender = EmailSender("smtp.gmail.com", 587)
    sender.set_credentials(login, password)
    sender.send(
        sender_email, receiver_email,
        "training finished on {0}".format(source_name),
        "Deep network training finished on {0}\n{1}".format(source_name, result_description)
    )


print(sys.argv[1])

send_email("Test email", sys.argv[1])
