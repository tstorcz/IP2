import math
import numpy as np
import os
import sys

from skimage.feature import hog
from skimage import exposure

from Image.ImageLoader import ImageLoader
from Image.ImageOut import ImageOut
from Image.Kernel import Kernel
from Tools.Files.FileSystem import FileSystem

from Applications.MiteCount.ExtractGroundTruth import ExtractGroundTruth
from Applications.MiteCount.MiteFinder import MiteFinder


def hog_converter(image, multichannel):
    hog_descriptor, hog_image = hog(image,
                                    orientations=8,
                                    pixels_per_cell=(4, 4), cells_per_block=(1, 1),
                                    visualize=True,
                                    multichannel=multichannel
                                    )
    hog_image = exposure.rescale_intensity(hog_image, in_range=(0, 10))
    return hog_image


def image_converter(image, multichannel=True):
    # return False, hog_converter(image, multichannel)
    return multichannel, image


def save_labeled_images(extractor, target_folder, index, multichannel):
    for idx in range(0, extractor.get_part_count()):
        image_to_export = extractor.get_part(idx)
        save_image(image_to_export,
                   os.path.join(base_path, target_folder, "mite_{0:05d}.jpg".format(index)),
                   multichannel
                   )
        index += 1
    return index


def save_image(image, name, multichannel):
    ImageOut.save_image(
        image if multichannel else image.astype(np.uint8),
        name,
        mode="RGB" if multichannel else "L"
    )


def check_pois(point, poi_list, distance_threshold=10):
    found = False
    poi_index = 0
    while poi_index < len(poi_list) and not found:
        distance = math.sqrt((point[0] - poi_list[poi_index][0])**2 + (point[1] - poi_list[poi_index][1])**2)
        if distance < distance_threshold:
            found = True
        poi_index += 1

    return found


def get_kernels_from_path(path, rotation_angles, extracted_kernel_size):
    mite_kernel_names = FileSystem.get_file_names_of_folder(path, is_recursive=False)
    kernel_collection = []

    for kernel_name in mite_kernel_names:
        _, mite_kernel = image_converter(ImageLoader.load_image_pixels(kernel_name[0], 'L').astype(np.float), False)

        # Filter.high_pass(mite_kernel, 200, -1)
        # Filter.low_pass(mite_kernel, 200, 1)

        kernel_collection.extend(
            Kernel.rotate_kernel(
                mite_kernel, rotation_angles,
                size=extracted_kernel_size
            )
        )

    return kernel_collection


base_path = sys.argv[1]
base_kernel_size = 38
exported_image_size = 38
exported_kernel_size = 27
kernel_folder = os.path.join(base_path, "kernels", "mite_{0}".format(exported_image_size))
labeled_folder = os.path.join("_output", "labeled", "mite_{0}".format(exported_image_size))
false_positive_folder = os.path.join("_output", "false_positives", "mite_{0}".format(exported_image_size))
false_negative_folder = os.path.join("_output", "false_negatives", "mite_{0}".format(exported_image_size))

FileSystem.create_folder(os.path.join(base_path, labeled_folder))
FileSystem.create_folder(os.path.join(base_path, false_positive_folder))
FileSystem.create_folder(os.path.join(base_path, false_negative_folder))

original_image_names = FileSystem.get_file_names_of_folder(
    os.path.join(base_path, "labeled", "source"),
    is_recursive=False
)
marked_image_names = FileSystem.get_file_names_of_folder(
    os.path.join(base_path, "labeled", "dotted"),
    is_recursive=False
)

rotated_kernels = get_kernels_from_path(
    os.path.join(base_path, kernel_folder),
    rotation_angles=Kernel.get_rotation_angles(-90, 90, 8),
    extracted_kernel_size=(exported_kernel_size, exported_kernel_size)
)

mite_index = 0
sum_fp = 0
sum_fn = 0
sum_marked = 0
sum_found = 0

correlation_threshold = 0.65

image_names = original_image_names
for image_index in range(0, len(image_names)):
    print("loading image ({0}/{1}) {2}".format(
        image_index + 1,
        len(image_names),
        image_names[image_index][0]))

    is_original_image_multichannel, original_image = \
        image_converter(ImageLoader.load_oriented_image_pixels(image_names[image_index][0]))
    marked_image = ImageLoader.load_oriented_image_pixels(marked_image_names[image_index][0])

    mites_on_image = 0

    print("Extracting POIs")

    print("Extracting yellow")
    extractor_yellow = ExtractGroundTruth(
        original_image, marked_image,
        exported_image_size, exported_image_size,
        35, 10
    )
    mites_on_image += extractor_yellow.get_part_count()

    print("Extracting blue")
    extractor_blue = ExtractGroundTruth(
        original_image, marked_image,
        exported_image_size, exported_image_size,
        170, 10
    )
    mites_on_image += extractor_blue.get_part_count()

    print("Extracting red")
    extractor_red = ExtractGroundTruth(
        original_image, marked_image,
        exported_image_size, exported_image_size,
        255, 10
    )
    mites_on_image += extractor_red.get_part_count()

    # Save labeled images
    mite_index = save_labeled_images(extractor_yellow, labeled_folder, mite_index, is_original_image_multichannel)
    mite_index = save_labeled_images(extractor_blue, labeled_folder, mite_index, is_original_image_multichannel)
    mite_index = save_labeled_images(extractor_red, labeled_folder, mite_index, is_original_image_multichannel)

    print("Initializing MiteFinder")

    mite_finder = MiteFinder(
        original_image,
        rectangle_mask=None,
        mite_kernels=rotated_kernels,
        mite_correlations_threshold=correlation_threshold,
        crop_size=(exported_image_size, exported_image_size)
    )

    print("Extracting false positives")
    false_positive = 0
    for poi in mite_finder.roi_locations:
        true_positive = check_pois(poi, extractor_yellow.pois)

        if not true_positive:
            true_positive = check_pois(poi, extractor_blue.pois)

        if not true_positive:
            true_positive = check_pois(poi, extractor_red.pois)

        if not true_positive:
            false_positive += 1
            sum_fp += 1

            save_image(
                mite_finder.crop(center=poi),
                os.path.join(base_path, false_positive_folder, "mite_fp_{0:05d}.jpg".format(sum_fp)),
                is_original_image_multichannel
            )

    print("Extracting false negatives")
    false_negative = 0
    for poi in extractor_yellow.pois:
        if not check_pois(poi, mite_finder.roi_locations):
            false_negative += 1
            sum_fn += 1

            save_image(
                mite_finder.crop(center=poi),
                os.path.join(base_path, false_negative_folder, "mite_fp_{0:05d}.jpg".format(sum_fp)),
                is_original_image_multichannel
            )

    for poi in extractor_blue.pois:
        if not check_pois(poi, mite_finder.roi_locations):
            false_negative += 1
            sum_fn += 1

            save_image(
                mite_finder.crop(center=poi),
                os.path.join(base_path, false_negative_folder, "mite_fp_{0:05d}.jpg".format(sum_fp)),
                is_original_image_multichannel
            )

    for poi in extractor_red.pois:
        if not check_pois(poi, mite_finder.roi_locations):
            false_negative += 1
            sum_fn += 1

            save_image(
                mite_finder.crop(center=poi),
                os.path.join(base_path, false_negative_folder, "mite_fp_{0:05d}.jpg".format(sum_fp)),
                is_original_image_multichannel
            )

    marked = int(marked_image_names[image_index][1].split("_")[-1])

    sum_marked += marked
    sum_found += mites_on_image

    print("#{0}: marked: {1}, found: {2}, identified: {3}, FP: {4}, FN: {5}".format(
            image_index + 1, marked, mites_on_image,
            mite_finder.roi_count(),
            false_positive, false_negative
        )
    )

print("Sum Marked: {0}, Found: {1}, FP: {2}, FN: {3}".format(sum_marked, sum_found, sum_fp, sum_fn))
