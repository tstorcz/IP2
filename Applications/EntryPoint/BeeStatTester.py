import os

from Applications.Tests.BeeStats import BeeStats
from Tools.Files.FileSystem import FileSystem

is_test = False

# base_path = r"C:\data\mite"
base_path = r"D:\OneDrive - Pécsi Tudományegyetem\Projects\MiteCount"

if is_test:
    project_path = base_path
    source_image_path = "_test"
    recursive_file_search = False
    log_file_name = "test_sheet_stats.txt"
    database_file_name = "test_sheet_stats.db"
    database_table_name = "log_entry_2"
    use_database = False
else:
    project_path = base_path
    source_image_path = "Atkafotok"
    recursive_file_search = True
    log_file_name = "sheet_stats.txt"
    database_file_name = "sheet_stats.db"
    database_table_name = "log_entry_2"
    use_database = True

mite_identifier_model_folder = r"data\models\DSNN_1c3_1c3_1c3___d128_d1_0.9245_220330_0918"

bee_stat = BeeStats(
    os.path.join(project_path, source_image_path),
    os.path.join(base_path, "kernels"),
    os.path.join(project_path, "_output"),
    os.path.join(project_path, "labeled"),
    magnitude=0.2,

    use_database=use_database,
    database_file_name=database_file_name,
    database_table_name=database_table_name,

    output_kernel_size=(27, 27),
    mite_image_output_folder=r"C:\data\mite\rois",

    ocr_model_path=r"C:\data\mite\models\easyocr",

    mite_identifier=None  # MiteIdentifier(os.path.join(project_path, mite_identifier_model_folder))
)

bee_stat.log_file_name = log_file_name
bee_stat.database_file_name = database_file_name
bee_stat.use_database = use_database

# Loading text log file contents into SQLite database
# bee_stat.load_logs_into_database()

files = FileSystem.get_file_names_of_folder(
        bee_stat.source_path,
        is_recursive=recursive_file_search,
        extension_list=['.jpg']
    )

bee_stat.process_images(files)
