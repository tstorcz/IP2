import numpy as np
import os
import tensorflow as tf

from Image.ImageLoader import ImageLoader
from Image.Filter import Filter
from Tools.Files.FileSystem import FileSystem

from Applications.MiteCount.LogDatabase import LogDatabase
from Applications.MiteCount.MiteDSNN import MiteDSNet


def create_deep_net(image_shape, output_count, learning_rate=0.001, saved_model_path=None):
    if saved_model_path is None:
        hidden_layer_size = 16

        if output_count is None:
            output_count = 1
            for dim in image_shape:
                output_count *= dim

        model = MiteDSNet(
            image_shape,
            None,
            [
                [1, 3, 16, tf.keras.activations.linear, 0],
                [1, 3, 16, tf.keras.activations.linear, 0]
            ],
            [
                [hidden_layer_size, tf.keras.activations.relu],
                [output_count, tf.keras.activations.sigmoid]
            ],
            use_batch_norm=True
        )

        model.create_optimizer("adam", learning_rate=learning_rate)
        model.compile()
    else:
        model = MiteDSNet(
            image_shape,
            None, None, [[1, tf.keras.activations.sigmoid]],
            use_batch_norm=True
        )
        model.load(saved_model_path)

    return model


def extend_input(input_data, out_dimensions):
    ext = tuple(map(lambda i, j: int((j - i)/2), input_data.shape, out_dimensions))
    return np.pad(
        input_data,
        pad_width=((ext[0], ext[0]), (ext[1], ext[1]), (ext[2], ext[2])),
        mode="edge"
    )


model_path = r"D:\OneDrive - Pécsi Tudományegyetem\Projects\MiteCount\data\models\DSNN_1p3c8_1p3c8___d16_d1_0.8975_220406_1537"
roi_path = r"C:\data\mite\rois"

database = LogDatabase(
            r"D:\OneDrive - Pécsi Tudományegyetem\Projects\MiteCount\data\sheet_stats.db",
            table_name="log_entry_2"
        )
database.active(True)
database.open()

sample_size = 27
channel_count = 1

deep_net = create_deep_net(
    (sample_size, sample_size, channel_count),
    1,
    saved_model_path=model_path
)

folders = FileSystem.get_folders_of_folder(roi_path, is_recursive=False)

re_evaluate_files = True

for folder in folders:
    image_id = int(folder.split("_")[-1])

    rois = FileSystem.get_file_names_of_folder(folder, is_recursive=False)

    print("Folder: {0}, files: {1}".format(folder, len(rois)))

    mite_count = 0
    for roi in rois:
        if (roi[1].endswith("_p") or roi[1].endswith("_n")) and not re_evaluate_files:
            if roi[1].endswith("_p"):
                mite_count += 1
        else:
            if roi[1].endswith("_p") or roi[1].endswith("_n"):
                file_name = roi[1][0:-2]
            else:
                file_name = roi[1]

            roi_data = ImageLoader.load_image_pixels(roi[0], mode="L")
            if len(roi_data.shape) < 3:
                roi_data = roi_data.reshape(roi_data.shape + (1,))
            roi_data = extend_input(roi_data, (sample_size, sample_size, channel_count))
            roi_data = Filter.normalize(roi_data)
            estimated_value = deep_net.predict(roi_data).reshape(1)

            if estimated_value[0] >= 0.5:
                FileSystem.move_file(roi[0], os.path.join(folder, file_name + "_p" + roi[2]))
                mite_count += 1
            else:
                FileSystem.move_file(roi[0], os.path.join(folder, file_name + "_n" + roi[2]))

    database.update(database.update_roi_mites_command, [len(rois), mite_count, image_id])
    print("RoIs: {0}, Mites: {1}".format(len(rois), mite_count))

database.commit()
database.close()
