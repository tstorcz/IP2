def get_cog(array_2d):
    cog_y = 0.0
    cog_x = 0.0
    sum_for_avg = 0.0
    for i in range(0, 9):
        cog_y += array_2d[i] * ((i // 3) + 1)
        cog_x += array_2d[i] * ((i % 3) + 1)
        sum_for_avg += array_2d[i]

    if sum_for_avg != 0:
        return int(cog_y/sum_for_avg) - 1, int(cog_x/sum_for_avg) - 1

    return 0, 0


for v in range(0, 2**9):
    arr = [int(x) for x in list('{0:09b}'.format(v))]
    arr.append(1 if get_cog(arr) == (1, 1) else 0)
    print(arr)
