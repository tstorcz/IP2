import os
import sys

from Applications.MiteCount.LogDatabase import LogDatabase
from Tools.Files.FileSystem import FileSystem


def get_text_parts(text: str):
    parts = text.split('_')

    collection = {"name": parts[0], "image_id": int(parts[1])}

    if len(parts) > 2:
        collection["roi_id"] = int(parts[2])

    for idx in range(3, len(parts)):
        if parts[idx][0:1].lower() == 'p':
            collection["type"] = "positive"
        elif parts[idx][0:1].lower() == 'n':
            collection["type"] = "negative"
        else:
            collection[parts[idx][0:1]] = parts[idx][1:]

    return collection


def classify_samples_ncc(image_names, threshold):
    positive = 0
    negative = 0
    unknown = 0

    for image in image_names:
        image_details = get_text_parts(image[1])

        if "c" in image_details.keys():
            if int(image_details["c"]) >= threshold:
                positive += 1
            else:
                negative += 1
        else:
            unknown += 1

    return positive, negative, unknown


def get_folder_names(folder_name):
    folder_full_name = os.path.join(base_path, folder_name)
    return FileSystem.get_folders_of_folder(folder_full_name)


def get_image_names(folder_name, is_recursive=False):
    image_folder_full_name = os.path.join(base_path, folder_name)
    return FileSystem.get_file_names_of_folder(image_folder_full_name, is_recursive=is_recursive)


base_path = sys.argv[1]

database = LogDatabase(
            os.path.join(base_path, "Atkafotok\sheet_stats.db"),
            table_name="log_entry_2"
        )
database.open()

folders = get_folder_names("rois")

for folder in folders:
    print(folder)
    image_descr = get_text_parts(folder)
    p, n, u = classify_samples_ncc(get_image_names(folder), 800)

    database.update(database.update_mites_command, [p, image_descr["image_id"]])

database.commit()
database.close()
