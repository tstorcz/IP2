import subprocess
import sys
import importlib.util as import_util


def execute_command(command_arg_list):
    subprocess.call([sys.executable] + command_arg_list)


def install_package(target_package_name):
    print("installing: " + target_package_name)
    execute_command(["-m", "pip", "install", target_package_name])


def uninstall_package(target_package_name):
    print("uninstalling: " + target_package_name)
    execute_command(["-m", "pip", "uninstall", target_package_name])


execute_command(["--version"])

execute_command(["-m", "pip", "install", "--upgrade", "pip"])
execute_command(["-m", "pip", "install", "--upgrade", "setuptools"])

print("Uninstall before install? (y/n)")
answer = input()

prerequisites = ["numpy", "matplotlib", "scipy", "scikit-image",
                 "sklearn", "matplotlib",
                 "Pillow", "Pillow-PIL",
                 "pyzernikemoment",
                 "opencv-python",
                 "progressbar", "StereoVision",
                 "simplejson", "mttkinter",
                 "Keras", "tensorflow==2.2.0"]


if answer == "y":
    for package_name in prerequisites[::-1]:
        if import_util.find_spec(package_name) is not None:
            uninstall_package(package_name)
            if import_util.find_spec(package_name) is not None:
                print("Please revise uninstallation of {}".format(package_name))

for package_name in prerequisites:
    if import_util.find_spec(package_name) is None:
        print("missing: " + package_name)
        install_package(package_name)
