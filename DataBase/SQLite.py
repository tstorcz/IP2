import sqlite3


class SQLite(object):

    is_active = True

    def __init__(self, database_file_name):
        self.database_file_name = database_file_name
        self.connection = None

    def open(self):
        if self.is_active:
            self.connection = sqlite3.connect(self.database_file_name)

    def commit(self):
        if self.connection:
            self.connection.commit()

    def close(self):
        if self.connection:
            self.connection.commit()
            self.connection.close()
        self.connection = None

    def execute(self, sql_statement, parameters=None):
        if self.is_active and self.connection:
            c = self.connection.cursor()
            if parameters is None:
                c.execute(sql_statement)
            else:
                c.execute(sql_statement, parameters)

            return c.lastrowid

        return -1

    def fetch_rows(self, sql_statement, parameters=None):
        if self.is_active and self.connection:
            c = self.connection.cursor()
            if parameters is None:
                c.execute(sql_statement)
            else:
                c.execute(sql_statement, parameters)

            return c.fetchall()

        return []
