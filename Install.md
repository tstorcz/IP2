## Project structure  
```
IP2  
+-- Data
+-- SourceImages  
+-- src  
```
## Install
1. Create project structure manually  
2. Clone the repository into **src** folder of the project.
3. set project interpreter
4. set project structure
5. set git ignores
6. install packages listed in Requirements