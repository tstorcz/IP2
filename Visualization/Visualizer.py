import numpy as np
import matplotlib.pyplot as plt


class Visualizer(object):

    @staticmethod
    def visualize_2d(X, y, model, output_converter=None, mesh_steps=0.01):
        x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
        y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, mesh_steps), np.arange(y_min, y_max, mesh_steps))
        Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        y_pred = model.predict(X)

        if output_converter is not None:
            Z = output_converter(Z)
            y_pred = output_converter(y_pred)

        plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)
        plt.scatter(X[:, 0], X[:, 1], c=y*-1, cmap=plt.cm.Set1, s=60)
        plt.scatter(X[:, 0], X[:, 1], c=y_pred*-1, cmap=plt.cm.Set1, s=20)
        plt.show()
