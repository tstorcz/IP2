# Requirements
## Required Python packages
- numpy
- matplotlib
- Pillow-PIL
- scipy
- pyzernikemoment
- scikit-image
- scikit-learn
- cv2-python [opencv-python / opencv-contrib-python]
- progressbar-latest [progressbar] - required for StereoVision
- StereoVision
- simplejson
- mttkinter - above Python 3.1 tkinter is a part of Python distribution
- Keras - required for MachineLearning/FeedForwardNeuralNetworks/MultiLayerPerceptron
- tensorflow [tensorflow-gpu for GPU support]

## Other requirements
Compilation requires C++ 14 (included in VisualStudio 2015)
  
When import cv2 results "*DLL load failed*", OpenCV2 requires MediaFeaturePack for Windows N editions
- [Windows OS info](https://support.microsoft.com/en-us/help/13443/windows-which-version-am-i-running)
- [Select MFP version](https://support.microsoft.com/en-us/help/3145500/media-feature-pack-list-for-windows-n-editions)
- [Download](https://www.microsoft.com/en-us/software-download/mediafeaturepack)