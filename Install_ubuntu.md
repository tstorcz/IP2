Get Ubuntu version and list of GPU devices:  
```commandline
lsb_release -a
sudo lshw -C display
```

Remove nvidia related installations:  
```commandline
sudp apt update
sudo apt upgrade  

sudo apt purge nvidia*
sudo apt autoremove
```

##Install python 3.8 with pip and build essentials  
```commandline
sudo apt install python / sudo apt install python3
sudo apt install python3-pip
sudo apt install build-essential
```
 
##Download, than install driver and toolkit for Ubuntu 20.04:  
[NVidia oficial driver](https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=2004&target_type=runfilelocal)  
Contains: Driver 460, ToolKit 11.2
```commandline
wget https://developer.download.nvidia.com/compute/cuda/11.2.1/local_installers/cuda_11.2.1_460.32.03_linux.run
sudo sh cuda_11.2.1_460.32.03_linux.run
```
Please make sure that
 -   PATH includes /usr/local/cuda-11.2/bin  
```commandline
export PATH=$PATH:/usr/local/cuda-11.2/bin  
```
 -   LD_LIBRARY_PATH includes /usr/local/cuda-11.2/lib64, or, add /usr/local/cuda-11.2/lib64 to /etc/ld.so.conf and run ldconfig as root  
```commandline
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-11.2/lib64
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-11.2/targets/x86_64-linux/lib
```
 - persist environment variables
```commandline
source ~/.profile
```
 - reboot the system
```commandline
sudo reboot
```

###Check NVidia driver version:
```commandline
nvidia-smi
nvcc --version
```

###Uninstall 
To uninstall the CUDA Toolkit, run cuda-uninstaller in /usr/local/cuda-11.2/bin  
To uninstall the NVIDIA Driver, run nvidia-uninstall  
Logfile has been created: /var/log/cuda-installer.log  

###Extension
Installation details:
- CUDA version: 11.2  
- CUDA driver: 460.32.03  
- Python: 3.8.5  
- TensorFlow: 2.4.1  

TensorFlow 2.4 also requires libcusolver.so.10, therefore install ToolKit separatelly, which will contain CUDA10
####Install NVIDIA CUDA ToolKit
```commandline
sudo apt install nvidia-cuda-toolkit
```

##Install cuDNN
- After login, download cuDNN.tar file from [nVidia developer site](https://developer.nvidia.com/cudnn)   
- Send file to Linux machine from Windows:
```commandline
pscp -P 22 -i <Private key (ppk) file> <source file> <user>@<host URL>:<destination folder>
```
- Extract tar file  
 ```commandline
tar -xvf <tar file name>
```
- Follow instructions: [SDK documentation - Installation](https://docs.nvidia.com/deeplearning/cudnn/archives/cudnn_765/cudnn-install/index.html)  
Copy files to CUDA installation path
```commandline
whereis cuda
sudo cp cuda/include/cudnn.h /usr/local/cuda/include
sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64
sudo chmod a+r /usr/local/cuda/include/cudnn.h /usr/local/cuda/lib64/libcudnn*
```

##Install tensorflow
```commandline
pip3 install tensorflow
```

Test tensorflow GPU installation in Python console:
```python
import tensorflow as tf
print(tf.__version__)
print(tf.config.list_physical_devices("GPU"))
```