# GPU support  

Hardware requirements: NVIDIA® GPU card with CUDA® Compute Capability 3.5 or higher    
[CUDA-enabled GPU cards](https://developer.nvidia.com/cuda-gpus)  
 
Tested with GeForce780, GTX1660Ti, Tesla P100  
[Tensorflow GPU support](https://www.tensorflow.org/install/gpu)  

####Install tensorflow-gpu package
_**Always check installed TensorFlow package version and be careful with updates**_ 

####CUDA examples
To compile CUDA examples, install Visual Studio first  
CUDA ToolKit 10.0 requires [VisualStudio 2017](https://visualstudio.microsoft.com/vs/older-downloads/) (requires VisualStudio Dev Essentials subscription)  
Comatibilitx can be checked [for CUDA 10.2](https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.html)  

###[CUDA compatibility](https://docs.nvidia.com/deploy/cuda-compatibility/index.html)  
- CUDA 10.0 requires NVIDIA driver 410.48 or higher  
- CUDA 10.1 requires NVIDIA driver 418.39 or higher  

###Install driver  
[NVIDIA driver search page](https://www.nvidia.com/Download/index.aspx)  
[NVIDIA driver](https://www.geforce.com/drivers)

###[CUDA ToolKit](https://developer.nvidia.com/cuda-toolkit-archive)  
TensorFlow 2.0 supports **ONLY** [CUDA 10.0](https://developer.nvidia.com/cuda-10.0-download-archive)  
TensorFlow 2.1 supports **ONLY** [CUDA 10.1 with updates](https://developer.nvidia.com/cuda-10.1-download-archive-update2?target_os=Windows&target_arch=x86_64&target_version=10&target_type=exenetwork)   

####**To compile samples with VisualStudio copy all files from:**  
If Visual Studio installed first, CUDA installer creates VS integration

####CUDA 10.1 and VS2019  
C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.1\extras\visual_studio_integration\MSBuildExtensions  
**to this path:**  
C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Microsoft\VC\v160\BuildCustomizations

####[cuDNN](https://developer.nvidia.com/rdp/cudnn-download)  
v7.6.5 for CUDA 9.0-10.2  
Download the 2019.11.05 for CUDA 10.1  
Most important part of [Installation](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html) is 4.3.3 - 4.3.4
 