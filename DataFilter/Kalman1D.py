class Kalman1D(object):

    def __init__(self, params = {}):
        self.isInitialized = False
        self.xhat = 0                 # a posteri estimate of x
        self.P = 0                    # a posteri error estimate
        self.xhatminus = 0            # a priori estimate of x
        self.Pminus = 0               # a priori error estimate
        self.K = 0                    # gain or blending factor       
        self.Q = params["Q"] if "Q" in params else 1e-5       # measured process variance
        self.R = params["R"] if "R" in params else 0.1**2     # estimate of measurement noise variance
        
    def getEstimation(self):
        return self.xhat
        
    def addValue(self, newValue):
        if self.isInitialized:
            self.xhatminus = self.xhat
            self.Pminus = self.P + self.Q
        
            self.K = self.Pminus / (self.Pminus + self.R)
            self.xhat = self.xhatminus + self.K * (newValue - self.xhatminus)
            self.P = (1 - self.K) * self.Pminus
        else:
            self.xhat = newValue
            self.isInitialized = True